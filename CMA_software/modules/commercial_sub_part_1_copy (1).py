# Databricks notebook source
# MAGIC %md
# MAGIC # Part 1: Prepare the Program Coded Viewing File
# MAGIC 
# MAGIC **Input Tables and Views**  
# MAGIC * `TAM_NPM_MCH_TV_PROD.program_coded_metered_tv_location_media_engagement_event`
# MAGIC * `TAM_NPM_MCH_TV_PROD.program_coded_metered_tv_person_media_engagement_event`
# MAGIC * `event_table_raw_df`   (parquet, sub-minute households tuning)
# MAGIC * `station_maping_table` (parquet)
# MAGIC * `TAM_NPM_MCH_TV_PROD.report_originator_lineup`
# MAGIC * `TAM_NPM_MCH_TV_PROD.national_tv_content_originator`
# MAGIC * `TAM_NPM_MCH_TV_PROD.intab_period`
# MAGIC * `TAM_NPM_MCH_TV_PROD.metered_tv_household_location_intab_status`
# MAGIC * `TAM_NPM_MCH_TV_PROD.metered_tv_household_location_intab_and_weight`
# MAGIC * `TAM_NPM_MCH_TV_PROD.metered_tv_household_location_person_intab_and_weight`
# MAGIC * `TAM_NPM_MCH_TV_PROD.metered_tv_person`
# MAGIC * `TAM_NPM_MCH_TV_PROD.program_summary_type`
# MAGIC 
# MAGIC **Output Views**  
# MAGIC * `telecast_details`
# MAGIC * `intab_period`
# MAGIC * `temp_program_coded_viewing`         (local)
# MAGIC * `temp_program_coded_person_viewing`  (local)
# MAGIC * `program_coded`              (unreused)
# MAGIC * `program_coded_stn`          (unreused)
# MAGIC * `program_coded_stn_demo`
# MAGIC * `pb_program_coded_stn_demo`

# COMMAND ----------

# depending on if it is a run with rounded file, subminute file, household or person tables, different MDL paths are determined
if execution_type in ('Rounded file, rounded methodology', 'Rounded file, non-rounded methodology'):
  event_table_df = \
    'TAM_NPM_MCH_TV_PROD.program_coded_metered_tv_{}_media_engagement_event' \
      .format('location' if household_run else 'person')
else:
  if household_run:
    event_table_df = 'event_table_raw_df'
    event_table_raw_df = spark.read.parquet(tuning_file_sub_mins_dir)
    event_table_raw_df.createOrReplaceTempView(event_table_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Pull the Viewing Table from the MDL

# COMMAND ----------

#######################################################################################################
### This query pulls all telecasts in report_originator_lineup table for the measurement period 
### of interest. We will use this telecast to find out which telecast are missing from the ACM table
#######################################################################################################

s ='''--sql
  SELECT
    COMP.report_originator_lineup_key,
    COMP.national_tv_report_program_id           AS program_id,
    COMP.report_originator_lineup_id             AS telecast_id,
    COMP.reportable_report_originator_lineup_id  AS reportable_telecast_id,
    COMP.complex_id,
    COMP.national_tv_broadcast_date              AS telecast_broadcast_date,
    date_format(COMP.national_tv_broadcast_date, 'EEEE')
                                                 AS telecast_broadcast_date_days_of_week,
    COMP.program_name,
    COMP.telecast_name,
    COMP.program_summary_type_code,
    COMP.report_duration_in_minutes,
    COMP.report_start_time_ny,
    COMP.is_umbrella_flag,
    CONT.originator_distributor_id,
    CONT.distributor_type_code,
    CONT.originator_name,
    CASE  -- 8:Broadcast 9:Cable 12:Syndication 21:Unwired Network 
      WHEN CONT.distributor_type_code =  9 THEN 'Cable'
      WHEN CONT.distributor_type_code = 12 THEN 'Syndication'
      WHEN CONT.distributor_type_code =  8 THEN 'Network'
    END                                      AS  originator_common_service_name,
    GENRE.program_summary_type_desc

  FROM (
      SELECT
        report_originator_lineup_key,
        national_tv_report_program_id,
        report_originator_lineup_id,
        reportable_report_originator_lineup_id,
        complex_id,
        national_tv_broadcast_date,
        program_name,
        telecast_name,
        program_summary_type_code,
        report_duration_in_minutes,
        report_start_time_ny,
        is_umbrella_flag,
        originator_distributor_id
      FROM
        TAM_NPM_MCH_TV_PROD.report_originator_lineup
      WHERE TRUE
        AND released_for_processing_flag     = 'Y' 
        AND multi_barter_flag               <> 'Y' 
        AND cash_barter_flag                <> 'Y' 
        AND lineup_release_type_code         =  1 --Currency telecasts only (excluding Fasties, Retro Coded telecasts)
        AND originator_type_code            IN (8, 9, 12)  -- 8:Broadcast 9:Cable 12:Syndication 21:Unwired Network 
        AND national_tv_broadcast_date BETWEEN '{national_tv_broadcast_start_date}'
                                           AND '{national_tv_broadcast_end_date}'
    ) AS COMP

  JOIN 
    TAM_NPM_MCH_TV_PROD.national_tv_content_originator AS CONT 
  ON    TRUE
    AND CONT.originator_distributor_id        = COMP.originator_distributor_id 
    AND COMP.national_tv_broadcast_date BETWEEN CONT.effective_start_date
                                            AND CONT.effective_end_date 

  JOIN
    TAM_NPM_MCH_TV_PROD.program_summary_type AS GENRE
  ON    TRUE
    AND COMP.program_summary_type_code = GENRE.program_summary_type_code
  --  AND GENRE.program_summary_type_desc IN {genre}
  
  --endsql''' \
    .format(national_tv_broadcast_start_date = national_tv_broadcast_start_date,
            national_tv_broadcast_end_date   = national_tv_broadcast_end_date,
            genre                            = genre)

telecast_details = spark.sql(s).cache()

telecast_details.createOrReplaceTempView("telecast_details")

# COMMAND ----------

intab_period = spark.sql("""--sql
  SELECT
    intab_period_id,
    intab_period_start_date
  FROM
    TAM_NPM_MCH_TV_PROD.intab_period
  WHERE TRUE
    AND intab_period_start_date BETWEEN '{metered_start_date}'
                                    AND '{viewing_end_date}'
  
  --endsql"""
    .format(metered_start_date = metered_start_date,
            viewing_end_date   = viewing_end_date)) \
  .repartition('intab_period_id').cache()

intab_period.createOrReplaceTempView('intab_period')

# COMMAND ----------

tuning_query = '''--sql
  WITH
    temp_program_coded_viewing AS (
      SELECT 
        --     Telecast     --
        TELECAST.telecast_broadcast_date, 
        TELECAST.originator_name, 
        TELECAST.originator_distributor_id, 
        TELECAST.originator_common_service_name, 
        TELECAST.distributor_type_code, 
        TELECAST.report_originator_lineup_key, 
        TELECAST.report_start_time_ny,
        TELECAST.report_duration_in_minutes,
        TELECAST.program_name, 
        TELECAST.program_id,
        TELECAST.program_summary_type_code, 
        TELECAST.telecast_name, 
        TELECAST.telecast_id,
        TELECAST.reportable_telecast_id,
        TELECAST.complex_id,
        TELECAST.is_umbrella_flag,

        --     Time period     --
        -- VIEW.metered_tv_intab_period_id,
        IP.intab_period_start_date  AS intab_period_date, 
        -- VIEW.viewed_intab_period_id,  -- VIP.intab_period_start_date  AS viewed_intab_period_date,
        -- VIEW.national_service_time_zone_id, 
        -- VIEW.credit_quarter_hour_datetime_utc,
        VIEW.sample_characteristic_intab_period_id,  -- used in a join

        --     Location     --
        VIEW.program_coded_media_engagement_event_key,
        VIEW.market_id, 
        VIEW.source_installed_location_number, 
        VIEW.source_media_device_number, 
        VIEW.household_media_consumer_id, 
        VIEW.location_media_consumer_id, 
        -- VIEW.location_media_consumer_type_code, 
        -- VIEW.media_device_id, 
        VIEW.media_device_category_code,                        -- equals 1

        --     Station     --
        VIEW.metered_tv_credit_station_code, 

        --     Tuning event     --
        VIEW.credit_start_of_viewing_utc,
        -- VIEW.credit_end_of_viewing_utc, 
        VIEW.viewed_start_of_viewing_utc,
        -- VIEW.viewed_end_of_viewing_utc, 
        -- VIEW.credit_start_of_viewing_local_time,
        -- VIEW.credit_end_of_viewing_local_time,
        -- VIEW.viewed_start_of_viewing_local_time,
        -- VIEW.viewed_end_of_viewing_local_time,
        VIEW.credit_start_of_viewing_ny_time,
        VIEW.credit_end_of_viewing_ny_time,
        VIEW.viewed_start_of_viewing_ny_time,
        VIEW.viewed_end_of_viewing_ny_time,
        VIEW.start_minute_of_program, 
        VIEW.end_minute_of_program, 
        VIEW.duration_in_minutes, 
        VIEW.start_second_of_program,
        VIEW.end_second_of_program,
        VIEW.duration_in_seconds,

        --     Time-shifted tuning     --
        -- VIEW.playback_type_code,
        VIEW.time_shifted_viewing_type_code, 

        --     Other     --
        VIEW.is_simultaneous_viewing_flag,                          -- equals 'N'
        -- INTAB.source_installed_location_number,
        INTAB.sample_collection_method_key                          -- equals 7
         
      FROM
        intab_period AS IP

      JOIN (
          SELECT
            *
          FROM
            {event_table}
          WHERE TRUE
            AND media_device_category_code        =  1  -- 1:TV Tuning, 2:PC Tuning
            AND released_for_processing_flag      = 'Y' 
            AND is_simultaneous_viewing_flag      = 'N'  -- removing simultaneouse viewing
            AND location_media_consumer_type_code =  1  -- deleting extended homes
        ) AS VIEW
      ON    TRUE
        AND VIEW.metered_tv_intab_period_id   = IP.intab_period_id

      JOIN 
        telecast_details AS TELECAST
      ON    TRUE
        AND TELECAST.report_originator_lineup_key = VIEW.report_originator_lineup_key

      -- JOIN 
      --   TAM_NPM_MCH_TV_PROD.intab_period AS VIP
      -- ON    TRUE
      --   AND VIP.intab_period_id = VIEW.viewed_intab_period_id 

      JOIN (
          SELECT
            sample_collection_method_key,

            intab_period_id,
            household_media_consumer_id,
            media_consumer_id
          FROM
            TAM_NPM_MCH_TV_PROD.metered_tv_household_location_intab_status
          WHERE TRUE
            AND released_for_processing_flag = 'Y'
            AND sample_collection_method_key =  7  -- 7:NPM Homes, 2:SM Homes
        ) AS INTAB 
      ON    TRUE
        AND INTAB.intab_period_id              = VIEW.sample_characteristic_intab_period_id
        AND INTAB.household_media_consumer_id  = VIEW.household_media_consumer_id
        AND INTAB.media_consumer_id            = VIEW.location_media_consumer_id
    )


  SELECT 
    --     Telecast     --
    VIEW_T.telecast_broadcast_date, 
    VIEW_T.originator_name, 
    VIEW_T.originator_distributor_id, 
    VIEW_T.originator_common_service_name, 
    VIEW_T.distributor_type_code, 
    VIEW_T.report_originator_lineup_key,
    VIEW_T.report_start_time_ny,
    VIEW_T.report_duration_in_minutes,
    VIEW_T.program_name, 
    VIEW_T.program_id, 
    VIEW_T.program_summary_type_code, 
    VIEW_T.telecast_name, 
    VIEW_T.telecast_id, 
    VIEW_T.reportable_telecast_id, 
    VIEW_T.complex_id, 
    VIEW_T.is_umbrella_flag,

    --     Time period     --
    -- VIEW_T.metered_tv_intab_period_id,
    VIEW_T.intab_period_date,
    -- VIEW_T.viewed_intab_period_id,
    -- VIEW_T.national_service_time_zone_id,
    -- VIEW_T.credit_quarter_hour_datetime_utc,

    --     Location     --
    VIEW_T.program_coded_media_engagement_event_key,
    VIEW_T.market_id, 
    VIEW_T.source_installed_location_number, 
    VIEW_T.source_media_device_number,
    VIEW_T.household_media_consumer_id,
    VIEW_T.location_media_consumer_id,
    -- VIEW_T.location_media_consumer_type_code, 
    -- VIEW.media_device_id, 
    VIEW_T.media_device_category_code,                     -- equals 1

    --     Station     --
    VIEW_T.metered_tv_credit_station_code, 

    --     Tuning event     --
    -- VIEW.credit_start_of_viewing_utc,
    -- VIEW.credit_end_of_viewing_utc, 
    -- VIEW.viewed_start_of_viewing_utc,
    -- VIEW.viewed_end_of_viewing_utc, 
    -- VIEW_T.credit_start_of_viewing_local_time,
    -- VIEW_T.credit_end_of_viewing_local_time,
    -- VIEW_T.viewed_start_of_viewing_local_time,
    -- VIEW_T.viewed_end_of_viewing_local_time,
    VIEW_T.credit_start_of_viewing_ny_time, 
    VIEW_T.credit_end_of_viewing_ny_time, 
    VIEW_T.viewed_start_of_viewing_ny_time, 
    VIEW_T.viewed_end_of_viewing_ny_time, 
    VIEW_T.start_minute_of_program, 
    VIEW_T.end_minute_of_program, 
    VIEW_T.duration_in_minutes, 
    VIEW_T.start_second_of_program,
    VIEW_T.end_second_of_program,
    VIEW_T.duration_in_seconds,

    --     Time-shifted tuning     --
    VIEW_T.time_shifted_viewing_type_code, 
    -- VIEW_T.playback_type_code,
    unix_timestamp(VIEW_T.viewed_start_of_viewing_utc)
      - unix_timestamp(VIEW_T.credit_start_of_viewing_utc) AS viewing_delay,

    --     Other     --
    VIEW_T.is_simultaneous_viewing_flag,              -- equals 'N'
    -- VIEW_T.source_installed_location_number,
    
    --     Location details     --
    WEIGHT.location_weight                AS weight,
    WEIGHT.sample_collection_method_key,              -- equals 7
    WEIGHT.is_intab_indicator                         -- equals 1

  FROM 
    temp_program_coded_viewing AS VIEW_T

  JOIN (
      SELECT
        location_weight,
        sample_collection_method_key,
        is_intab_indicator,

        household_media_consumer_id,
        location_media_consumer_id,
        sample_characteristic_intab_period_id
      FROM
        TAM_NPM_MCH_TV_PROD.metered_tv_household_location_intab_and_weight
      WHERE TRUE
        AND is_intab_indicator           = 1
        AND media_device_category_code   = 1
        AND sample_collection_method_key = 7
    ) AS WEIGHT
  ON    TRUE
    AND WEIGHT.household_media_consumer_id           = VIEW_T.household_media_consumer_id
    AND WEIGHT.location_media_consumer_id            = VIEW_T.location_media_consumer_id
    AND WEIGHT.sample_characteristic_intab_period_id = VIEW_T.sample_characteristic_intab_period_id

  --endsql''' \
    .format(event_table        = event_table_df)

# COMMAND ----------

viewing_query ='''--sql
  WITH
    temp_program_coded_person_viewing AS (
      SELECT 
        --     Telecast     --
        TELECAST.telecast_broadcast_date, 
        TELECAST.originator_name, 
        TELECAST.originator_distributor_id, 
        TELECAST.originator_common_service_name, 
        TELECAST.distributor_type_code, 
        TELECAST.report_originator_lineup_key,
        TELECAST.report_start_time_ny,
        TELECAST.report_duration_in_minutes,
        TELECAST.program_name, 
        TELECAST.program_id,
        TELECAST.program_summary_type_code, 
        TELECAST.telecast_name, 
        TELECAST.telecast_id,
        TELECAST.reportable_telecast_id,
        TELECAST.complex_id,
        TELECAST.is_umbrella_flag,

        --     Time period     --
        -- VIEW.metered_tv_intab_period_id,
        IP.intab_period_start_date  AS intab_period_date, 
        -- VIEW.viewed_intab_period_id,   -- VIP.intab_period_start_date  AS viewed_intab_period_date,
        -- VIEW.national_service_time_zone_id, 
        -- VIEW.credit_quarter_hour_datetime_utc,
        VIEW.sample_characteristic_intab_period_id, 

        --     Location/person     --
        VIEW.program_coded_media_engagement_event_key,
        VIEW_T.market_id,  
        VIEW.source_installed_location_number, 
        VIEW.source_media_device_number, 
        VIEW.source_person_number,
        VIEW.household_media_consumer_id, 
        VIEW.location_media_consumer_id, 
        -- VIEW.location_media_consumer_type_code, 
        -- VIEW.media_device_id, 
        VIEW.media_device_category_code,                   -- equals 1
        VIEW.person_media_consumer_id,   
        VIEW.person_media_consumer_key,  -- used in a join

        --     Station     --
        VIEW.metered_tv_credit_station_code, 

        --     Viewing event     --
        VIEW.credit_start_of_viewing_utc, 
        -- VIEW.credit_end_of_viewing_utc, 
        VIEW.viewed_start_of_viewing_utc, 
        -- VIEW.viewed_end_of_viewing_utc, 
        -- VIEW.credit_start_of_viewing_local_time, 
        -- VIEW.credit_end_of_viewing_local_time, 
        -- VIEW.viewed_start_of_viewing_local_time, 
        -- VIEW.viewed_end_of_viewing_local_time, 
        VIEW.credit_start_of_viewing_ny_time, 
        VIEW.credit_end_of_viewing_ny_time, 
        VIEW.viewed_start_of_viewing_ny_time, 
        VIEW.viewed_end_of_viewing_ny_time, 
        VIEW.start_minute_of_program, 
        VIEW.end_minute_of_program, 
        -- VIEW.start_minute_after_midnight, 
        -- VIEW.end_minute_after_midnight,    
        VIEW.duration_in_minutes, 

        --     Time-shifted viewing     --
        -- VIEW.minutes_from_record_to_playback, 
        -- VIEW.playback_type_code, 
        VIEW.time_shifted_viewing_type_code, 

        --     Other     --
        VIEW.is_simultaneous_viewing_flag,              -- equals 'N'
        -- INTAB.source_installed_location_number,
        -- INTAB.is_intab_flag,
        -- INTAB.is_intab_indicator,
        INTAB.sample_collection_method_key

      FROM
        intab_period AS IP

      JOIN (
          SELECT
            *
          FROM
            {event_table}
          WHERE TRUE
            AND media_device_category_code        =  1   -- 1:TV Tuning, 2:PC Tuning
            AND released_for_processing_flag      = 'Y'
            AND is_simultaneous_viewing_flag      = 'N'  -- removing simultaneouse viewing
            AND location_media_consumer_type_code =  1   -- deleting extended homes
        ) AS VIEW
      ON    TRUE
        AND VIEW.metered_tv_intab_period_id = IP.intab_period_id

      JOIN
        telecast_details AS TELECAST
      ON    TRUE
        AND TELECAST.report_originator_lineup_key = VIEW.report_originator_lineup_key

      -- JOIN 
      --   TAM_NPM_MCH_TV_PROD.intab_period AS VIP 
      -- ON 
      --   VIP.intab_period_id = VIEW.viewed_intab_period_id 

      JOIN (
          SELECT
            sample_collection_method_key,

            intab_period_id,
            household_media_consumer_id,
            media_consumer_id
          FROM
            TAM_NPM_MCH_TV_PROD.metered_tv_household_location_intab_status
          WHERE TRUE
            AND released_for_processing_flag  = 'Y'
            AND sample_collection_method_key  =  7   -- 7:NPM Homes, 2:SM Homes
        ) AS INTAB 
      ON    TRUE
        AND INTAB.intab_period_id              = VIEW.sample_characteristic_intab_period_id
        AND INTAB.household_media_consumer_id  = VIEW.household_media_consumer_id
        AND INTAB.media_consumer_id            = VIEW.location_media_consumer_id
    )


  SELECT 
    --     Telecast     --
    VIEW_T.telecast_broadcast_date, 
    VIEW_T.originator_name, 
    VIEW_T.originator_distributor_id, 
    VIEW_T.originator_common_service_name, 
    VIEW_T.distributor_type_code, 
    VIEW_T.report_originator_lineup_key,
    VIEW_T.report_duration_in_minutes,
    VIEW_T.report_start_time_ny,
    VIEW_T.program_name, 
    VIEW_T.program_id, 
    VIEW_T.program_summary_type_code, 
    VIEW_T.telecast_name, 
    VIEW_T.telecast_id, 
    VIEW_T.reportable_telecast_id, 
    VIEW_T.complex_id, 
    VIEW_T.is_umbrella_flag,

    --     Time period     --
    -- VIEW_T.metered_tv_intab_period_id,
    VIEW_T.intab_period_date,
    -- VIEW_T.viewed_intab_period_id, 
    -- VIEW_T.national_service_time_zone_id,
    -- VIEW_T.credit_quarter_hour_datetime_utc,
    -- VIEW_T.reported_daypart_desc,

    --     Location/person     --
    VIEW_T.program_coded_media_engagement_event_key,
    VIEW_T.market_id,  
    VIEW_T.source_installed_location_number, 
    VIEW_T.source_media_device_number,
    VIEW_T.source_person_number,
    VIEW_T.household_media_consumer_id,
    VIEW_T.location_media_consumer_id,
    -- VIEW_T.location_media_consumer_type_code,
    -- VIEW_T.media_device_id, 
    VIEW_T.media_device_category_code,            -- equals 1
    VIEW_T.person_media_consumer_id,

    --     Station     --
    VIEW_T.metered_tv_credit_station_code, 

    --     Viewing event     --
    VIEW_T.credit_start_of_viewing_utc, 
    -- VIEW_T.credit_end_of_viewing_utc, 
    VIEW_T.viewed_start_of_viewing_utc, 
    -- VIEW_T.viewed_end_of_viewing_utc, 
    -- VIEW_T.credit_start_of_viewing_local_time, 
    -- VIEW_T.credit_end_of_viewing_local_time, 
    -- VIEW_T.viewed_start_of_viewing_local_time, 
    -- VIEW_T.viewed_end_of_viewing_local_time, 
    VIEW_T.credit_start_of_viewing_ny_time, 
    VIEW_T.credit_end_of_viewing_ny_time, 
    VIEW_T.viewed_start_of_viewing_ny_time, 
    VIEW_T.viewed_end_of_viewing_ny_time, 
    VIEW_T.start_minute_of_program, 
    VIEW_T.end_minute_of_program, 
    -- VIEW_T.start_minute_after_midnight, 
    -- VIEW_T.end_minute_after_midnight, 
    VIEW_T.duration_in_minutes, 

    --     Time-shifted viewing     --
    -- VIEW_T.minutes_from_record_to_playback, 
    -- VIEW_T.playback_type_code,
    VIEW_T.time_shifted_viewing_type_code, 
    unix_timestamp(VIEW_T.viewed_start_of_viewing_utc)
      - unix_timestamp(VIEW_T.credit_start_of_viewing_utc) AS viewing_delay,

        --     Other     --
    VIEW_T.is_simultaneous_viewing_flag,             -- equals 'N'
    -- VIEW_T.is_intab_flag,

        --     Person details     --
    WEIGHT.person_weight                        AS weight,
    WEIGHT.is_long_term_visitor_indicator,
    WEIGHT.is_short_term_visitor_indicator,
    WEIGHT.sample_collection_method_key,             -- equals  7
    WEIGHT.is_intab_indicator,                 -- equals 1

    CHAR.age,
    CHAR.gender_code

  FROM 
    temp_program_coded_person_viewing AS VIEW_T

  JOIN (
      SELECT
        person_weight,
        is_long_term_visitor_indicator,
        is_short_term_visitor_indicator,
        sample_collection_method_key,
        is_intab_indicator,

        household_media_consumer_id,
        location_media_consumer_id,
        person_media_consumer_id,
        sample_characteristic_intab_period_id
      FROM
        TAM_NPM_MCH_TV_PROD.metered_tv_household_location_person_intab_and_weight
      WHERE TRUE
        AND media_device_category_code   = 1
        AND sample_collection_method_key = 7
        AND is_intab_indicator           = 1
    ) AS WEIGHT
  ON    TRUE
    AND WEIGHT.household_media_consumer_id            = VIEW_T.household_media_consumer_id
    AND WEIGHT.location_media_consumer_id             = VIEW_T.location_media_consumer_id
    AND WEIGHT.person_media_consumer_id               = VIEW_T.person_media_consumer_id
    AND WEIGHT.sample_characteristic_intab_period_id  = VIEW_T.sample_characteristic_intab_period_id

  JOIN
    TAM_NPM_MCH_TV_PROD.metered_tv_person AS CHAR
  ON    TRUE
    AND VIEW_T.person_media_consumer_key = CHAR.person_media_consumer_key

  --endsql''' \
    .format(event_table        = event_table_df,
            metered_start_date = metered_start_date,
            viewing_end_date   = viewing_end_date)

# COMMAND ----------

program_coded = spark.sql(tuning_query if household_run else viewing_query)

if execution_type ==  'Non-rounded file, non-rounded methodology':
  program_coded =  program_coded \
    .filter('duration_in_seconds >= 1') \
    .withColumn('credit_start_of_viewing_ny_time1', f.date_format(f.col('credit_start_of_viewing_ny_time'), "yyyy-MM-dd HH:mm:ss")) \
    .withColumn('credit_end_of_viewing_ny_time1', f.date_format(f.col('credit_end_of_viewing_ny_time'), "yyyy-MM-dd HH:mm:ss")) \
    .withColumn('viewed_start_of_viewing_ny_time1', f.date_format(f.col('viewed_start_of_viewing_ny_time'), "yyyy-MM-dd HH:mm:ss")) \
    .withColumn('viewed_end_of_viewing_ny_time1', f.date_format(f.col('viewed_end_of_viewing_ny_time'), "yyyy-MM-dd HH:mm:ss")) \
    .drop('credit_start_of_viewing_ny_time') \
    .drop('credit_end_of_viewing_ny_time') \
    .drop('viewed_start_of_viewing_ny_time') \
    .drop('viewed_end_of_viewing_ny_time') \
    .withColumnRenamed('credit_start_of_viewing_ny_time1','credit_start_of_viewing_ny_time') \
    .withColumnRenamed('credit_end_of_viewing_ny_time1','credit_end_of_viewing_ny_time')\
    .withColumnRenamed('viewed_start_of_viewing_ny_time1','viewed_start_of_viewing_ny_time')\
    .withColumnRenamed('viewed_end_of_viewing_ny_time1','viewed_end_of_viewing_ny_time')

program_coded.createOrReplaceTempView('program_coded') 

# COMMAND ----------

##############################################################################
### start_second_of_program and end_second_of_program in  rounded tuning
### and viewing files are wrong. Here, these two columns are recomputed 
### by software
##############################################################################

fix_second_column_query = '''
SELECT
  *,
  (start_minute_of_program-1)*60+1 as computed_start_second_of_program,
  (start_minute_of_program-1)*60+duration_in_seconds	 as computed_end_second_of_program
FROM
  program_coded
'''
if execution_type == 'Rounded file, non-rounded methodology':
  program_coded = spark.sql(fix_second_column_query)


  program_coded = program_coded.drop('start_second_of_program') \
  .drop('end_second_of_program') \
  .withColumnRenamed('computed_start_second_of_program','start_second_of_program') \
  .withColumnRenamed('computed_end_second_of_program','end_second_of_program')

  program_coded.createOrReplaceTempView("program_coded")

# COMMAND ----------

#######################################################
## the list of split stations is pulled here
########################################################

split_station = spark.read.load(split_station_table_path,
                     format="csv",inferSchema="true", header="true")
# split_station = split_station.select('METERED_TV_CREDIT_STATION_CODE').distinct()
split_station.createOrReplaceTempView('split_station')

# COMMAND ----------

#######################################################
## split stations are not present in the sub-minute date
## in order to hold a fair compariosn between minute-level
## rating and sub-minute level rating, they should be
## also removed from production
########################################################
if remove_split_stations:
  program_coded = spark.sql('''

  SELECT
    event.*
  FROM 
    program_coded event
  LEFT JOIN
    split_station stn
  ON
    event.metered_tv_credit_station_code = stn.metered_tv_credit_station_code
    AND event.telecast_broadcast_date between stn.effective_start_date and stn.effective_end_date
  WHERE
    stn.METERED_TV_CREDIT_STATION_CODE IS NULL
  ''')
  program_coded.createOrReplaceTempView('program_coded')

# COMMAND ----------

###########################################################################################
### The station maping parquet file is pulled from the MDL here              #######
### this file is used to identify the networks each station code belongs to  #######
###########################################################################################

station_maping_table = \
  spark.read.parquet(station_mapping_table_path) \
       .hint('broadcast')

station_maping_table.createOrReplaceTempView("station_maping_table")

# COMMAND ----------

#############################################################################
### Here the viewing file is joined with the station mapping file and 
### genre table. Also the data is cut back to only genres present in
### genra variable, which is one of the input parameters
#############################################################################

s = '''--sql
  SELECT
    VIEW.*,
    date_format(VIEW.telecast_broadcast_date, 'EEEE')  AS telecast_broadcast_date_days_of_week,
    coalesce(call, 'not exists')                       AS viewing_source_short_name,
    GENRE.program_summary_type_desc

  FROM
    program_coded AS VIEW

  LEFT JOIN
    station_maping_table AS STATION
  ON
    VIEW.metered_tv_credit_station_code = STATION.station_code
    AND VIEW.telecast_broadcast_date = STATION.effective_date

  JOIN
    TAM_NPM_MCH_TV_PROD.program_summary_type AS GENRE
  ON
    VIEW.program_summary_type_code = GENRE.program_summary_type_code
    
  --endsql'''

program_coded_stn = spark.sql(s)
program_coded_stn.createOrReplaceTempView('program_coded_stn')

# COMMAND ----------

#############################################################################################
### This python function creates the sql queries that are required in order to append 
### the demo columns to the dataset
#############################################################################################


# Boundary ages of the demos

if not household_run:
  age_limits = {
    demo: 
      {'low': demo[1:demo.index('_')], 'high': demo[demo.index('_')+1:]}
    for demo in demos_of_interest
  }
  
  person_query_builder = ',\n'.join([
    'CASE\n' +
    '  WHEN TABLE.age     BETWEEN  {}\n'.format(age_limits[demo]['low' ]) +
    '                         AND  {}\n'.format(age_limits[demo]['high']) +
    ( ''
      if demo[0] == 'P' else
      '   AND TABLE.gender_code  =   {}\n'.format(1 if demo[0] == 'M' else 0)
    ) +
    "    THEN 'Y'\n" +
    "  ELSE   'N'\n" +
    'END   AS  {}'.format(demo)
    for demo in demos_of_interest])


# COMMAND ----------

###########################################################################
### The function append_demo_indicator is defined to add demo indicator 
### columns to the input data set. The assumption of the function is that
### the columns age and  gender_code exist within the input dataset.       
###########################################################################

def append_demo_indicator (data_frame_without_demo_ind):
  '''
  This function appends demo indicator columns for 'HH'.
  '''
  
  query = '''--sql
    SELECT
      TABLE.*,
      {sub_query}
    FROM
      {data_frame_without_demo_ind} AS TABLE

  --endsql''' \
    .format(data_frame_without_demo_ind = data_frame_without_demo_ind,
            sub_query                   = "'Y' AS `HH`" if household_run else person_query_builder)

  return spark.sql(query)

# COMMAND ----------

###########################################################################
### The function append_demo_indicator is called for 
### program_coded_stn as an input table. The output table will have
### a bunch of demo indicator columns.
###########################################################################

program_coded_stn_demo = append_demo_indicator('program_coded_stn').cache()
program_coded_stn_demo.createOrReplaceTempView("program_coded_stn_demo")

# COMMAND ----------

#############################################################################
### Here the playback type is defined.
### `time_shifted_viewing_type_code` of 0 means "live",
### 1 means "Same Day",lpoki9 hyt5grew2qsa2s
### 2 means "1st day", 3 means "2nd day", etc.
### (See `TAM_NPM_MCH_TV_PROD.time_shifted_viewing_type`)
#############################################################################

live_program_coded_stn_demo = \
  program_coded_stn_demo.filter('time_shifted_viewing_type_code = 0')
live_program_coded_stn_demo = \
  live_program_coded_stn_demo.withColumn('playback_type', f.lit('live'))

live_sd_program_coded_stn_demo = \
  program_coded_stn_demo.filter('time_shifted_viewing_type_code in (0,1)')
live_sd_program_coded_stn_demo = \
  live_sd_program_coded_stn_demo.withColumn('playback_type', f.lit('live+SD'))

seven_day_pb_program_coded_stn_demo = \
  program_coded_stn_demo.filter('viewing_delay <= 604800')  # 168hrs * 3600 secs/hr
seven_day_pb_program_coded_stn_demo = \
  seven_day_pb_program_coded_stn_demo.withColumn('playback_type', f.lit('live+7'))

three_day_pb_program_coded_stn_demo = \
  program_coded_stn_demo.filter('viewing_delay <= 270000')  # 75 hrs * 3600 secs/hr
three_day_pb_program_coded_stn_demo = \
  three_day_pb_program_coded_stn_demo.withColumn('playback_type', f.lit('live+3'))


# pb_program_coded_stn_demo    =     live_program_coded_stn_demo if playback_type_list == ['live'] else live_program_coded_stn_demo.union(live_sd_program_coded_stn_demo) if playback_type_list == ['live','live+SD'] else live_program_coded_stn_demo.union(three_day_pb_program_coded_stn_demo).union(seven_day_pb_program_coded_stn_demo)

pb_program_coded_stn_demo    =     live_program_coded_stn_demo.union(three_day_pb_program_coded_stn_demo).union(seven_day_pb_program_coded_stn_demo)


pb_program_coded_stn_demo.createOrReplaceTempView("pb_program_coded_stn_demo")
