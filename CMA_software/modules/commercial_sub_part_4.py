# Databricks notebook source
# MAGIC %md
# MAGIC # Part 4: Compute ACM per Telecast/Network
# MAGIC 
# MAGIC 
# MAGIC **Input Tables and Views**  
# MAGIC * `ads_fact_dim`
# MAGIC * `intab_df`
# MAGIC * `pb_program_coded_stn_demo`
# MAGIC * `telecast_details`
# MAGIC 
# MAGIC **Output Views**  
# MAGIC * `commercial_minutes`         (local)
# MAGIC * `commercial_minutes_adj`
# MAGIC * `acm_commercial_minutes_adj_ag`
# MAGIC * `commercial_tuning`          (local, in function, params)
# MAGIC * `commercial_tuning_adj`      (local, in function, params)
# MAGIC * `commercial_tuning_adj_ag`   (local, in function, params)
# MAGIC * `acm_table`                  (unreused)
# MAGIC * `telecast_details_enhanced`  (unreused)
# MAGIC * `acm_table_enhanced`

# COMMAND ----------

# MAGIC %md
# MAGIC ### ACM Computation Engine

# COMMAND ----------

commercial_minutes_adj = \
  spark.sql("""--sql
    WITH
      commercial_minutes AS (
        SELECT 
            broadcast_date,
            program_id,
            telecast_id,
            minute_of_program,
            sum(advertisement_duration) AS advertisement_duration

        FROM
          ads_fact_dim

        GROUP BY
          broadcast_date, program_id, telecast_id, minute_of_program
      )


    SELECT
      *,
      {adjusted_duration} AS advertisement_duration_adj

    FROM
      commercial_minutes

    --endsql"""
      .format(adjusted_duration = 'least(advertisement_duration, 60)'
                                  if adjust_commercial_duration else
                                  'advertisement_duration')) \
    .cache()

commercial_minutes_adj.createOrReplaceTempView('commercial_minutes_adj')

# COMMAND ----------

acm_commercial_minutes_adj_ag = \
  spark.sql("""--sql
    SELECT
      broadcast_date,
      program_id,
      telecast_id,
      sum(advertisement_duration_adj) AS advertisement_duration_per_telecast

    FROM
      commercial_minutes_adj

    GROUP BY
      broadcast_date, program_id, telecast_id

    --endsql""") \
    .cache()

acm_commercial_minutes_adj_ag.createOrReplaceTempView('acm_commercial_minutes_adj_ag')

# COMMAND ----------

##############################################################
### This python function computes ACM rating and projection 
### for a specified viewing file, ads_fact table, and demo 
### of interest
##############################################################

def ACM_computation_engine_rounded(demo,viewing_df):
  
  '''
  This python function returns ACM projection and rating
  for a specified viewing file and ads_fact table.
  The function assumes that following columns exist in the input datasets with the specified format. 
  
  viewing table columns:
  [
    "telecast_broadcast_date",              StringType()
    "program_id",                           StringType()
    "telecast_id",                          StringType()
    "playback_type",                        StringType()
    "demo",                                 StringType()
    "originator_common_service_name",       StringType()
    "program_name",                         StringType()
    "telecast_name",                        StringType()
    "telecast_broadcast_date_days_of_week", StringType()
    "report_start_time_ny",                 StringType()
    "viewing_source_short_name",            StringType()
    "program_summary_type_desc",            StringType()
    "start_minute_of_program",              IntegerType()
    "end_minute_of_program"],               IntegerType()
  ]
  
  
  ads_fact table columns: 
  [
    "broadcast_date",         StringType()
    "program_id",             StringType()
    "telecast_id",            StringType()
    "minute_of_program",      IntegerType()
    "advertisement_duration", DecimalType()
  ]
  '''
  
  s = '''--sql
    WITH
      commercial_tuning AS (
        SELECT 
          VIEW.telecast_broadcast_date,
          VIEW.program_id,
          VIEW.telecast_id,
          VIEW.playback_type,
          VIEW.originator_common_service_name,
          VIEW.program_name,
          VIEW.telecast_name,
          VIEW.telecast_broadcast_date_days_of_week,
          VIEW.report_start_time_ny,
          VIEW.viewing_source_short_name,
          VIEW.program_summary_type_desc,
          COMMERCIAL.minute_of_program,
          COMMERCIAL.advertisement_duration_adj,
          sum(VIEW.weight)                        AS sow
        
        FROM 
          {viewing_df} AS VIEW
        
        JOIN
          commercial_minutes_adj AS COMMERCIAL
        
        WHERE TRUE
          AND VIEW.telecast_broadcast_date       = COMMERCIAL.broadcast_date
          and VIEW.program_id                    = COMMERCIAL.program_id
          and VIEW.telecast_id                   = COMMERCIAL.telecast_id
          and COMMERCIAL.minute_of_program BETWEEN VIEW.start_minute_of_program
                                               AND VIEW.end_minute_of_program
          AND VIEW.{demo}                        = 'Y'
        
        GROUP BY
          VIEW.telecast_broadcast_date, VIEW.program_id, VIEW.telecast_id, VIEW.playback_type,
          VIEW.originator_common_service_name, VIEW.program_name, VIEW.telecast_name, VIEW.telecast_broadcast_date_days_of_week,
          VIEW.report_start_time_ny, VIEW.viewing_source_short_name, VIEW.program_summary_type_desc,
          COMMERCIAL.minute_of_program, COMMERCIAL.advertisement_duration_adj
      ),

      commercial_tuning_adj AS (
        SELECT
          VIEW.*,
          round(VIEW.sow*INTAB.adj_factor,2) AS sow_adj

        FROM
          commercial_tuning AS VIEW

        JOIN
          intab_df AS INTAB
        ON
          VIEW.telecast_broadcast_date = INTAB.intab_period_start_date

        WHERE
          INTAB.demo = '{demo}'
      ),

      commercial_tuning_adj_ag AS (
        SELECT
          VIEW.telecast_broadcast_date,
          VIEW.program_id,
          VIEW.telecast_id,
          VIEW.playback_type,
          VIEW.originator_common_service_name,
          VIEW.program_name,
          VIEW.telecast_name,
          VIEW.telecast_broadcast_date_days_of_week,
          VIEW.report_start_time_ny,
          VIEW.viewing_source_short_name,
          VIEW.program_summary_type_desc,
          sum(VIEW.advertisement_duration_adj*VIEW.sow_adj) AS sow_per_telecast

        FROM
          commercial_tuning_adj AS VIEW

        GROUP BY
          VIEW.telecast_broadcast_date, VIEW.program_id, VIEW.telecast_id, VIEW.playback_type,
          VIEW.originator_common_service_name, VIEW.program_name, VIEW.telecast_name, VIEW.telecast_broadcast_date_days_of_week,
          VIEW.report_start_time_ny, VIEW.viewing_source_short_name, VIEW.program_summary_type_desc
      )


    SELECT
      VIEW_AG.telecast_broadcast_date,
      VIEW_AG.program_id,
      VIEW_AG.telecast_id,
      VIEW_AG.playback_type,
      '{demo}'                            AS demo,
      VIEW_AG.originator_common_service_name,
      VIEW_AG.program_name,
      VIEW_AG.telecast_name,
      VIEW_AG.telecast_broadcast_date_days_of_week,
      VIEW_AG.report_start_time_ny,
      VIEW_AG.viewing_source_short_name,
      VIEW_AG.program_summary_type_desc,
      round(VIEW_AG.sow_per_telecast / COMMERCIAL_AG.advertisement_duration_per_telecast)
                                          AS ac_projection,
      100 * VIEW_AG.sow_per_telecast / (COMMERCIAL_AG.advertisement_duration_per_telecast*INTAB.ue)
                                          AS ac_rating

    FROM
      commercial_tuning_adj_ag AS VIEW_AG

    JOIN
      intab_df AS INTAB
    ON
      VIEW_AG.telecast_broadcast_date = INTAB.intab_period_start_date

    JOIN
      acm_commercial_minutes_adj_ag AS COMMERCIAL_AG
    ON    TRUE
      AND VIEW_AG.telecast_broadcast_date  = COMMERCIAL_AG.broadcast_date
      AND VIEW_AG.program_id               = COMMERCIAL_AG.program_id
      AND VIEW_AG.telecast_id              = COMMERCIAL_AG.telecast_id

    WHERE
      INTAB.demo = '{demo}'

    --endsql''' \
      .format(demo = demo,
              viewing_df = viewing_df)

  return spark.sql(s)

# COMMAND ----------

##############################################################
### This python function computes ACM rating and projection 
### for a specified viewing file, ads_fact table, and demo 
### of interest
##############################################################

def ACM_computation_engine_non_rounded(demo,viewing_df):
  
  '''
  This python function returns ACM projection and rating
  for a specified viewing file and ads_fact table.
  The function assumes that following columns exist in the input datasets with the specified format. 
  
  viewing table columns:
  [
    "telecast_broadcast_date",              StringType()
    "program_id",                           StringType()
    "telecast_id",                          StringType()
    "playback_type",                        StringType()
    "demo",                                 StringType()
    "originator_common_service_name",       StringType()
    "program_name",                         StringType()
    "telecast_name",                        StringType()
    "telecast_broadcast_date_days_of_week", StringType()
    "report_start_time_ny",                 StringType()
    "viewing_source_short_name",            StringType()
    "program_summary_type_desc",            StringType()
    "start_minute_of_program",              IntegerType()
    "end_minute_of_program"],               IntegerType()
  ]
  
  
  ads_fact table columns: 
  [
    "broadcast_date",         StringType()
    "program_id",             StringType()
    "telecast_id",            StringType()
    "minute_of_program",      IntegerType()
    "advertisement_duration", DecimalType()
  ]
  '''
  
  s = '''--sql
    WITH
      commercial_tuning_adj_ag AS (
        SELECT
          VIEW.telecast_broadcast_date,
          VIEW.program_id,
          VIEW.telecast_id,
          VIEW.playback_type,
          VIEW.originator_common_service_name,
          VIEW.program_name,
          VIEW.telecast_name,
          VIEW.telecast_broadcast_date_days_of_week,
          VIEW.report_start_time_ny,
          VIEW.viewing_source_short_name,
          VIEW.program_summary_type_desc,
          round(sum(VIEW.watched_advertisement_duration  * VIEW.weight * INTAB.adj_factor),2) AS sow_per_telecast

        FROM
          {viewing_df} AS VIEW
        JOIN
          intab_df AS INTAB
        ON
          VIEW.telecast_broadcast_date = INTAB.intab_period_start_date

        GROUP BY
          VIEW.telecast_broadcast_date, VIEW.program_id, VIEW.telecast_id, VIEW.playback_type,
          VIEW.originator_common_service_name, VIEW.program_name, VIEW.telecast_name, VIEW.telecast_broadcast_date_days_of_week,
          VIEW.report_start_time_ny, VIEW.viewing_source_short_name, VIEW.program_summary_type_desc
      )


    SELECT
      VIEW_AG.telecast_broadcast_date,
      VIEW_AG.program_id,
      VIEW_AG.telecast_id,
      VIEW_AG.playback_type,
      '{demo}'                            AS demo,
      VIEW_AG.originator_common_service_name,
      VIEW_AG.program_name,
      VIEW_AG.telecast_name,
      VIEW_AG.telecast_broadcast_date_days_of_week,
      VIEW_AG.report_start_time_ny,
      VIEW_AG.viewing_source_short_name,
      VIEW_AG.program_summary_type_desc,
      round(VIEW_AG.sow_per_telecast / COMMERCIAL_AG.advertisement_duration_per_telecast)
                                          AS ac_projection,
      100 * VIEW_AG.sow_per_telecast / (COMMERCIAL_AG.advertisement_duration_per_telecast*INTAB.ue)
                                          AS ac_rating

    FROM
      commercial_tuning_adj_ag AS VIEW_AG

    JOIN
      intab_df AS INTAB
    ON
      VIEW_AG.telecast_broadcast_date = INTAB.intab_period_start_date

    JOIN
      acm_commercial_minutes_adj_ag AS COMMERCIAL_AG
    ON    TRUE
      AND VIEW_AG.telecast_broadcast_date  = COMMERCIAL_AG.broadcast_date
      AND VIEW_AG.program_id               = COMMERCIAL_AG.program_id
      AND VIEW_AG.telecast_id              = COMMERCIAL_AG.telecast_id

    WHERE
      INTAB.demo = '{demo}'

    --endsql''' \
      .format(demo = demo,
              viewing_df = viewing_df)

  return spark.sql(s)

# COMMAND ----------

####################################################################
### The function ACM_computation_engine is called for 
### the specific demo of interest. The output table will have
### ac_projection (000) and ac_rating columns. The ACM_computation_engine
### library assumes that columns with a very specific format exist in both 
### viewing and ads_fact tables
#####################################################################

field = [
  t.StructField("telecast_broadcast_date",              t.StringType(),  True),
  t.StructField("program_id",                           t.IntegerType(), True),
  t.StructField("telecast_id",                          t.LongType(),    True),
  t.StructField("playback_type",                        t.StringType(),  True),
  t.StructField("demo",                                 t.StringType(),  True),
  t.StructField("originator_common_service_name",       t.StringType(),  True),
  t.StructField("program_name",                         t.StringType(),  True),
  t.StructField("telecast_name",                        t.StringType(),  True),
  t.StructField("telecast_broadcast_date_days_of_week", t.StringType(),  True),
  t.StructField("report_start_time_ny",                 t.StringType(),  True),
  t.StructField("viewing_source_short_name",            t.StringType(),  True),
  t.StructField("program_summary_type_desc",            t.StringType(),  True),
  t.StructField("ac_projection",                        t.FloatType(),   True),
  t.StructField("ac_rating",                            t.FloatType(),   True)
]

acm_schema = t.StructType(field)
acm_table = spark.createDataFrame([], acm_schema) 

for demo in demos_of_interest:
  if is_rounded:
    acm_table_part = ACM_computation_engine_rounded(demo, 'pb_program_coded_stn_demo')
  else:
    acm_table_part = ACM_computation_engine_non_rounded(demo, 'pb_program_coded_stn_demo_ads')

  acm_table = acm_table.union(acm_table_part)
    

acm_table.createOrReplaceTempView("acm_table")

# COMMAND ----------

###########################################################################################
### The telecast_details table created in the previous cell is expanded here by 
### playback_type and demo. In other words, for every row in telecast_details table
### we will increase the number of rows to (number of demos) * (number of playback_type) rows.
### The reason is that since the acm table is a table by demo/playback_type, we need to have 
### telecast_detail table in a consistent format
#############################################################################################

telecast_details_enhanced = spark.createDataFrame([], acm_schema)

s = '''--sql
  SELECT
    telecast_broadcast_date,
    program_id,
    telecast_id,
    '{playback}'                           AS playback_type,
    '{demo}'                               AS demo,
    originator_common_service_name,
    program_name,
    telecast_name,
    telecast_broadcast_date_days_of_week,
    report_start_time_ny,
    NULL                                   AS viewing_source_short_name,
    program_summary_type_desc,
    NULL                                   AS ac_projection,
    NULL                                   AS ac_rating
    
  FROM
    telecast_details

  --endsql'''

for demo in demos_of_interest:
  for playback in playback_type_list:
    telecast_details_enhanced_part = spark.sql(s.format(playback = playback, demo = demo))
    telecast_details_enhanced = telecast_details_enhanced.union(telecast_details_enhanced_part)
  
telecast_details_enhanced.createOrReplaceTempView("telecast_details_enhanced")

# COMMAND ----------

###############################################################################
### Here ACM table is joined with telecast_detail_enhanced table
### As a result of this join, we create the ACM of 0 for all commercials 
### that are absent in the ACM table
###############################################################################

s = '''--sql
  SELECT
    TELECAST.telecast_broadcast_date,
    TELECAST.program_id,
    TELECAST.telecast_id,
    TELECAST.playback_type,
    TELECAST.demo,
    TELECAST.originator_common_service_name,
    TELECAST.program_name,
    TELECAST.telecast_name,
    TELECAST.telecast_broadcast_date_days_of_week,
    TELECAST.report_start_time_ny,
    TELECAST.program_summary_type_desc,
    coalesce(ACM.viewing_source_short_name, 'Zero Audience') AS viewing_source_short_name,
    coalesce(ACM.ac_projection, 0.0)                         AS ac_projection,
    coalesce(ACM.ac_rating, 0.0)                             AS ac_rating
    
  FROM
    telecast_details_enhanced AS TELECAST

  FULL OUTER JOIN
    acm_table AS ACM
  ON    TRUE
    AND TELECAST.telecast_broadcast_date  = ACM.telecast_broadcast_date
    and TELECAST.program_id               = ACM.program_id
    and TELECAST.telecast_id              = ACM.telecast_id
    and TELECAST.demo                     = ACM.demo
    and TELECAST.playback_type            = ACM.playback_type

  --endsql'''

acm_table_enhanced = spark.sql(s)  
acm_table_enhanced.createOrReplaceTempView("acm_table_enhanced")

acm_table_enhanced.cache()
