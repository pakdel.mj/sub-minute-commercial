# Databricks notebook source
# MAGIC %md
# MAGIC # Part 5: ECM Computation per Telecast/Network/Commercial
# MAGIC 
# MAGIC 
# MAGIC **Input Tables and Views**  
# MAGIC * `intab_df`
# MAGIC * `pb_program_coded_stn_demo`
# MAGIC * `ads_fact_dim`
# MAGIC * `commercial_minutes_adj`
# MAGIC 
# MAGIC **Output Views**  
# MAGIC * `ecm_commercial_minutes_adj_ag`
# MAGIC * `commercial_tuning`                          (local, in function, params)
# MAGIC * `commercial_tuning_adj`                      (local, in function, params)
# MAGIC * `commercial_tuning_adj_exp`                  (local, in function, params)
# MAGIC * `commercial_tuning_adj_ag`                   (local, in function, params)
# MAGIC * `ecm_table`                                  (unreused)
# MAGIC * `commercial_pos_in_pod_df`
# MAGIC * `commercial_pos_in_pod_df_enhanced`          (unreused)
# MAGIC * `ecm_table_enhanced`                         (unreused)

# COMMAND ----------

ecm_commercial_minutes_adj_ag = \
  spark.sql("""--sql
    SELECT
      broadcast_date,
      program_id,
      telecast_id,
      advertisement_key,
      advertisement_code,
      commercial_start_time,
      pod_value,
      brand_desc,
      brand_variant_desc,
      creative_desc,
      brand_code,
      pcc_industry_group_desc,
      sum(advertisement_duration) AS advertisement_duration_per_telecast

    FROM
      ads_fact_dim
      
    GROUP BY
      broadcast_date, program_id, telecast_id, advertisement_key, advertisement_code,
      commercial_start_time, pod_value, brand_desc, brand_variant_desc, creative_desc,
      brand_code, pcc_industry_group_desc

    --endsql""") \
    .cache()

ecm_commercial_minutes_adj_ag.createOrReplaceTempView('ecm_commercial_minutes_adj_ag')

# COMMAND ----------

##############################################################
### This python function computes ECM rating and projection 
### for a specified viewing file, ads_fact table, and demo 
### of interest
##############################################################

def ECM_computation_engine_rounded(demo,viewing_df,ads_fact_dim_df):
  
  s = '''--sql
    WITH
      commercial_tuning AS (
        SELECT 
          VIEW.telecast_broadcast_date,
          VIEW.program_id,
          VIEW.telecast_id,
          VIEW.playback_type,
          VIEW.viewing_source_short_name,
          COMMERCIAL.minute_of_program,
          COMMERCIAL.advertisement_duration_adj,
          sum(VIEW.weight) AS sow

        FROM 
          {viewing_df} AS VIEW

        JOIN
          commercial_minutes_adj AS COMMERCIAL

        WHERE TRUE
          AND VIEW.telecast_broadcast_date       = COMMERCIAL.broadcast_date
          AND VIEW.program_id                    = COMMERCIAL.program_id
          AND VIEW.telecast_id                   = COMMERCIAL.telecast_id
          AND COMMERCIAL.minute_of_program BETWEEN VIEW.start_minute_of_program
                                               AND VIEW.end_minute_of_program
          AND VIEW.{demo}                        = 'Y'

        GROUP BY
          VIEW.telecast_broadcast_date, VIEW.program_id, VIEW.telecast_id, VIEW.playback_type,
          VIEW.viewing_source_short_name,
          COMMERCIAL.minute_of_program, COMMERCIAL.advertisement_duration_adj
      ),

      commercial_tuning_adj AS (
        SELECT
          VIEW.*,
          round(VIEW.sow*INTAB.adj_factor, 2) AS sow_adj

        FROM
          commercial_tuning AS VIEW

        JOIN
          intab_df AS INTAB
        ON
          VIEW.telecast_broadcast_date = INTAB.intab_period_start_date

        WHERE
          intab.demo = '{demo}'
      ),

      commercial_tuning_adj_exp as (
        SELECT DISTINCT
          VIEW.telecast_broadcast_date,
          VIEW.program_id,
          VIEW.telecast_id,
          VIEW.playback_type,
          VIEW.viewing_source_short_name,
          VIEW.minute_of_program,
          VIEW.sow_adj,

          ADS.commercial_start_time,
          ADS.pod_value,
          ADS.advertisement_key,
          ADS.advertisement_code,
          ADS.brand_desc,
          ADS.brand_variant_desc,
          ADS.creative_desc,
          ADS.brand_code,
          ADS.pcc_industry_group_desc,
          ADS.advertisement_duration 

        FROM
          commercial_tuning_adj AS VIEW

        JOIN
          {ads_fact_dim_df} AS ADS

        ON    TRUE
          AND VIEW.telecast_broadcast_date  = ADS.broadcast_date
          AND VIEW.program_id               = ADS.program_id
          AND VIEW.telecast_id              = ADS.telecast_id
          AND VIEW.minute_of_program        = ADS.minute_of_program
      ),

      commercial_tuning_adj_ag AS (
        SELECT
          telecast_broadcast_date,
          program_id,
          telecast_id,
          playback_type,
          viewing_source_short_name,
          commercial_start_time,
          pod_value,
          advertisement_key,
          advertisement_code,
          brand_desc,
          brand_variant_desc,
          creative_desc,
          brand_code,
          pcc_industry_group_desc,
          sum(sow_adj*advertisement_duration) AS sow_per_ads_per_telecast

        FROM
          commercial_tuning_adj_exp

        GROUP BY
          telecast_broadcast_date, program_id, telecast_id, playback_type, viewing_source_short_name,
          commercial_start_time, pod_value, advertisement_key, advertisement_code,
          brand_desc, brand_variant_desc,
          creative_desc, brand_code,pcc_industry_group_desc
      )

    SELECT
      VIEW_AG.telecast_broadcast_date,
      VIEW_AG.program_id,
      VIEW_AG.telecast_id,
      VIEW_AG.playback_type,
      '{demo}'                                            AS demo,
      VIEW_AG.viewing_source_short_name,
      VIEW_AG.commercial_start_time,
      VIEW_AG.pod_value,
      VIEW_AG.advertisement_key,
      VIEW_AG.advertisement_code,
      VIEW_AG.brand_desc,
      VIEW_AG.brand_variant_desc,
      VIEW_AG.creative_desc,
      VIEW_AG.brand_code,
      VIEW_AG.pcc_industry_group_desc,
      VIEW_AG.sow_per_ads_per_telecast,
      COMMERCIAL_AG.advertisement_duration_per_telecast,
      round(VIEW_AG.sow_per_ads_per_telecast / COMMERCIAL_AG.advertisement_duration_per_telecast)
                                                          AS ec_projection,
      100 * VIEW_AG.sow_per_ads_per_telecast / (COMMERCIAL_AG.advertisement_duration_per_telecast*INTAB.ue)
                                                          AS ec_rating

    FROM
      commercial_tuning_adj_ag AS VIEW_AG

    JOIN
      intab_df AS INTAB
    ON
      VIEW_AG.telecast_broadcast_date = INTAB.intab_period_start_date

    JOIN
      ecm_commercial_minutes_adj_ag AS COMMERCIAL_AG
    ON    TRUE
      AND VIEW_AG.telecast_broadcast_date  = COMMERCIAL_AG.broadcast_date
      AND VIEW_AG.program_id               = COMMERCIAL_AG.program_id
      AND VIEW_AG.telecast_id              = COMMERCIAL_AG.telecast_id
      AND VIEW_AG.advertisement_key        = COMMERCIAL_AG.advertisement_key
      AND VIEW_AG.advertisement_code       = COMMERCIAL_AG.advertisement_code
      AND VIEW_AG.commercial_start_time    = COMMERCIAL_AG.commercial_start_time

    WHERE
      intab.demo = '{demo}'

  --endsql''' \
    .format(demo = demo,
            viewing_df = viewing_df,
            ads_fact_dim_df = ads_fact_dim_df)
  
  return spark.sql(s)

# COMMAND ----------

##############################################################
### This python function computes ECM rating and projection 
### for a specified viewing file, ads_fact table, and demo 
### of interest
##############################################################

def ECM_computation_engine_non_rounded(demo,viewing_df,ads_fact_dim_df):
  
  s = '''--sql
    WITH
      commercial_tuning_adj_ag AS (
        SELECT
          VIEW.telecast_broadcast_date,
          VIEW.program_id,
          VIEW.telecast_id,
          VIEW.playback_type,
          VIEW.viewing_source_short_name,
          VIEW.commercial_start_time,
          VIEW.pod_value,
          VIEW.advertisement_key,
          VIEW.advertisement_code,
          VIEW.brand_desc,
          VIEW.brand_variant_desc,
          VIEW.creative_desc,
          VIEW.brand_code,
          VIEW.pcc_industry_group_desc,
          sum(VIEW.weight * VIEW.watched_advertisement_duration * INTAB.adj_factor) AS sow_per_ads_per_telecast

        FROM
          {viewing_df} AS VIEW
        JOIN
          intab_df AS INTAB
        ON
          VIEW.telecast_broadcast_date = INTAB.intab_period_start_date

        GROUP BY
          VIEW.telecast_broadcast_date, VIEW.program_id, VIEW.telecast_id,
          VIEW.playback_type, VIEW.viewing_source_short_name,
          VIEW.commercial_start_time, VIEW.pod_value, VIEW.advertisement_key, VIEW.advertisement_code,
          VIEW.brand_desc, VIEW.brand_variant_desc, VIEW.creative_desc, VIEW.brand_code,
          VIEW.pcc_industry_group_desc
      )

    SELECT
      VIEW_AG.telecast_broadcast_date,
      VIEW_AG.program_id,
      VIEW_AG.telecast_id,
      VIEW_AG.playback_type,
      '{demo}'                                            AS demo,
      VIEW_AG.viewing_source_short_name,
      VIEW_AG.commercial_start_time,
      VIEW_AG.pod_value,
      VIEW_AG.advertisement_key,
      VIEW_AG.advertisement_code,
      VIEW_AG.brand_desc,
      VIEW_AG.brand_variant_desc,
      VIEW_AG.creative_desc,
      VIEW_AG.brand_code,
      VIEW_AG.pcc_industry_group_desc,
      VIEW_AG.sow_per_ads_per_telecast,
      COMMERCIAL_AG.advertisement_duration_per_telecast,
      round(VIEW_AG.sow_per_ads_per_telecast / COMMERCIAL_AG.advertisement_duration_per_telecast)
                                                          AS ec_projection,
      100 * VIEW_AG.sow_per_ads_per_telecast / (COMMERCIAL_AG.advertisement_duration_per_telecast*INTAB.ue)
                                                          AS ec_rating

    FROM
      commercial_tuning_adj_ag AS VIEW_AG

    JOIN
      intab_df AS INTAB
    ON
      VIEW_AG.telecast_broadcast_date = INTAB.intab_period_start_date

    JOIN
      ecm_commercial_minutes_adj_ag AS COMMERCIAL_AG
    ON    TRUE
      AND VIEW_AG.telecast_broadcast_date  = COMMERCIAL_AG.broadcast_date
      AND VIEW_AG.program_id               = COMMERCIAL_AG.program_id
      AND VIEW_AG.telecast_id              = COMMERCIAL_AG.telecast_id
      AND VIEW_AG.advertisement_key        = COMMERCIAL_AG.advertisement_key
      AND VIEW_AG.advertisement_code       = COMMERCIAL_AG.advertisement_code
      AND VIEW_AG.commercial_start_time    = COMMERCIAL_AG.commercial_start_time

    WHERE
      intab.demo = '{demo}'

  --endsql''' \
    .format(demo = demo,
            viewing_df = viewing_df,
            ads_fact_dim_df = ads_fact_dim_df)
  
  return spark.sql(s)

# COMMAND ----------

####################################################################
### The function ECM_computation_engine is called for 
### the specific demo of interest. The output table will have
### ACM_projection (000) and ACM_rating columns. The ACM_computation_engine
### library assumes that columns with a very specific format exist in both 
### viewing and ads_fact tables
#####################################################################

field = [
  t.StructField("telecast_broadcast_date",             t.StringType(),  True),
  t.StructField("program_id",                          t.IntegerType(), True),
  t.StructField("telecast_id",                         t.LongType(),    True),
  t.StructField("playback_type",                       t.StringType(),  True),
  t.StructField("demo",                                t.StringType(),  True),
  t.StructField("viewing_source_short_name",           t.StringType(),  True),
  t.StructField("commercial_start_time",               t.StringType(),  True),
  t.StructField("pod_value",                           t.ShortType(),   True),
  t.StructField("advertisement_key",                   t.IntegerType(), True),
  t.StructField("advertisement_code",                  t.StringType(),  True),
  t.StructField("brand_desc",                          t.StringType(),  True),
  t.StructField("brand_variant_desc",                  t.StringType(),  True),
  t.StructField("creative_desc",                       t.StringType(),  True),
  t.StructField("brand_code",                          t.StringType(),  True),
  t.StructField("pcc_industry_group_desc",             t.StringType(),  True),
  t.StructField("sow_per_ads_per_telecast",            t.FloatType(),   True),
  t.StructField("advertisement_duration_per_telecast", t.IntegerType(), True),
  t.StructField("ec_projection",                       t.FloatType(),   True),
  t.StructField("ec_rating",                           t.FloatType(),   True)
]

ecm_schema = t.StructType(field)
ecm_table = spark.createDataFrame([], ecm_schema) 


for demo in demos_of_interest:
  if is_rounded:
    ecm_table_part = ECM_computation_engine_rounded(demo, 'pb_program_coded_stn_demo', 'ads_fact_dim')
  else:
    ecm_table_part = ECM_computation_engine_non_rounded(demo, 'pb_program_coded_stn_demo_ads', 'ads_fact_dim')
  
  ecm_table = ecm_table.union(ecm_table_part)
    
    

ecm_table.createOrReplaceTempView('ecm_table')

# COMMAND ----------

####################################################################################
### Here a datafram with position within a pod for ecach commercial is defined
### the outputed data frame's format is very similar to ads_fact_dim table's
### format
####################################################################################

s = '''--sql
  SELECT 
    broadcast_date,
    program_id,
    telecast_id,
    advertisement_key,
    advertisement_code,
    commercial_start_time,
    pod_value,
    brand_desc,
    brand_variant_desc,
    creative_desc,
    brand_code,
    pcc_industry_group_desc,
    advertisement_duration_per_telecast,
    row_number() OVER (
      PARTITION BY
        broadcast_date,
        telecast_id,
        program_id,
        pod_value
      ORDER BY
        commercial_start_time
    )
                               AS position_in_pod
  FROM 
    ecm_commercial_minutes_adj_ag

  --endsql'''

commercial_pos_in_pod_df = spark.sql(s)

commercial_pos_in_pod_df.cache()
commercial_pos_in_pod_df.createOrReplaceTempView("commercial_pos_in_pod_df")

# COMMAND ----------

###########################################################################################
### The commercial_pos_in_pod_df table created in the previous cell is expanded here by 
### playback_type and demo. In other words, for every row in commercial_pos_in_pod_df table
### we will increase the number of rows to (number of demos) * (number of playback_type) rows.
### The reason is that since the ecm table is a table by demo/playback_type, we need to have 
### commercial_pos_in_pod_df table in a consistent format
#############################################################################################

commercial_pos_in_pod_df_enhanced = spark.createDataFrame([],ecm_schema) 
s = '''--sql
  SELECT
    broadcast_date                        AS telecast_broadcast_date,
    program_id,
    telecast_id,
    '{playback}'                          AS playback_type,
    '{demo}'                              AS demo,
    NULL                                  AS viewing_source_short_name,
    commercial_start_time,
    pod_value,
    advertisement_key,
    advertisement_code,
    brand_desc,
    brand_variant_desc,
    creative_desc,
    brand_code,
    pcc_industry_group_desc,
    NULL                                  AS sow_per_ads_per_telecast,
    advertisement_duration_per_telecast,
    NULL                                  AS ec_projection,
    NULL                                  AS ec_rating
    
  FROM
    commercial_pos_in_pod_df

  --endsql'''

for demo in demos_of_interest:
  for playback in playback_type_list:
    commercial_pos_in_pod_df_enhanced_part = spark.sql(s.format(playback = playback,
                                                                demo = demo))
    commercial_pos_in_pod_df_enhanced = commercial_pos_in_pod_df_enhanced.union(commercial_pos_in_pod_df_enhanced_part)
  
commercial_pos_in_pod_df_enhanced.createOrReplaceTempView("commercial_pos_in_pod_df_enhanced")

# COMMAND ----------

###############################################################################
### Here ECM table is joined with commercial_pos_in_pod_df_enhanced table
### As a result of this join, we create the ECM of 0 for all commercials 
### that are absent in the ECM table
###############################################################################

s = '''--sql
  SELECT
    ADS.telecast_broadcast_date,
    ADS.program_id,
    ADS.telecast_id,
    ADS.playback_type,
    ADS.demo,
    coalesce(ECM.viewing_source_short_name, 'Zero Audience') AS viewing_source_short_name,
    ADS.commercial_start_time,
    ADS.pod_value,
    ADS.advertisement_key,
    ADS.advertisement_code,
    ADS.brand_desc,
    ADS.brand_variant_desc,
    ADS.creative_desc,
    ADS.brand_code,
    ADS.pcc_industry_group_desc,
    coalesce(ECM.sow_per_ads_per_telecast, 'Zero Audience')  AS sow_per_ads_per_telecast,
    ADS.advertisement_duration_per_telecast,
    coalesce(ECM.ec_projection, 0.0)                         AS ec_projection,
    coalesce(ECM.ec_rating, 0.0)                             AS ec_rating
    
  FROM
    commercial_pos_in_pod_df_enhanced AS ADS

  FULL OUTER JOIN
    ecm_table AS ECM
  ON    TRUE
    AND ADS.telecast_broadcast_date  = ECM.telecast_broadcast_date
    AND ADS.program_id               = ECM.program_id
    AND ADS.telecast_id              = ECM.telecast_id
    AND ADS.advertisement_key        = ECM.advertisement_key
    AND ADS.advertisement_code       = ECM.advertisement_code
    AND ADS.commercial_start_time    = ECM.commercial_start_time
    AND ADS.demo                     = ECM.demo
    AND ADS.playback_type            = ECM.playback_type

  --endsql'''

ecm_table_enhanced = spark.sql(s)
  
ecm_table_enhanced.createOrReplaceTempView("ecm_table_enhanced")
