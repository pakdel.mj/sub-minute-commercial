# Databricks notebook source
# MAGIC %md
# MAGIC # Part 2 : Compute Adjustment Factor for Different Demos
# MAGIC 
# MAGIC 
# MAGIC **Input Tables and Views**  
# MAGIC * `TAM_NPM_MCH_TV_PROD.national_tv_weighting_control_universe_estimate`
# MAGIC * `intab_period`
# MAGIC * `TAM_NPM_MCH_TV_PROD.metered_tv_household_location_intab_and_weight`
# MAGIC * `TAM_NPM_MCH_TV_PROD.metered_tv_household_location_person_intab_and_weight`
# MAGIC * `TAM_NPM_MCH_TV_PROD.metered_tv_person`
# MAGIC 
# MAGIC ** Output views**  
# MAGIC * `hh_ue`          (unreused)
# MAGIC * `hh_sow`         (unreused)
# MAGIC * `pp_wc_ue`
# MAGIC * `pp_demo_ue`     (unreused)
# MAGIC * `pp_char`        (unreused)
# MAGIC * `pp_char_demo`
# MAGIC * `pp_demo_sow`    (unreused)
# MAGIC * `intab_df`

# COMMAND ----------

# MAGIC %md
# MAGIC ### Compute UEs for Demos of Interest

# COMMAND ----------

################################################################
### These query pulls the UE for different demos from 
### the MDL for a specific measurement period
################################################################

hh_ue_query = """--sql
  SELECT
    universe_estimate      AS ue,
    effective_start_date,
    effective_end_date
  FROM
    TAM_NPM_MCH_TV_PROD.national_tv_weighting_control_universe_estimate
  WHERE TRUE
    AND effective_start_date         <= '{national_tv_broadcast_end_date}'
    AND effective_end_date           >= '{national_tv_broadcast_start_date}'
    AND household_or_person_code      = 'H'
    AND weighting_control_id          =  9999
    AND universe_estimate_type_code   =  1
    AND released_for_processing_flag  = 'Y'

  --endsql""" \
    .format(national_tv_broadcast_start_date = national_tv_broadcast_start_date,
            national_tv_broadcast_end_date   = national_tv_broadcast_end_date)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Compute Sum of Weights (SOW) for Demos of Interest

# COMMAND ----------

########################################################################
### In this query, persons characteristics table is pulled from the 
### MDL for a specified date range. extended homes, long term and short
### term visitors are removed. Only intab NPM homes and homes with TV tunings are
### pulled
########################################################################

hh_sow_query = '''--sql
  SELECT
    PERIOD.intab_period_start_date,
    sum(INTAB_PER.location_weight)  AS sow_per_day

  FROM (
      SELECT
        intab_period_id,
        intab_period_start_date
      FROM
        intab_period
      WHERE TRUE
        AND intab_period_start_date BETWEEN '{national_tv_broadcast_start_date}'
                                        AND '{national_tv_broadcast_end_date}'
    ) AS PERIOD

  JOIN (
      SELECT
        location_weight,
        intab_period_id
      FROM
        TAM_NPM_MCH_TV_PROD.metered_tv_household_location_intab_and_weight
      WHERE TRUE
        AND sample_collection_method_key       =  7   -- NPM SAMPLE
        AND media_device_category_code         =  1   -- 1:TV Tuning 2:PC Tuning
        AND is_intab_indicator                 =  1   -- keeping intab homes
        AND location_media_consumer_type_code  =  1   -- removing extended homes
    ) AS INTAB_PER
  ON    TRUE
    AND INTAB_PER.intab_period_id                    =  PERIOD.intab_period_id
  
  GROUP BY
    PERIOD.intab_period_start_date

  --endsql''' \
    .format(national_tv_broadcast_start_date = national_tv_broadcast_start_date,
            national_tv_broadcast_end_date   = national_tv_broadcast_end_date)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Join Demo UEs and Demo SOW Tables 

# COMMAND ----------

############################################################
### In this query, the table with UE of different demos
### is joined with the table with SOWs of different demos
############################################################

hh_ue_sow_query = '''--sql
SELECT
  SOW.intab_period_start_date,
  'HH'                            AS demo,
  SOW.sow_per_day,
  UE.ue,
  round(UE.ue/SOW.sow_per_day, 6) AS adj_factor
FROM
  hh_sow AS SOW
JOIN
  hh_ue AS UE
ON    TRUE
  AND SOW.intab_period_start_date BETWEEN UE.effective_start_date
                                      AND UE.effective_end_date
  
  --endsql'''

# COMMAND ----------

################################################################
### These query pulls the UE for different demos from 
### the MDL for a specific measurement period
################################################################

pp_wc_ue_query = """--sql
  SELECT
    universe_estimate,
    weighting_control_id,
    effective_start_date,
    effective_end_date
  FROM
    TAM_NPM_MCH_TV_PROD.national_tv_weighting_control_universe_estimate
  WHERE TRUE
    AND effective_start_date               <= '{national_tv_broadcast_end_date}'
    AND effective_end_date                 >= '{national_tv_broadcast_start_date}'
    AND household_or_person_code            = 'P'
    AND universe_estimate_type_code         =  1
    AND released_for_processing_flag        = 'Y'
    AND weighting_control_id          BETWEEN  1
                                          AND  30

  --endsql""" \
    .format(national_tv_broadcast_start_date = national_tv_broadcast_start_date,
            national_tv_broadcast_end_date   = national_tv_broadcast_end_date)

# COMMAND ----------

##################################################################
### deomos_of_interest_dic specifies how UEs for different
### weighting control ids should be combined in order to produce
### UE for demo of interest
#################################################################

start_wc_ids = {
  '2': {'F': 1, 'M': 16},
  '6': {'F': 2, 'M': 17},
  '9': {'F': 3, 'M': 18},
  '12': {'F': 4, 'M': 19},
  '15': {'F': 5, 'M': 20},
  '18': {'F': 6, 'M': 21},
  '21': {'F': 7, 'M': 22},
  '25': {'F': 8, 'M': 23},
  '30': {'F': 9, 'M': 24},
  '35': {'F': 10, 'M': 25},
  '40': {'F': 11, 'M': 26},
  '45': {'F': 12, 'M': 27},
  '50': {'F': 13, 'M': 28},
  '55': {'F': 14, 'M': 29},
  '65': {'F': 15, 'M': 30}
}

end_wc_ids = {
  '5': {'F': 1, 'M': 16},
  '8': {'F': 2, 'M': 17},
  '11': {'F': 3, 'M': 18},
  '14': {'F': 4, 'M': 19},
  '17': {'F': 5, 'M': 20},
  '20': {'F': 6, 'M': 21},
  '24': {'F': 7, 'M': 22},
  '29': {'F': 8, 'M': 23},
  '34': {'F': 9, 'M': 24},
  '39': {'F': 10, 'M': 25},
  '44': {'F': 11, 'M': 26},
  '49': {'F': 12, 'M': 27},
  '54': {'F': 13, 'M': 28},
  '64': {'F': 14, 'M': 29},
  '999': {'F': 15, 'M': 30}
}

if not household_run:
  demos_of_interest_dic = {
    demo: 
      '((weighting_control_id BETWEEN {flow} AND {fhigh}) OR (weighting_control_id BETWEEN {mlow} AND {mhigh}))'
        .format(flow =start_wc_ids[age_limits[demo]['low' ]]['F'],
                fhigh=end_wc_ids  [age_limits[demo]['high']]['F'],
                mlow =start_wc_ids[age_limits[demo]['low' ]]['M'],
                mhigh=end_wc_ids  [age_limits[demo]['high']]['M'])
      if demo[0] == 'P' else
        'weighting_control_id BETWEEN {low} AND {high}'
          .format(low =start_wc_ids[age_limits[demo]['low' ]][demo[0]],
                  high=end_wc_ids  [age_limits[demo]['high']][demo[0]])
    for demo in demos_of_interest
  }

# COMMAND ----------

########################################################################
### In this query, using the demo_of_interest_dic, UEs of demos' 
### building blocks are combined in order to produce the UE
### of demo of interest.
########################################################################

pp_demo_ue_query = '''--sql
  SELECT
    '{demo}'                AS demo,
    effective_start_date,
    effective_end_date,
    sum(universe_estimate)  AS ue
  FROM
    pp_wc_ue
  WHERE TRUE
    AND {condition}
  GROUP BY
    effective_start_date,
    effective_end_date

  --endsql'''

# COMMAND ----------


########################################################################
### In this query, persons characteristics table is pulled from the 
### MDL for a specified date range. extended homes, long term and short
### term visitors are removed. Only intab NPM homes and homes with TV tunings are
### pulled
########################################################################

pp_char_query = '''--sql
  SELECT 
    PERIOD.intab_period_start_date,
    INTAB_PER.person_media_consumer_id,
    INTAB_PER.person_weight,
    CHARACTERISTICS.age,
    CHARACTERISTICS.gender_code,
    CHARACTERISTICS.is_long_term_visitor_flag,
    CHARACTERISTICS.is_short_term_visitor_flag

  FROM (
      SELECT
        intab_period_start_date,
        intab_period_id
      FROM
        intab_period
      WHERE TRUE
        AND intab_period_start_date BETWEEN '{national_tv_broadcast_start_date}'
                                        AND '{national_tv_broadcast_end_date}'
    ) AS PERIOD

  JOIN (
      SELECT
        person_media_consumer_id,
        person_weight,
        intab_period_id,
        person_media_consumer_key
      FROM
        TAM_NPM_MCH_TV_PROD.metered_tv_household_location_person_intab_and_weight
      WHERE TRUE
        AND sample_collection_method_key       =  7   -- NPM SAMPLE
        AND media_device_category_code         =  1   -- 1:TV Tuning 2:PC Tuning
        AND is_intab_indicator                 =  1   -- keeping intab homes
        AND location_media_consumer_type_code  = '1'  -- removing extended homes
    ) AS INTAB_PER
  ON    TRUE
    AND INTAB_PER.intab_period_id =  PERIOD.intab_period_id

  JOIN (
      SELECT
        age,
        gender_code,
        is_long_term_visitor_flag,
        is_short_term_visitor_flag,
        person_media_consumer_key
      FROM
        TAM_NPM_MCH_TV_PROD.metered_tv_person
      WHERE TRUE
        AND is_short_term_visitor_flag  = 'N'
        AND is_long_term_visitor_flag  IS  NULL
    ) AS CHARACTERISTICS
  ON    TRUE
    AND CHARACTERISTICS.person_media_consumer_key = INTAB_PER.person_media_consumer_key

  --endsql''' \
    .format(national_tv_broadcast_start_date = national_tv_broadcast_start_date,
            national_tv_broadcast_end_date   = national_tv_broadcast_end_date)

# COMMAND ----------

########################################################################
### In this query, using the demo_of_interest list, sow of each demo 
### is computed
########################################################################

pp_demo_sow_query = '''--sql
  SELECT
    intab_period_start_date,
    '{demo}'                  AS demo,
    sum(person_weight)        AS sow_per_day
  FROM
    pp_char_demo
  WHERE
    {demo} = 'Y'
  GROUP BY
    intab_period_start_date

  --endsql'''

# COMMAND ----------

############################################################
### In this query, the table with UE of different demos
### is joined with the table with SOWs of different demos
############################################################

pp_ue_sow_query = '''--sql
  SELECT
    SOW.intab_period_start_date,
    SOW.demo,
    SOW.sow_per_day,
    UE.ue,
    round(UE.ue/SOW.sow_per_day, 6) AS adj_factor
  FROM
    pp_demo_sow AS SOW
  JOIN
    pp_demo_ue AS UE
  ON    TRUE
    AND SOW.demo                          = UE.demo
    AND SOW.intab_period_start_date BETWEEN UE.effective_start_date
                                        AND UE.effective_end_date
    
    --endsql'''

# COMMAND ----------

if household_run:
  hh_ue = spark.sql(hh_ue_query)
  hh_ue.createOrReplaceTempView("hh_ue")
  
  hh_sow = spark.sql(hh_sow_query)
  hh_sow.createOrReplaceTempView("hh_sow")
  
  intab_df = spark.sql(hh_ue_sow_query).cache()
  intab_df.createOrReplaceTempView('intab_df')
else:
  pp_wc_ue = spark.sql(pp_wc_ue_query).cache()
  pp_wc_ue.createOrReplaceTempView("pp_wc_ue")
  
  field = [
    t.StructField("demo",                 t.StringType(), True),
    t.StructField("effective_start_date", t.StringType(), True),
    t.StructField("effective_end_date",   t.StringType(), True),
    t.StructField("ue",                   t.LongType(),   True)
  ]
  schema = t.StructType(field)

  pp_demo_ue = spark.createDataFrame([], schema)
  for demo in demos_of_interest_dic:
    pp_demo_ue_part = spark.sql(pp_demo_ue_query.format(demo = demo,condition = demos_of_interest_dic[demo]))
    pp_demo_ue = pp_demo_ue.union(pp_demo_ue_part)
  pp_demo_ue.createOrReplaceTempView("pp_demo_ue")
  
  pp_char = spark.sql(pp_char_query)
  pp_char.createOrReplaceTempView("pp_char")

  pp_char_demo = append_demo_indicator('pp_char').cache()
  pp_char_demo.createOrReplaceTempView("pp_char_demo")
  
  field = [
    t.StructField("intab_period_start_date", t.StringType(),  True),
    t.StructField("demo",                    t.StringType(),  True),
    t.StructField("sow_per_day",             t.LongType(),    True)
  ]
  schema = t.StructType(field)

  pp_demo_sow = spark.createDataFrame([], schema)
  for demo in demos_of_interest:
    pp_demo_sow_part = spark.sql(pp_demo_sow_query.format(demo = demo))
    pp_demo_sow = pp_demo_sow.union(pp_demo_sow_part)
  pp_demo_sow.createOrReplaceTempView('pp_demo_sow')
  
  intab_df = spark.sql(pp_ue_sow_query).cache()
  intab_df.createOrReplaceTempView('intab_df')
