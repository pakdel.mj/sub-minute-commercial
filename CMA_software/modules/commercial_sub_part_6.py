# Databricks notebook source
# MAGIC %md
# MAGIC # Part 6: Join ACM and ECM Tables
# MAGIC 
# MAGIC 
# MAGIC **Input Tables and Views**  
# MAGIC * `ads_fact_dim`
# MAGIC * `commercial_pos_in_pod_df`
# MAGIC * `ecm_table_enhanced`
# MAGIC * `acm_table_enhanced`
# MAGIC * `intab_df`
# MAGIC 
# MAGIC **Output Views**  
# MAGIC * `max_pod_in_telecast_df`       (local)
# MAGIC * `commercial_counts_in_pod_df`  (local)
# MAGIC * `ecm_table_with_pod_pos`
# MAGIC * `ecm_acm_join`                 (local)
# MAGIC * `acm_ecm_table`                (output)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Compute Positon within a Pod for Commercials

# COMMAND ----------

##############################################################################
### Column max_pod_in_telecast shows the maximum pod value for a specific 
### telecast. This column can be used to decided if an ads is early or late 
### within a telecast.
###############################################################################

max_pod_in_telecast_df = \
  spark.sql('''--sql
    SELECT 
      broadcast_date,
      program_id,
      telecast_id,
      max(pod_value) AS max_pod_in_telecast
    FROM
      ads_fact_dim
    GROUP BY
      broadcast_date,
      program_id,
      telecast_id
      
    --endsql''')

max_pod_in_telecast_df.createOrReplaceTempView('max_pod_in_telecast_df') 

# COMMAND ----------

##############################################################################
### Column commercial_counts_in_pod specified the number of commercials 
### per telecast/pod. This column can be used to  decide if an ad is early 
### or late within a specific pod.
###############################################################################

commercial_counts_in_pod_df = \
  spark.sql('''--sql
    SELECT 
      broadcast_date,
      program_id,
      telecast_id,
      pod_value,
      max(position_in_pod) AS commercial_counts_in_pod
    FROM 
      commercial_pos_in_pod_df
    GROUP BY
      broadcast_date,
      program_id,
      telecast_id,
      pod_value
      
    --endsql''')

commercial_counts_in_pod_df.createOrReplaceTempView('commercial_counts_in_pod_df')

# COMMAND ----------

####################################################################################
### Here columns max_pod_in_telecast and commercial_counts_in_pod are added 
### to the viewing table. 
####################################################################################

s = '''--sql
  SELECT 
    ECM.*,
    POD_COUNT.max_pod_in_telecast,
    ADS_COUNT.commercial_counts_in_pod
  FROM
    ecm_table_enhanced AS ECM

  JOIN
    max_pod_in_telecast_df AS POD_COUNT
  ON    TRUE
    AND ECM.telecast_broadcast_date = POD_COUNT.broadcast_date
    AND ECM.program_id              = POD_COUNT.program_id
    AND ECM.telecast_id             = POD_COUNT.telecast_id

  JOIN
    commercial_counts_in_pod_df AS ADS_COUNT
  ON    TRUE
    AND ECM.telecast_broadcast_date = ADS_COUNT.broadcast_date
    AND ECM.program_id              = ADS_COUNT.program_id
    AND ECM.telecast_id             = ADS_COUNT.telecast_id
    AND ECM.pod_value               = ADS_COUNT.pod_value

  --endsql'''

ecm_table_with_pod_pos = spark.sql(s).cache()
ecm_table_with_pod_pos.createOrReplaceTempView("ecm_table_with_pod_pos")

# COMMAND ----------

##############################################################################
### Here the ACM and ECM tables are joined. The resulted table is the master
### dataset which is outputed as the final result of this notebook
##############################################################################

s = '''--sql
  WITH
    ecm_acm_join AS (
      SELECT
        ECM.telecast_broadcast_date,
        ECM.program_id,
        ECM.telecast_id,
        ECM.playback_type,
        ECM.demo,
        ACM.originator_common_service_name,
        ACM.program_name,
        ACM.telecast_name,
        ACM.telecast_broadcast_date_days_of_week,
        ACM.report_start_time_ny,
        ECM.viewing_source_short_name,
        ACM.program_summary_type_desc,
        ECM.commercial_start_time,
        ECM.advertisement_duration_per_telecast AS ad_duration,
        ECM.pod_value,
        ECM.max_pod_in_telecast,
        POD_POS.position_in_pod,
        ECM.commercial_counts_in_pod,
        ECM.advertisement_key,
        ECM.advertisement_code,
        ECM.brand_code,
        ECM.brand_desc,
        ECM.brand_variant_desc,
        ECM.creative_desc,
        INTAB.ue,
        ECM.pcc_industry_group_desc,
        ACM.ac_projection,
        ACM.ac_rating,
        ECM.ec_projection,
        ECM.ec_rating

      FROM 
        ecm_table_with_pod_pos AS ECM

      LEFT JOIN  -- may or may not have an ACM match
        acm_table_enhanced     AS ACM
      ON    TRUE
        AND ECM.telecast_broadcast_date   = ACM.telecast_broadcast_date
        AND ECM.program_id                = ACM.program_id
        AND ECM.telecast_id               = ACM.telecast_id
        AND ECM.viewing_source_short_name = ACM.viewing_source_short_name
        AND ECM.demo                      = ACM.demo
        AND ECM.playback_type             = ACM.playback_type

      JOIN
        commercial_pos_in_pod_df AS POD_POS
      ON    TRUE
        AND ECM.telecast_broadcast_date = POD_POS.broadcast_date
        AND ECM.program_id              = POD_POS.program_id
        AND ECM.telecast_id             = POD_POS.telecast_id
        AND ECM.advertisement_key       = POD_POS.advertisement_key
        AND ECM.advertisement_code      = POD_POS.advertisement_code
        AND ECM.pod_value               = POD_POS.pod_value
        AND ECM.commercial_start_time   = POD_POS.commercial_start_time

      JOIN
        intab_df AS INTAB
      ON    TRUE
        AND ECM.telecast_broadcast_date = INTAB.intab_period_start_date
        AND ECM.demo                    = INTAB.demo
    )


  -- telecasts with zero tuning and non-zero ad minutes
  SELECT 
    ECM.telecast_broadcast_date,
    ECM.program_id,
    ECM.telecast_id,
    ECM.playback_type,
    ECM.demo,
    ACM.originator_common_service_name,
    ACM.program_name,
    ACM.telecast_name,
    ACM.telecast_broadcast_date_days_of_week,
    ACM.report_start_time_ny,
    ACM.viewing_source_short_name,  -- ACM instead of ECM
    ACM.program_summary_type_desc,
    ECM.commercial_start_time,
    ECM.ad_duration,
    ECM.pod_value,
    ECM.max_pod_in_telecast,
    ECM.position_in_pod,
    ECM.commercial_counts_in_pod,
    ECM.advertisement_key,
    ECM.advertisement_code,
    ECM.brand_code,
    ECM.brand_desc,
    ECM.brand_variant_desc,
    ECM.creative_desc,
    ECM.ue,
    ECM.pcc_industry_group_desc,
    ACM.ac_projection,
    ACM.ac_rating,
    ECM.ec_projection,
    ECM.ec_rating
    
  FROM ecm_acm_join AS ECM

  JOIN
    acm_table_enhanced AS ACM
  ON    TRUE
    AND ECM.ac_projection          IS NULL

    AND ECM.telecast_broadcast_date  = ACM.telecast_broadcast_date
    AND ECM.program_id               = ACM.program_id
    AND ECM.telecast_id              = ACM.telecast_id
    AND ECM.demo                     = ACM.demo
    AND ECM.playback_type            = ACM.playback_type

    
  UNION ALL


  -- all other telecasts
  SELECT
    *

  FROM ecm_acm_join

  WHERE TRUE
    AND ac_projection IS NOT NULL

  --endsql'''

acm_ecm_table = spark.sql(s)
acm_ecm_table.createOrReplaceTempView("acm_ecm_table")
acm_ecm_table.cache().count()

# COMMAND ----------

#############################################################
### Find distinct viewing sources per data/program/telecast/
### playback_type/demo
#############################################################
field = [
  t.StructField("telecast_broadcast_date",              t.StringType(),  True),
  t.StructField("program_id",                           t.IntegerType(), True),
  t.StructField("telecast_id",                          t.LongType(),    True),
  t.StructField("playback_type",                        t.StringType(),  True),
  t.StructField("viewing_source_short_name",            t.StringType(),  True),
  t.StructField("demo",                                 t.StringType(),  True)
]

def unique_viewing_sources_query(demo, viewing_df):
  s = """--sql
    SELECT DISTINCT
      telecast_broadcast_date,
      program_id,
      telecast_id,
      playback_type,
      viewing_source_short_name,
      '{demo}' AS demo
      
    FROM
      {viewing_df}
    
    --endsql""" \
      .format(demo = demo,
              viewing_df = viewing_df)

  return spark.sql(s)
  

unique_viewing_sources_schema = t.StructType(field)
unique_viewing_sources = spark.createDataFrame([], unique_viewing_sources_schema) 

for demo in demos_of_interest:
  unique_viewing_sources_part = unique_viewing_sources_query(demo, 'pb_program_coded_stn_demo')
  unique_viewing_sources = unique_viewing_sources.union(unique_viewing_sources_part)

unique_viewing_sources.createOrReplaceTempView('unique_viewing_sources')

# COMMAND ----------

#############################################################
### Find distinct viewing sources per date/program/telecast/
### playback_type/demo/commercial
#############################################################

ads_by_call = spark.sql('''--sql
  SELECT
    ADS.*,
    SOURCE.viewing_source_short_name,
    SOURCE.playback_type,
    SOURCE.demo
  FROM
    ads_fact_summary AS ADS

  JOIN
    unique_viewing_sources AS SOURCE
  ON    TRUE
    AND SOURCE.telecast_broadcast_date = ADS.broadcast_date
    AND SOURCE.program_id              = ADS.program_id
    AND SOURCE.telecast_id             = ADS.telecast_id
    
  JOIN
    telecast_details AS TEL
  ON    TRUE
    AND TEL.telecast_broadcast_date = ADS.broadcast_date
    AND TEL.program_id              = ADS.program_id
    AND TEL.telecast_id             = ADS.telecast_id

  --endsql''')

ads_by_call.createOrReplaceTempView("ads_by_call")
ads_by_call.cache().count()

# COMMAND ----------

###########################################################
## distinct date/prgram/telecast/playback/demo are pulled
## from acm_ecm_table
###########################################################

acm_table_distinct = acm_table_enhanced \
  .select('telecast_broadcast_date',  # acm_ecm_table
          'telecast_broadcast_date_days_of_week',

          'playback_type',
          'demo',

          'viewing_source_short_name',
          'originator_common_service_name',

          'program_id',
          'program_name',
          'program_summary_type_desc',

          'telecast_id',
          'telecast_name',
          'report_start_time_ny',

          'ac_projection',
          'ac_rating').distinct()
                        
acm_table_distinct.createOrReplaceTempView('acm_table_distinct')

# COMMAND ----------

##############################################################
## Here commercials that are distributed across multiple 
## viewing sources and have 0 ECs are created
##############################################################

acm_ecm_table_with_zero_ec_commercial = \
  spark.sql('''--sql
    SELECT
      SOURCE.broadcast_date AS telecast_broadcast_date,
      ACM.telecast_broadcast_date_days_of_week,

      SOURCE.playback_type,
      SOURCE.demo,
      INTAB.ue,

      ACM.originator_common_service_name,
      SOURCE.viewing_source_short_name,

      SOURCE.program_id,
      ACM.program_name,
      ACM.program_summary_type_desc,

      SOURCE.telecast_id,
      ACM.telecast_name,
      ACM.report_start_time_ny,

      SOURCE.commercial_start_time,
      SOURCE.advertisement_duration AS ad_duration,
      SOURCE.pod_value,
      POD_COUNT.max_pod_in_telecast,
      POD_POS.position_in_pod,
      ADS_COUNT.commercial_counts_in_pod,
      SOURCE.advertisement_key,
      SOURCE.advertisement_code,
      SOURCE.brand_code,
      SOURCE.brand_desc,
      SOURCE.brand_variant_desc,
      SOURCE.creative_desc,
      SOURCE.pcc_industry_group_desc,

      ACM.ac_projection,
      ACM.ac_rating,
      0.0 AS ec_projection,
      0.0 AS ec_rating

    FROM
      ads_by_call AS SOURCE

    LEFT ANTI JOIN
      acm_ecm_table AS EVENT
    ON    TRUE
      AND EVENT.telecast_broadcast_date   = SOURCE.broadcast_date
      AND EVENT.program_id                = SOURCE.program_id
      AND EVENT.telecast_id               = SOURCE.telecast_id
      AND EVENT.demo                      = SOURCE.demo
      AND EVENT.viewing_source_short_name = SOURCE.viewing_source_short_name
      AND EVENT.playback_type             = SOURCE.playback_type
      AND EVENT.advertisement_key         = SOURCE.advertisement_key
      AND EVENT.advertisement_code        = SOURCE.advertisement_code
      AND EVENT.pod_value                 = SOURCE.pod_value
      AND EVENT.commercial_start_time     = SOURCE.commercial_start_time

    JOIN
      acm_table_distinct AS ACM
    ON    TRUE
      AND SOURCE.broadcast_date            = ACM.telecast_broadcast_date
      AND SOURCE.playback_type             = ACM.playback_type
      AND SOURCE.demo                      = ACM.demo
      AND SOURCE.viewing_source_short_name = ACM.viewing_source_short_name
      AND SOURCE.program_id                = ACM.program_id
      AND SOURCE.telecast_id               = ACM.telecast_id

    JOIN
      max_pod_in_telecast_df AS POD_COUNT
    ON    TRUE
      AND SOURCE.broadcast_date = POD_COUNT.broadcast_date
      AND SOURCE.program_id     = POD_COUNT.program_id
      AND SOURCE.telecast_id    = POD_COUNT.telecast_id

    JOIN
      commercial_counts_in_pod_df AS ADS_COUNT
    ON    TRUE
      AND SOURCE.broadcast_date = ADS_COUNT.broadcast_date
      AND SOURCE.program_id     = ADS_COUNT.program_id
      AND SOURCE.telecast_id    = ADS_COUNT.telecast_id
      AND SOURCE.pod_value      = ADS_COUNT.pod_value

    JOIN
      commercial_pos_in_pod_df AS POD_POS
    ON    TRUE
      AND SOURCE.broadcast_date        = POD_POS.broadcast_date
      AND SOURCE.program_id            = POD_POS.program_id
      AND SOURCE.telecast_id           = POD_POS.telecast_id
      AND SOURCE.advertisement_key     = POD_POS.advertisement_key
      AND SOURCE.advertisement_code    = POD_POS.advertisement_code
      AND SOURCE.pod_value             = POD_POS.pod_value
      AND SOURCE.commercial_start_time = POD_POS.commercial_start_time

    JOIN
      intab_df AS INTAB
    ON    TRUE
      AND SOURCE.broadcast_date = INTAB.intab_period_start_date
      AND SOURCE.demo           = INTAB.demo

    --endsql''')

# COMMAND ----------

#####################################################
## Here the original acm_ecm_table is appended to
## the acm_ecm_table_with_zero_ec_commercial dataset
#####################################################

acm_ecm_table2 = \
  (acm_ecm_table) \
    .union(acm_ecm_table_with_zero_ec_commercial.select(*acm_ecm_table.columns))

acm_ecm_table2.createOrReplaceTempView('acm_ecm_table2')

# COMMAND ----------

#########################################################
## here for those telecasts in acm_ecm_table that are
## also present in no_match table will be replaced with
## value "removed" for ratings
##########################################################

if drop_corrupt_mop_rows:
  s = '''--sql
    SELECT
      ac_ec.telecast_broadcast_date,
      ac_ec.program_id,
      ac_ec.telecast_id,
      ac_ec.playback_type,demo,
      ac_ec.originator_common_service_name,
      ac_ec.program_name,
      ac_ec.telecast_name,
      ac_ec.telecast_broadcast_date_days_of_week,
      ac_ec.report_start_time_ny,
      ac_ec.viewing_source_short_name,
      ac_ec.program_summary_type_desc,   
      ac_ec.commercial_start_time,
      ac_ec.ad_duration,
      ac_ec.pod_value,
      ac_ec.max_pod_in_telecast,
      ac_ec.position_in_pod,
      ac_ec.commercial_counts_in_pod,
      ac_ec.advertisement_key,
      ac_ec.advertisement_code,
      ac_ec.brand_code,
      ac_ec.brand_desc,
      ac_ec.brand_variant_desc,
      ac_ec.creative_desc,
      ac_ec.ue,
      ac_ec.pcc_industry_group_desc,
      CASE WHEN no_match.ti IS NOT NULL THEN 'removed' ELSE ac_ec.ac_projection END AS ac_projection,
      CASE WHEN no_match.ti IS NOT NULL THEN 'removed' ELSE ac_ec.ac_rating END AS ac_rating,
      CASE WHEN no_match.ti IS NOT NULL THEN 'removed' ELSE ac_ec.ec_projection END AS ec_projection,
      CASE WHEN no_match.ti IS NOT NULL THEN 'removed' ELSE ac_ec.ec_rating END AS ec_rating
    FROM
      acm_ecm_table2 AS ac_ec

    LEFT JOIN
      no_match
    ON    TRUE
      AND ac_ec.telecast_broadcast_date = no_match.bd
      AND ac_ec.program_id              = no_match.pi
      AND ac_ec.telecast_id             = no_match.ti

    --endsql'''
    
  acm_ecm_table2 = spark.sql(s)
  acm_ecm_table2.createOrReplaceTempView("acm_ecm_table")
  acm_ecm_table2.cache().count()
