# Databricks notebook source
# MAGIC %md
# MAGIC # Part 3: Read Commercial Information from the MDL
# MAGIC 
# MAGIC 
# MAGIC **Input Tables and Views**  
# MAGIC * `TAM_NPM_MCH_TV_PROD.advertisement_fact`
# MAGIC * `TAM_NPM_EDW_TV_PROD.pod_dimension`
# MAGIC * `TAM_NPM_MCH_TV_PROD.advertisement_dimension`
# MAGIC 
# MAGIC **Output Views**  
# MAGIC * `ads_fact`      (unreused)
# MAGIC * `ads_fact_dim`

# COMMAND ----------

#######################################################################
### the commercial information for a specified date range is read from
### the tam_npm_mch_tv_prod.advertisement_fact table. pod_value column 
### is added from the tam_npm_edw_tv_prod.pod_DIMENSION table to it
#######################################################################

query = '''--sql
  SELECT
    FACT.broadcast_date,
    FACT.advertisement_key,
    FACT.telecast_key,
    FACT.pod_key,
    FACT.mop_key,
    FACT.minute_of_program,
    FACT.advertisement_duration,
    FACT.commercial_start_time_key,
    FACT.commercial_start_time,
    60*(60*HOUR(FACT.commercial_start_time)
          + MINUTE(FACT.commercial_start_time))
      + SECOND(FACT.commercial_start_time)       AS commercial_start_time_ny_second,
    FACT.first_minute_indicator,
    FACT.load_id,
    FACT.advertisement_type_code,
    FACT.program_distributor_id,
    FACT.program_id,
    FACT.telecast_id,
    FACT.complex_id,
    EDW_POD.pod_value

  FROM
    TAM_NPM_MCH_TV_PROD.advertisement_fact AS FACT

  JOIN
    TAM_NPM_EDW_TV_PROD.pod_dimension AS EDW_POD
  ON
    FACT.pod_key = EDW_POD.pod_key

  WHERE TRUE
    AND FACT.broadcast_date BETWEEN '{national_tv_broadcast_start_date}'
                                AND '{national_tv_broadcast_end_date}'
    AND FACT.complex_id           =  0
    
  --endsql''' \
    .format(national_tv_broadcast_start_date = national_tv_broadcast_start_date,
            national_tv_broadcast_end_date = national_tv_broadcast_end_date)

ads_fact = spark.sql(query)

ads_fact.createOrReplaceTempView("ads_fact")

# COMMAND ----------

######################################################################
### some characteristics from the advertisement_dimension table are
### added to the commercial data in advertisement_fact table. All 
### commercials with promotional_flag of 1 and commercials without any
### creative description and parent_code of 2 are removed. 
######################################################################

query = '''--sql
  SELECT
    ADS.*,
    ADS_DIM.brand_desc,
    ADS_DIM.brand_variant_desc,
    ADS_DIM.promotional_flag,
    ADS_DIM.adviews_load_id,
    ADS_DIM.advertisement_code,
    ADS_DIM.commercial_type_code,
    ADS_DIM.creative_desc,
    ADS_DIM.product_desc,
    ADS_DIM.brand_code,
    ADS_DIM.pib_major_group_code,
    ADS_DIM.pib_major_group_desc,
    ADS_DIM.pcc_industry_group_desc
    
  FROM
    ads_fact AS ADS

  JOIN
    TAM_NPM_MCH_TV_PROD.advertisement_dimension AS ADS_DIM
  ON
    ADS.advertisement_key = ADS_DIM.advertisement_key
 
  WHERE TRUE
    AND      ADS_DIM.promotional_flag = 1
    AND NOT (ADS_DIM.creative_desc = 'NO TITLE AVAILABLE' AND parent_code = 2)

  --endsql'''

ads_fact_dim = spark.sql(query)
ads_fact_dim.createOrReplaceTempView("ads_fact_dim")

# COMMAND ----------

######################################################################
### program_start_time from the telecast_details table is
### added to the commercial data in ads_fact_dim table. 
######################################################################

query = '''--sql
  SELECT
    ADS.*,
    TEL.report_start_time_ny,

    CASE
      WHEN ADS.commercial_start_time < TEL.report_start_time_ny
        THEN concat(date_add(ADS.broadcast_date, 1), ' ', ADS.commercial_start_time)
      ELSE   concat(ADS.broadcast_date, ' ', ADS.commercial_start_time)
    END                                                        AS commercial_start_time_adj,

    concat(ADS.broadcast_date, ' ', TEL.report_start_time_ny)  AS report_start_time_ny_adj
    
  FROM
    ads_fact_dim AS ADS

  JOIN
    telecast_details AS TEL
  ON    TRUE
    AND ADS.broadcast_date = TEL.telecast_broadcast_date
    AND ADS.program_id     = TEL.program_id
    AND ADS.telecast_id    = TEL.telecast_id
    
  --endsql'''

ads_fact_dim2 = spark.sql(query)

ads_fact_dim2.createOrReplaceTempView("ads_fact_dim")

# COMMAND ----------

######################################################################
### minute of program for each commercial is computed as program_start_time
### minus commercial start time
######################################################################

if drop_corrupt_mop_rows:
  query = '''--sql
    SELECT
      ADS.*,
      floor((unix_timestamp(commercial_start_time_adj)
              - unix_timestamp(report_start_time_ny_adj))/60)
            + row_number   AS computed_minute_of_program
      
    FROM (
      SELECT
        *, 
        row_number() OVER (
          PARTITION BY
            broadcast_date,
            program_id,
            telecast_id,
            commercial_start_time 
          ORDER BY
            broadcast_date,
            program_id,
            telecast_id,
            commercial_start_time,
            first_minute_indicator desc,
            mop_key)          AS row_number 
      FROM
        ads_fact_dim) AS ADS
      
    --endsql'''

  ads_fact_dim3 = spark.sql(query).cache()
  ads_fact_dim3.createOrReplaceTempView("ads_fact_dim")

# COMMAND ----------

#######################################################################################
## Those commercials for which the computed minute of program is not the same
## as the available minute_of_program in the ads_fact table are identified
#######################################################################################

if drop_corrupt_mop_rows:
  no_match = \
    ads_fact_dim3.filter('minute_of_program != computed_minute_of_program') \
                 .select(f.col('broadcast_date').alias('bd'),
                         f.col('program_id').alias('pi'),
                         f.col('telecast_id').alias('ti')) \
                 .distinct()

  no_match.createOrReplaceTempView('no_match')
