# Databricks notebook source
# MAGIC %md
# MAGIC # Part 3 : Join Sub-minute and ad-fact Tables

# COMMAND ----------

#############################################################
### Find the commercila duratin as well as minimum and maximum 
### minute of program per telecast/commercial/pod_value
#############################################################
s = '''--sql
  SELECT 
    broadcast_date,
    program_id,
    telecast_id,
    advertisement_key,
    advertisement_code,
    commercial_start_time,
    pod_value,
    brand_desc,
    creative_desc,
    brand_code,
    brand_variant_desc,
    pcc_industry_group_desc,
    report_start_time_ny,
    commercial_start_time_adj,
    report_start_time_ny_adj,
    min(minute_of_program)      AS start_mop,
    max(minute_of_program)      AS end_mop,
    sum(advertisement_duration) AS advertisement_duration
  FROM
    ads_fact_dim
  GROUP BY
    broadcast_date,
    program_id,
    telecast_id,
    advertisement_key,
    advertisement_code,
    commercial_start_time,
    pod_value,
    brand_desc,
    brand_variant_desc,
    creative_desc,
    brand_code,
    pcc_industry_group_desc,
    report_start_time_ny,
    commercial_start_time_adj,
    report_start_time_ny_adj
  
  --endsql'''

ads_fact_summary = spark.sql(s)
ads_fact_summary.createOrReplaceTempView("ads_fact_summary")

# COMMAND ----------

##############################################################################
### Compute at which second of the program the commercial started and
### at which second it ended
##############################################################################

if not is_rounded:
  s = '''--sql
    SELECT
      *,
      unix_timestamp(commercial_start_time_adj) - 
        unix_timestamp(report_start_time_ny_adj) + 1
                                  AS computed_start_second_of_ad,
      unix_timestamp(commercial_start_time_adj) - 
        unix_timestamp(report_start_time_ny_adj) + advertisement_duration 
                                  AS computed_end_second_of_ad
    FROM
      ads_fact_summary

    --endsql'''

  ads_fact_summary2 = spark.sql(s)
  ads_fact_summary2.createOrReplaceTempView("ads_fact_summary2")

# COMMAND ----------

###########################################################################
### Add new varialbes adjusted_computed_start_second_of_ad and adusted_com
### puted_end_second_of_the program. These two variables specify how much
### of each commercial was tuned
##########################################################################

if not is_rounded:
  query = '''--sql
    SELECT
      *,
      adj_computed_end_second_of_ad - adj_computed_start_second_of_ad + 1
                                              AS watched_advertisement_duration
    FROM (
      SELECT
        VIEW.HH,
        VIEW.telecast_broadcast_date, 
        VIEW.source_installed_location_number,
        VIEW.source_media_device_number,
        VIEW.program_id,
        VIEW.telecast_id,
        VIEW.playback_type,
        VIEW.originator_common_service_name,
        VIEW.program_name,
        VIEW.telecast_name,
        VIEW.telecast_broadcast_date_days_of_week,
        VIEW.report_start_time_ny,
        VIEW.viewing_source_short_name,
        VIEW.program_summary_type_desc,
        VIEW.report_originator_lineup_key,
        VIEW.complex_id,
        VIEW.credit_start_of_viewing_ny_time,
        VIEW.credit_end_of_viewing_ny_time,
        VIEW.viewed_start_of_viewing_ny_time,
        VIEW.viewed_end_of_viewing_ny_time,
        VIEW.start_minute_of_program,
        VIEW.end_minute_of_program,
        VIEW.start_second_of_program,
        VIEW.end_second_of_program,
        VIEW.duration_in_seconds,
        VIEW.weight,
          
        ADS.advertisement_key,
        ADS.advertisement_code,
        ADS.commercial_start_time,
        ADS.advertisement_duration,
        ADS.pod_value,
        ADS.creative_desc,
        ADS.brand_desc,
        ADS.brand_variant_desc,
        ADS.brand_code,
        ADS.pcc_industry_group_desc,
        ADS.computed_start_second_of_ad,
        ADS.computed_end_second_of_ad,
        greatest(ADS.computed_start_second_of_ad,
                VIEW.start_second_of_program)      AS adj_computed_start_second_of_ad,
        least(ADS.computed_end_second_of_ad,
              VIEW.end_second_of_program)          AS adj_computed_end_second_of_ad

      FROM
        pb_program_coded_stn_demo AS VIEW
        
      JOIN
        ads_fact_summary2 AS ADS
      ON    TRUE
        AND ADS.broadcast_date               = VIEW.telecast_broadcast_date
        AND ADS.program_id                   = VIEW.program_id
        AND ADS.telecast_id                  = VIEW.telecast_id
        -- Only ads overlaping with the program
        AND ADS.computed_end_second_of_ad   >= VIEW.start_second_of_program
        AND ADS.computed_start_second_of_ad <= VIEW.end_second_of_program
    )
      
    --endsql'''

  pb_program_coded_stn_demo_ads = spark.sql(query).cache()
  pb_program_coded_stn_demo_ads.createOrReplaceTempView("pb_program_coded_stn_demo_ads")
