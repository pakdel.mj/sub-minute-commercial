# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC # Commercial Metrics Analysis
# MAGIC 
# MAGIC > This notebook could be executed to compare commercial ratings/impressions
# MAGIC   using minute level data between average commercial minute (ACM)
# MAGIC   and exact commercial minute (ECM) for a supplied date range and service name.
# MAGIC 
# MAGIC **PURPOSE:**
# MAGIC > * The ACM rating is calculated as the total duration weighted impressions
# MAGIC     for all commercial minutes across a telecast
# MAGIC     divided by the total number of commercial seconds.
# MAGIC     All commercials within the same telecast have identical ACM rating.
# MAGIC     The ECM rating is calculated as the total duration weighted impressions
# MAGIC     for each commercial within a telecast divided by the total number of seconds,
# MAGIC     devoted to that commercial.
# MAGIC     For the commercials that aired within only one minute of a telecast,
# MAGIC     ECM is the same as ratings/impression of that specific telecast minute.
# MAGIC     For commercials that cross multiple minutes, ECM is the duration weighted impression
# MAGIC     of all commercial minutes.
# MAGIC 
# MAGIC 
# MAGIC 
# MAGIC **USER INPUTS (NOTEBOOK WIDGETS):**
# MAGIC > 1. **Begin (yyyy-mm-dd)**
# MAGIC >    - The beginning of the desired measurement period,
# MAGIC        in the **yyyy-mm-dd** format
# MAGIC > 1. **End (yyyy-mm-dd)**
# MAGIC >    - The ending of the desired measurement period,
# MAGIC        in the **yyyy-mm-dd** format
# MAGIC > 1. **Service**
# MAGIC >    - Service types of interest, used in the analysis  
# MAGIC        Available service types are 'Network', 'Cable', 'Syndication' or a combination of these.
# MAGIC > 1. **Time-shifted viewing**
# MAGIC >    - playback type of interest;  
# MAGIC        Available playback types are 'live', 'live+3', 'live+7' or a combination of these.
# MAGIC > 1. **Rounded tuning data**
# MAGIC >    - type of tuning data to use;  
# MAGIC        If 'Yes' is selected, minute-level tuning data will be used.
# MAGIC        Otherwise, the analysis will use sub-minute-level data.
# MAGIC > 1. **Adjust commercial duration**
# MAGIC      - when using minute-level data, whether or not to fix minute commercial duration over 60 seconds
# MAGIC > 1. **Write output**
# MAGIC >    - whether or not write the output data to an S3 location
# MAGIC > 1. **Output folder**
# MAGIC >    - The folder on S3 that will be used as the parent folder for the output
# MAGIC        if 'Yes' is selected in **Write output**
# MAGIC  
# MAGIC 
# MAGIC **DATA SOURCES:**
# MAGIC > | schema                          | table                                                        |
# MAGIC   |---------------------------------|--------------------------------------------------------------|
# MAGIC   | tam_npm_mch_tv_prod             | report_originator_lineup                                     |
# MAGIC   | tam_npm_mch_tv_prod             | national_tv_content_originator                               |
# MAGIC   | tam_npm_mch_tv_prod             | program_summary_type                                         |
# MAGIC   | tam_npm_mch_tv_prod             | program_coded_metered_tv_person_media_engagement_event       |
# MAGIC   | tam_npm_mch_tv_prod             | metered_tv_household_location_intab_status                   |
# MAGIC   | tam_npm_mch_tv_prod             | intab_period                                                 |
# MAGIC   | tam_npm_mch_tv_prod             | metered_tv_person                                            |
# MAGIC   | tam_npm_mch_tv_prod             | metered_tv_household_location_person_intab_and_weight        |
# MAGIC   | parquet                         | station_mapping_table                                        |
# MAGIC   | tam_npm_mch_tv_prod             | national_tv_weighting_control_universe_estimate              |
# MAGIC   | tam_npm_mch_tv_prod             | advertisement_fact                                           |
# MAGIC   | tam_npm_mch_tv_prod             | advertisement_dimension                                      |
# MAGIC   | tam_npm_edw_tv_prod             | pod_dimension                                                |
# MAGIC 
# MAGIC **OUTPUT FORMAT:**
# MAGIC > | col_name                              | data_type  | ex_results                             |
# MAGIC   |----------------------------           |----------- |--------------------------------------- |
# MAGIC   | telecast_broadcast_date               | date       |  2018-03-01                            |
# MAGIC   | program_id                            | bigint     |  178501                                |
# MAGIC   | telecast_id                           | bigint     |  94224                                 |
# MAGIC   | playback_type                         | string     | 'live+7'                               |
# MAGIC   | demo                                  | string     | 'HH'                                   |
# MAGIC   | originator_common_service_name        | string     | 'Syndication'                          |
# MAGIC   | program_name                          | string     | 'ENTERTAINMENT TONIGHT(AT)'            |
# MAGIC   | telecast_name                         | string     | 'ENTERTAINMENT TONIGHT(AT)'            |
# MAGIC   | telecast_broadcast_date_days_of_week  | string     | 'Wednesday'                            |
# MAGIC   | report_start_time_ny                  | time       | '19:00:00'                             |
# MAGIC   | viewing_source_short_name             | string     |  ABC                                   |
# MAGIC   | program_summary_type_desc             | string     |  NEWS                                  |
# MAGIC   | commercial_start_time                 | time       | '19:58:23'                             |
# MAGIC   | ad_duration                           | short      |  15                                    |
# MAGIC   | pod_value                             | int        |  4                                     |
# MAGIC   | max_pod_in_telecast                   | int        |  7                                     |
# MAGIC   | position_in_pod                       | int        |  8                                     |
# MAGIC   | commercial_counts_in_pod              | int        |  12                                    |
# MAGIC   | advertisement_key                     | bigint     |  8115483                               |
# MAGIC   | advertisement_code                    | string     | '1E0888E605'                           |
# MAGIC   | brand_code                            | bigint     |  13080                                 |
# MAGIC   | creative_desc                         | string     | 'WOMAN/BABY/WOMAN OPENS INCUBATOR'     |
# MAGIC   | ue                                    | bigint     |  120640000                             |
# MAGIC   | pcc_industry_group_desc               | string     | 'BUSINESS & CONSUMER SVCS'             |
# MAGIC   | ac_projection                         | decimal    |  241.0 (000)                           |
# MAGIC   | ac_rating                             | decimal    |  0.1997334466180371                    |
# MAGIC   | ec_projection                         | decimal    |  242.0 (000)                           |
# MAGIC   | ec_rating                             | decimal    |  0.20049197612732095                   |
# MAGIC 
# MAGIC 
# MAGIC **EXAMPLE RUN:**
# MAGIC > 
# MAGIC | Widget               | Value                                                                            |
# MAGIC | ---                  | ---                                                                              |
# MAGIC | Begin (yyyy-mm-dd)   | `2019-04-01`                                                                     |
# MAGIC | End (yyyy-mm-dd)     | `2019-04-28`                                                                     |
# MAGIC | Service              | `Network,Syndication`                                                            |
# MAGIC | Time-shifted viewing | `live,live+3`                                                                    |
# MAGIC | Rounded tuning data  | `Yes`                                                                            |
# MAGIC | Write output         | `Yes`                                                                            |
# MAGIC | Output folder        | `s3://useast1-nlsn-w-digital-dsci-dev/projects/commercial_sub_minutes/pakdmj01/` |
# MAGIC 
# MAGIC **CODE DEVELOPERS:**
# MAGIC > * MJ PAKDEL
# MAGIC > * Arseny EGOROV

# COMMAND ----------

# MAGIC %md
# MAGIC ### Import Libraries

# COMMAND ----------

from __future__ import division

import pyspark.sql.functions as f
import pyspark.sql.types as t
from datetime import datetime, timedelta

# COMMAND ----------

# MAGIC %md
# MAGIC ### Input

# COMMAND ----------

# Use this cell to add/populate/remove the widgets for user input.
# After creating the widgets, click the gear icon to the right of the widgets,
# and choose 'Run Accessed Commands' in the dropdown menu.

# Start date
dbutils.widgets.text('national_tv_broadcast_start_date',
                     '2019-04-01',
                     'Begin (yyyy-mm-dd)')

# End date
dbutils.widgets.text('national_tv_broadcast_end_date',
                     '2019-04-28',
                     'End (yyyy-mm-dd)')

# Service types
dbutils.widgets.multiselect('originator_common_service_name',
                            'Network',
                            [
                              "Network",
                              "Cable",
                              "Syndication"
                            ],
                            'Service')

# Playback types
dbutils.widgets.multiselect('time_shifted_viewing',
                            'live',
                            [
                              'live',
                              'live+SD',
                              'live+3',
                              'live+7'],
                            'Time-shifted viewing')


# Whether to write the output
dbutils.widgets.dropdown('please_export',
                         'Yes',
                         [
                           'Yes',
                           'No'
                         ],
                         'Write output')

# Where to write the output
dbutils.widgets.text('output_folder',
                     's3://useast1-nlsn-w-digital-dsci-dev/projects/commercial_sub_minutes/pakdmj01/',
                     'Output folder')

dbutils.widgets.dropdown('execution_type',
                         'Rounded file, rounded methodology',
                         [
                           'Rounded file, rounded methodology',
                           'Non-rounded file, non-rounded methodology',
                           'Rounded file, non-rounded methodology'
                         ],
                         'Execution type')

dbutils.widgets.dropdown('adjust_commercial_duration',
                         'Yes',
                         [
                           'Yes',
                           'No'
                         ],
                         'Adjust commercial minute duration')

dbutils.widgets.dropdown('remove_split_stations',
                         'Yes',
                         [
                           'Yes',
                           'No'
                         ],
                         'Remove split stations')

dbutils.widgets.dropdown('drop_corrupt_mop_rows',
                         'Yes',
                         [
                           'Yes',
                           'No'
                         ],
                         'Drop corrupt MOP rows')

# COMMAND ----------

#######################################
#####    USER INPUTS (widgets)    #####
#######################################

national_tv_broadcast_start_date        = dbutils.widgets.get('national_tv_broadcast_start_date')
national_tv_broadcast_end_date          = dbutils.widgets.get('national_tv_broadcast_end_date')
metered_start_date                      = datetime.strftime(datetime.strptime(national_tv_broadcast_start_date, '%Y-%m-%d')
                                                              - timedelta(days=60),
                                                            '%Y-%m-%d')
viewing_end_date                        = datetime.strftime(datetime.strptime(national_tv_broadcast_end_date, '%Y-%m-%d')
                                                              + timedelta(days=8),
                                                            '%Y-%m-%d')

originator_common_service_name          = "('{}')".format(dbutils.widgets.get('originator_common_service_name').replace(",", "', '"))
playback_type_list                      = sorted(dbutils.widgets.get('time_shifted_viewing').split(','))
execution_type                          = dbutils.widgets.get('execution_type') 
remove_split_stations                   = dbutils.widgets.get('remove_split_stations') == 'Yes'

adjust_commercial_duration              = dbutils.widgets.get('adjust_commercial_duration') == 'Yes'
drop_corrupt_mop_rows                   = dbutils.widgets.get('drop_corrupt_mop_rows') == 'Yes'

please_export                           = dbutils.widgets.get('please_export') == 'Yes'
output_folder                           = dbutils.widgets.get('output_folder')

print('Start date                 :  {}'.format(national_tv_broadcast_start_date))
print('End date                   :  {}'.format(national_tv_broadcast_end_date))
print('Metering start date        :  {}'.format(metered_start_date))
print('Viewing start date         :  {}'.format(viewing_end_date))

print('Service types              : {}'.format(originator_common_service_name.strip('()')))
print('Playback types             : {}'.format(', '.join(["'{}'".format(type) for type in playback_type_list])))

print('Execution type             :  {}'.format(execution_type))
print('remove split stations      :  {}'.format('Yes' if remove_split_stations else 'No'))
print('Adjust commercial duration :  {}'.format('Yes' if adjust_commercial_duration else 'No'))
print('Drop corrupt MOP rows      :  {}'.format('Yes' if drop_corrupt_mop_rows else 'No'))

print('Write output               :  {}'.format('Yes' if please_export else 'No'))
print('Output folder              :  {}'.format(output_folder))

# COMMAND ----------

######################################
#####    USER INPUTS (manual)    #####
######################################

genre                                   = '' 
#                                           '("GENERAL VARIETY", "DAYTIME DRAMA", "INSTRUCTION, ADVICE",' \
#                                           ' "GENERAL DRAMA", "SITUATION COMEDY", "CHILD MULTI-WEEKLY",' \
#                                           ' "NEWS", "SPORTS EVENT", "FEATURE FILM", "GENERAL DOCUMENTARY")'

# playback_type_code                      = '(0,1,2)'
# tsv_type_codes                          = '(0, 2, 3, 4, 5, 6, 7, 8)'  # 0=Live, 1=same day, 2=1st day, 3=2nd day, etc.

demos_of_interest                       = ['HH']
# demos_of_interest                       = ['P2_11',
#                                            'P12_17',
                                           
#                                            'P25_54', 'F25_54', 'M25_54',
                                           
#                                            'P18_49', 'F18_49', 'M18_49',
#                                            'P18_34', 'F18_34', 'M18_34',
#                                            'P35_49', 'F35_49', 'M35_49',
#                                            'P50_999','F50_999','M50_999']
household_run                           = 'HH' in demos_of_interest
is_rounded                              = True if execution_type == 'Rounded file, rounded methodology' else False

# station_mapping_table_path              = 's3://useast1-nlsn-w-digital-dsci-dev/data/users/luje8004/station_mapping/2019-04-11/station_mapping_updated.parquet'  # April
station_mapping_table_path              = 's3://useast1-nlsn-w-digital-dsci-dev/data/users/pakdmj01/station_mapping/2019-04-11/station_mapping_altered_sep_2019'  # Sep

split_station_table_path                = '/FileStore/tables/split_station.csv'

# These two variables are used for locating the checkpoints
user_name                               = 'pakdmj01'
tuning_file_sub_mins_dir                = 's3://adexposure-tam-sub-min-poc/program_coded_tsv'
override_dir                            = 's3://useast1-nlsn-w-digital-dsci-dev/projects/commercial_sub_minutes'

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_1

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_2

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_3

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_3.5

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_4

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_5

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_6

# COMMAND ----------

##############################################################################
### Here the output is written to S3 bucket
##############################################################################

# A string with the UTC date and time of when this cell is being run,
# in the format 'yyyy-MM-dd_HH-mm-ss',
# used for time stamping the output
run_datetime = \
  str(datetime.utcnow()) \
    .split('.')[0] \
    .replace(' ', '_') \
    .replace(':', '-')

run_type  = 'household'         if household_run              else 'person'
run_type1 = execution_type
run_type2 = 'duration-adjusted' if adjust_commercial_duration else 'duration-unadjusted'
run_type3 = 'mop-cleaned'       if drop_corrupt_mop_rows      else 'mop-uncleaned'
run_type4 = 'splits-removed'    if remove_split_stations      else 'splits-unremoved'

if please_export:
  output_file = '{of}{dt}_{rt}_acm-ecm-table_{sd}_to_{ed}_{rt1}_{rt2}_{rt3}_{rt4}/' \
    .format(of =output_folder,
            dt =run_datetime,
            sd =national_tv_broadcast_start_date,
            ed =national_tv_broadcast_end_date,
            rt =run_type,
            rt1=run_type1,
            rt2=run_type2,
            rt3=run_type3,
            rt4=run_type4)

  acm_ecm_table2.write.parquet(output_file,
                               mode='overwrite')
  
  print(output_file)
  
  dbutils.fs.put(file     =output_folder + 'debugging_info',
                 contents =output_file,
                 overwrite=True)

# COMMAND ----------


