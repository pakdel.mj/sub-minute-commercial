# Databricks notebook source
# MAGIC %md
# MAGIC # How to Use This Notebook
# MAGIC 1. Specify the parameters by using the input controls at the top of this notebook
# MAGIC 2.  Run this notebook twice:
# MAGIC     1. On a cluster with more cores on the master node, such as Cinnabar, to presort the data from the extract  
# MAGIC        This saves a few minutes compared to doing this on Saffron
# MAGIC     2. On Saffron, to write the data into tables

# COMMAND ----------

# This map defines each MDL table's data location, keep it as is.

table_params = {
# table                                     :  data parquet location under s3://useast1-nlsn-w-dsci-shared-dev/
#                                              and filtering condition
  'dsci_sandbox.ecm_ecs_hh_npm_tv_dev'      : ('ecm_ecs_hh_npm_tv_dev',   "TRUE"),
  'dsci_sandbox.ecm_hh_npm_tv_dev'          : ('ecm_hh_npm_tv_dev',       "TRUE"),
  'dsci_sandbox.ecs_hh_npm_tv_dev'          : ('ecs_hh_npm_tv_dev',       "TRUE"),

  'dsci_sandbox.ecm_ecs_pp_npm_tv_dev'      : ('ecm_ecs_pp_npm_tv_dev',   "TRUE"),
  'dsci_sandbox.ecm_pp_npm_tv_dev'          : ('ecm_pp_npm_tv_dev',       "TRUE"),
  'dsci_sandbox.ecs_pp_npm_tv_dev'          : ('ecs_pp_npm_tv_dev',       "TRUE")

  # The table below is not needed anymore, keeping as an example of what can be done 
  # 'dsci_sandbox.ecm_ecs_npm_tv_dev_live' : ('ecm_ecs_npm_tv_dev_live',  "playback_type = 'live'")
}

# COMMAND ----------

# # This cell can be used to create/repopulate/remove the widgets.
# # Normally, it should stay commented out.

# dbutils.widgets.text('input_folder',
#                      's3://useast1-nlsn-w-digital-dsci-dev/'
#                        'users/egar8001/'
#                        'Projects/Sub-minute ratings/'
#                        'checkpoints/',
#                      'Input folder')

# dbutils.widgets.text('minute_level_data',
#                      's3://useast1-nlsn-w-digital-dsci-dev/'
#                        'users/egar8001/'
#                        'Projects/Sub-minute ratings/'
#                        'checkpoints/'
#                        '2019-09-23_19-54-17_household_acm_ecm_table_2019-04-01_to_2019-04-28_rounded_run_duration_not_adjusted/',
#                      'Minute-level data')

# dbutils.widgets.text('subminute_level_data',
#                      's3://useast1-nlsn-w-digital-dsci-dev/'
#                        'users/egar8001/'
#                        'Projects/Sub-minute ratings/'
#                        'checkpoints/'
#                        '2019-09-23_20-33-35_household_acm_ecm_table_2019-04-01_to_2019-04-28_non_rounded_run_duration_not_adjusted/',
#                      'Sub-minute-level data')

# dbutils.widgets.dropdown('data_to_include',
#                          'Both', ['Minute', 'Subminute', 'Both'],
#                          'Data to include')

# dbutils.widgets.dropdown('h_p_selector',
#                          'Households', ['Households', 'Persons'],
#                          'Viewing level')

# dbutils.widgets.dropdown('check_counts',
#                          'No', ['No', 'Yes'],
#                          'Check row counts after upload')

# COMMAND ----------

# MAGIC %md
# MAGIC ## Initialization

# COMMAND ----------

# These values should be set by selecting/entering values into the widgets at the top of this notebook

input_folder = \
  dbutils.widgets.get('input_folder')

minute_level_data_location = \
  dbutils.widgets.get('minute_level_data')
subminute_level_data_location = \
  dbutils.widgets.get('subminute_level_data')

data_to_include = \
  dbutils.widgets.get('data_to_include')

# An optional integrity check after the tables have been updated
check_counts = \
  dbutils.widgets.get('check_counts') == 'Yes'

h_p_selector = \
  'hh' if dbutils.widgets.get('h_p_selector') == 'Households' else 'pp'

table_name = \
  ('ecm_' if data_to_include != 'Subminute' else '') + \
  ('ecs_' if data_to_include != 'Minute'    else '') + \
  h_p_selector + \
  '_npm_tv_dev'

table_full_name = \
  'dsci_sandbox.' + table_name

print('Input folder          : {}'.format(input_folder))
print('Minute-level data     : {}'.format(minute_level_data_location.split('/')[-2]))
print('Sub-minute-level data : {}'.format(subminute_level_data_location.split('/')[-2]))
print('Data to include       : {}'.format(data_to_include))
print('Table to update       : {}'.format(table_full_name))
print('Check row counts      : {}'.format('Yes' if check_counts else 'No'))

# COMMAND ----------

# Checking if running on Saffron
saffron_flag = \
  spark.conf.get('spark.databricks.clusterUsageTags.clusterName') == u'Saffron_Sandbox_py3_dsci'

# COMMAND ----------

# MAGIC %md
# MAGIC # 1 Presorting and Saving the Presorted Data

# COMMAND ----------

if not saffron_flag:
  # Reading and presorting the data into partitions
  if data_to_include != 'Subminute':
    minute_df = spark.read.parquet(minute_level_data_location) \
                          .select('telecast_broadcast_date_days_of_week',

                                  'originator_common_service_name',
                                  'viewing_source_short_name',

                                  'program_id',
                                  'program_name',
                                  'telecast_id',
                                  'telecast_name',
                                  'report_start_time_ny',
                                  'program_summary_type_desc',

                                  'advertisement_key',
                                  'advertisement_code',
                                  'pod_value',
                                  'max_pod_in_telecast',
                                  'position_in_pod',
                                  'commercial_counts_in_pod',
                                  'commercial_start_time',
                                  'ad_duration',
                                  'brand_code',
                                  'brand_desc',
                                  'brand_variant_desc',
                                  'creative_desc',
                                  'pcc_industry_group_desc',

                                  'ac_projection',
                                  'ac_rating',
                                  'ec_projection',
                                  'ec_rating',
                                  'ue',

                                  'telecast_broadcast_date',
                                  'playback_type',
                                  'demo') \
                         .withColumnRenamed('ac_projection', 'acm_projection') \
                         .withColumnRenamed('ac_rating',     'acm_rating') \
                         .withColumnRenamed('ec_projection', 'ecm_projection') \
                         .withColumnRenamed('ec_rating',     'ecm_rating')
  
  if data_to_include != 'Minute':
    subminute_df = spark.read.parquet(subminute_level_data_location) \
                             .select('telecast_broadcast_date_days_of_week',
 
                                     'originator_common_service_name',
                                     'viewing_source_short_name',
 
                                     'program_id',
                                     'program_name',
                                     'telecast_id',
                                     'telecast_name',
                                     'report_start_time_ny',
                                     'program_summary_type_desc',
 
                                     'advertisement_key',
                                     'advertisement_code',
                                     'pod_value',
                                     'max_pod_in_telecast',
                                     'position_in_pod',
                                     'commercial_counts_in_pod',
                                     'commercial_start_time',
                                     'ad_duration',
                                     'brand_code',
                                     'brand_desc',
                                     'brand_variant_desc',
                                     'creative_desc',
                                     'pcc_industry_group_desc',
 
                                     'ac_projection',
                                     'ac_rating',
                                     'ec_projection',
                                     'ec_rating',
                                     'ue',
 
                                     'telecast_broadcast_date',
                                     'playback_type',
                                     'demo')
                           
  
  if data_to_include == 'Minute':
    df = minute_df
  elif data_to_include == 'Subminute':
    df = subminute_df
  else:
    df = \
      minute_df.join(subminute_df.select('playback_type',
                                         'telecast_broadcast_date',
                                         'demo',
                                         'program_id',
                                         'telecast_id',
                                         'advertisement_key',
                                         'commercial_start_time',
                                         'viewing_source_short_name',
                                         'ac_projection',
                                         'ac_rating',
                                         'ec_projection',
                                         'ec_rating'),
                     on=[
                       'playback_type',
                       'telecast_broadcast_date',
                       'demo',
                       'program_id',
                       'telecast_id',
                       'advertisement_key',
                       'commercial_start_time',
                       'viewing_source_short_name'
                     ],
                     how='outer')

  df = df.repartition(3*28,  # 3 playback types * 28 days
                      'playback_type',
                      'telecast_broadcast_date')
  
  # Saving the presorted data
  # Took ~3 minutes on Cinnabar, ~10 minutes on Saffron
  df.write.partitionBy('playback_type', 'telecast_broadcast_date') \
    .parquet(input_folder + table_name + '.parquet/',
             mode='overwrite')

# COMMAND ----------

# MAGIC %md
# MAGIC # 2 Updating MDL tables

# COMMAND ----------

# Recreating the tables, to avoid large numbers of files in partitions
if saffron_flag:
  spark.sql("DROP TABLE IF EXISTS {table}".format(table=table_full_name))

  spark.sql(r"""\
    CREATE TABLE IF NOT EXISTS {table} (
      telecast_broadcast_date_days_of_week STRING   COMMENT 'Day of week; full, capitalized, e.g, \'Monday\'',

      originator_common_service_name       STRING   COMMENT '\'Network\', \'Cable\', etc.',
      viewing_source_short_name            STRING   COMMENT '\'ABC\', \'CBS\', \'NBC\', etc.',

      program_id                           INT      COMMENT 'From TAM_NPM_MCH_TV_PROD.report_originator_lineup.national_tv_report_program_id',
      program_name                         STRING   COMMENT 'From TAM_NPM_MCH_TV_PROD.report_originator_lineup.program_name',
      telecast_id                          BIGINT   COMMENT 'From TAM_NPM_MCH_TV_PROD.report_originator_lineup.report_originator_lineup_id',
      telecast_name                        STRING   COMMENT 'From TAM_NPM_MCH_TV_PROD.report_originator_lineup.telecast_name',
      report_start_time_ny                 STRING   COMMENT 'HH:mm:ss, New York time. From TAM_NPM_MCH_TV_PROD.report_originator_lineup.report_start_time_ny',
      program_summary_type_desc            STRING   COMMENT '\'NEWS\', \'GENERAL DRAMA\', etc. From TAM_NPM_MCH_TV_PROD.program_summary_type.program_summary_type_desc',

      advertisement_key                    INT      COMMENT 'From TAM_NPM_MCH_TV_PROD.advertisement_fact.advertisement_key',
      advertisement_code                   STRING   COMMENT 'From TAM_NPM_MCH_TV_PROD.advertisement_dimension.advertisement_code',

      pod_value                            TINYINT  COMMENT 'A 1-based index within the telecast. From TAM_NPM_EDW_TV_PROD.pod_dimension.pod_value',
      max_pod_in_telecast                  TINYINT,
      position_in_pod                      TINYINT  COMMENT 'A 1-based index within the pod',
      commercial_counts_in_pod             TINYINT,

      commercial_start_time                STRING   COMMENT 'HH:mm:ss, New York time. From TAM_NPM_MCH_TV_PROD.advertisement_fact.commercial_start_time',
      ad_duration                          SMALLINT COMMENT 'Seconds. From TAM_NPM_MCH_TV_PROD.advertisement_fact.advertisement_duration',
      brand_code                           BIGINT   COMMENT 'From TAM_NPM_MCH_TV_PROD.advertisement_dimension.brand_code',
      brand_desc                           STRING   COMMENT 'From TAM_NPM_MCH_TV_PROD.advertisement_dimension.brand_desc',
      brand_variant_desc                   STRING   COMMENT 'From TAM_NPM_MCH_TV_PROD.advertisement_dimension.brand_variant_desc',
      creative_desc                        STRING   COMMENT 'From TAM_NPM_MCH_TV_PROD.advertisement_dimension.creative_desc',
      pcc_industry_group_desc              STRING   COMMENT '\'TOILETRIES & COSMETICS\', \'INSURANCE & REAL ESTATE\', etc. From TAM_NPM_MCH_TV_PROD.advertisement_dimension.pcc_industry_group_desc',

      {minute_level_stats}
      {subminute_level_stats}
      ue                                   DECIMAL  COMMENT 'Units (x1)',

      telecast_broadcast_date              STRING   COMMENT 'yyyy-MM-dd, starts at 6 AM of the corresponding calendar date. From TAM_NPM_EDW_TV_PROD.telecast_dimension.telecast_broadcast_date',
      playback_type                        STRING   COMMENT '\'live\', \'live+3\', or \'live+7\'',
      demo                                 STRING
    )

    USING parquet

    PARTITIONED BY (telecast_broadcast_date,  -- These columns will be the last ones, regardless of the order above
                    playback_type,
                    demo)
                    
    LOCATION 's3://useast1-nlsn-w-dsci-shared-dev/{location}/'"""
      .format(table                =table_full_name,
              location             =table_params[table_full_name][0],
              minute_level_stats   =
                r"""acm_projection  DECIMAL  COMMENT 'Thousands (x1000)',
                    acm_rating      FLOAT    COMMENT 'Out of a total of 100',  -- 8-digit precision
                    ecm_projection  DECIMAL  COMMENT 'Thousands (x1000)',
                    ecm_rating      FLOAT    COMMENT 'Out of a total of 100',"""
                if data_to_include != 'Subminute' else
                '',
              subminute_level_stats=
                r"""ac_projection   DECIMAL  COMMENT 'Thousands (x1000)',
                    ac_rating       FLOAT    COMMENT 'Out of a total of 100',
                    ec_projection   DECIMAL  COMMENT 'Thousands (x1000)',
                    ec_rating       FLOAT    COMMENT 'Out of a total of 100',"""
                if data_to_include != 'Minute' else
                '')
  )

# COMMAND ----------

if saffron_flag:
  columns = [
    'telecast_broadcast_date_days_of_week',

    'originator_common_service_name',
    'viewing_source_short_name',

    'program_id',
    'program_name',
    'telecast_id',
    'telecast_name',
    'report_start_time_ny',
    'program_summary_type_desc',

    'advertisement_key',
    'advertisement_code',
    'pod_value',
    'max_pod_in_telecast',
    'position_in_pod',
    'commercial_counts_in_pod',
    'commercial_start_time',
    'ad_duration',
    'brand_code',
    'brand_desc',
    'brand_variant_desc',
    'creative_desc',
    'pcc_industry_group_desc']
  
  if data_to_include != 'Subminute':
    columns.extend([
      'acm_projection',
      'acm_rating',
      'ecm_projection',
      'ecm_rating'])
    
  if data_to_include != 'Minute':
    columns.extend([
      'ac_projection',
      'ac_rating',
      'ec_projection',
      'ec_rating'])
    
  columns.extend([
    'ue',
    'telecast_broadcast_date',
    'playback_type',
    'demo'])
  
  df = spark.read.parquet(input_folder + table_name + '.parquet/') \
            .select(*columns) \
            .repartition(3*28,                      # 3 playback types * 28 days
                         'telecast_broadcast_date',
                         'playback_type') \
            .cache()
  
  # Took 15-30 minutes on 90M rows
  df.where(table_params[table_full_name][1]) \
    .write.insertInto(table_full_name,
                      overwrite=True)

# COMMAND ----------

# MAGIC %md
# MAGIC ## 2.1 Checking the Counts (Optional)

# COMMAND ----------

if saffron_flag and check_counts:
  count = spark.sql("SELECT count(1) AS count FROM {table} WHERE {condition}"
                      .format(table    =table_full_name,
                              condition=table_params[table_full_name][1])) \
               .collect() \
                 [0]['count']

  print(  'Count in {0} is correct, {1}'.format(table_full_name, count)
        if 
          count ==
          df.where(table_params[table_full_name][1])
            .count()
        else
          'Count in {0} is off'.format(table_full_name))