# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC # Commercial Metrics Analysis
# MAGIC 
# MAGIC > This notebook could be executed to compare commercial ratings/impressions
# MAGIC   using minute level data between average commercial minute (ACM)
# MAGIC   and exact commercial minute (ECM) for a supplied date range and service name.
# MAGIC 
# MAGIC **PURPOSE:**
# MAGIC > * The ACM rating is calculated as the total duration weighted impressions
# MAGIC     for all commercial minutes across a telecast
# MAGIC     divided by the total number of commercial seconds.
# MAGIC     All commercials within the same telecast have identical ACM rating.
# MAGIC     The ECM rating is calculated as the total duration weighted impressions
# MAGIC     for each commercial within a telecast divided by the total number of seconds,
# MAGIC     devoted to that commercial.
# MAGIC     For the commercials that aired within only one minute of a telecast,
# MAGIC     ECM is the same as ratings/impression of that specific telecast minute.
# MAGIC     For commercials that cross multiple minutes, ECM is the duration weighted impression
# MAGIC     of all commercial minutes.
# MAGIC 
# MAGIC 
# MAGIC 
# MAGIC **USER INPUTS (NOTEBOOK WIDGETS):**
# MAGIC > 1. **Begin (yyyy-mm-dd)**
# MAGIC >    - The beginning of the desired measurement period,
# MAGIC        in the **yyyy-mm-dd** format
# MAGIC > 1. **End (yyyy-mm-dd)**
# MAGIC >    - The ending of the desired measurement period,
# MAGIC        in the **yyyy-mm-dd** format
# MAGIC > 1. **Service**
# MAGIC >    - Service types of interest, used in the analysis  
# MAGIC        Available service types are 'Network', 'Cable', 'Syndication' or a combination of these.
# MAGIC > 1. **Time-shifted viewing**
# MAGIC >    - playback type of interest;  
# MAGIC        Available playback types are 'live', 'live+3', 'live+7' or a combination of these.
# MAGIC > 1. **Rounded tuning data**
# MAGIC >    - type of tuning data to use;  
# MAGIC        If 'Yes' is selected, minute-level tuning data will be used.
# MAGIC        Otherwise, the analysis will use sub-minute-level data.
# MAGIC > 1. **Adjust commercial duration**
# MAGIC      - when using minute-level data, whether or not to fix minute commercial duration over 60 seconds
# MAGIC > 1. **Write output**
# MAGIC >    - whether or not write the output data to an S3 location
# MAGIC > 1. **Output folder**
# MAGIC >    - The folder on S3 that will be used as the parent folder for the output
# MAGIC        if 'Yes' is selected in **Write output**
# MAGIC  
# MAGIC 
# MAGIC **DATA SOURCES:**
# MAGIC > | schema                          | table                                                        |
# MAGIC   |---------------------------------|--------------------------------------------------------------|
# MAGIC   | tam_npm_mch_tv_prod             | report_originator_lineup                                     |
# MAGIC   | tam_npm_mch_tv_prod             | national_tv_content_originator                               |
# MAGIC   | tam_npm_mch_tv_prod             | program_summary_type                                         |
# MAGIC   | tam_npm_mch_tv_prod             | program_coded_metered_tv_person_media_engagement_event       |
# MAGIC   | tam_npm_mch_tv_prod             | metered_tv_household_location_intab_status                   |
# MAGIC   | tam_npm_mch_tv_prod             | intab_period                                                 |
# MAGIC   | tam_npm_mch_tv_prod             | metered_tv_person                                            |
# MAGIC   | tam_npm_mch_tv_prod             | metered_tv_household_location_person_intab_and_weight        |
# MAGIC   | parquet                         | station_mapping_table                                        |
# MAGIC   | tam_npm_mch_tv_prod             | national_tv_weighting_control_universe_estimate              |
# MAGIC   | tam_npm_mch_tv_prod             | advertisement_fact                                           |
# MAGIC   | tam_npm_mch_tv_prod             | advertisement_dimension                                      |
# MAGIC   | tam_npm_edw_tv_prod             | pod_dimension                                                |
# MAGIC 
# MAGIC **OUTPUT FORMAT:**
# MAGIC > | col_name                              | data_type  | ex_results                             |
# MAGIC   |----------------------------           |----------- |--------------------------------------- |
# MAGIC   | telecast_broadcast_date               | date       |  2018-03-01                            |
# MAGIC   | program_id                            | bigint     |  178501                                |
# MAGIC   | telecast_id                           | bigint     |  94224                                 |
# MAGIC   | playback_type                         | string     | 'live+7'                               |
# MAGIC   | demo                                  | string     | 'HH'                                   |
# MAGIC   | originator_common_service_name        | string     | 'Syndication'                          |
# MAGIC   | program_name                          | string     | 'ENTERTAINMENT TONIGHT(AT)'            |
# MAGIC   | telecast_name                         | string     | 'ENTERTAINMENT TONIGHT(AT)'            |
# MAGIC   | telecast_broadcast_date_days_of_week  | string     | 'Wednesday'                            |
# MAGIC   | report_start_time_ny                  | time       | '19:00:00'                             |
# MAGIC   | viewing_source_short_name             | string     |  ABC                                   |
# MAGIC   | program_summary_type_desc             | string     |  NEWS                                  |
# MAGIC   | commercial_start_time                 | time       | '19:58:23'                             |
# MAGIC   | ad_duration                           | short      |  15                                    |
# MAGIC   | pod_value                             | int        |  4                                     |
# MAGIC   | max_pod_in_telecast                   | int        |  7                                     |
# MAGIC   | position_in_pod                       | int        |  8                                     |
# MAGIC   | commercial_counts_in_pod              | int        |  12                                    |
# MAGIC   | advertisement_key                     | bigint     |  8115483                               |
# MAGIC   | advertisement_code                    | string     | '1E0888E605'                           |
# MAGIC   | brand_code                            | bigint     |  13080                                 |
# MAGIC   | creative_desc                         | string     | 'WOMAN/BABY/WOMAN OPENS INCUBATOR'     |
# MAGIC   | ue                                    | bigint     |  120640000                             |
# MAGIC   | pcc_industry_group_desc               | string     | 'BUSINESS & CONSUMER SVCS'             |
# MAGIC   | ac_projection                         | decimal    |  241.0 (000)                           |
# MAGIC   | ac_rating                             | decimal    |  0.1997334466180371                    |
# MAGIC   | ec_projection                         | decimal    |  242.0 (000)                           |
# MAGIC   | ec_rating                             | decimal    |  0.20049197612732095                   |
# MAGIC 
# MAGIC 
# MAGIC **EXAMPLE RUN:**
# MAGIC > 
# MAGIC | Widget               | Value                                                                            |
# MAGIC | ---                  | ---                                                                              |
# MAGIC | Begin (yyyy-mm-dd)   | `2019-04-01`                                                                     |
# MAGIC | End (yyyy-mm-dd)     | `2019-04-28`                                                                     |
# MAGIC | Service              | `Network,Syndication`                                                            |
# MAGIC | Time-shifted viewing | `live,live+3`                                                                    |
# MAGIC | Rounded tuning data  | `Yes`                                                                            |
# MAGIC | Write output         | `Yes`                                                                            |
# MAGIC | Output folder        | `s3://useast1-nlsn-w-digital-dsci-dev/projects/commercial_sub_minutes/pakdmj01/` |
# MAGIC 
# MAGIC **CODE DEVELOPERS:**
# MAGIC > * MJ PAKDEL
# MAGIC > * Arseny EGOROV

# COMMAND ----------

# MAGIC %md
# MAGIC ### Import Libraries

# COMMAND ----------

from __future__ import division

import pyspark.sql.functions as f
import pyspark.sql.types as t
from datetime import datetime, timedelta

# COMMAND ----------

# MAGIC %md
# MAGIC ### Input

# COMMAND ----------

# Use this cell to add/populate/remove the widgets for user input.
# After creating the widgets, click the gear icon to the right of the widgets,
# and choose 'Run Accessed Commands' in the dropdown menu.

# Start date
dbutils.widgets.text('national_tv_broadcast_start_date',
                     '2019-04-01',
                     'Begin (yyyy-mm-dd)')

# End date
dbutils.widgets.text('national_tv_broadcast_end_date',
                     '2019-04-28',
                     'End (yyyy-mm-dd)')

# Service types
dbutils.widgets.multiselect('originator_common_service_name',
                            'Network',
                            [
                              "Network",
                              "Cable",
                              "Syndication"
                            ],
                            'Service')

# Playback types
dbutils.widgets.multiselect('time_shifted_viewing',
                            'live',
                            [
                              'live',
                              'live+SD',
                              'live+3',
                              'live+7'],
                            'Time-shifted viewing')


# Whether to write the output
dbutils.widgets.dropdown('please_export',
                         'Yes',
                         [
                           'Yes',
                           'No'
                         ],
                         'Write output')

# Where to write the output
dbutils.widgets.text('output_folder',
                     's3://useast1-nlsn-w-digital-dsci-dev/projects/commercial_sub_minutes/pakdmj01/',
                     'Output folder')

dbutils.widgets.dropdown('execution_type',
                         'Rounded file, rounded methodology',
                         [
                           'Rounded file, rounded methodology',
                           'Non-rounded file, non-rounded methodology',
                           'Rounded file, non-rounded methodology'
                         ],
                         'Execution type')

dbutils.widgets.dropdown('adjust_commercial_duration',
                         'Yes',
                         [
                           'Yes',
                           'No'
                         ],
                         'Adjust commercial minute duration')

dbutils.widgets.dropdown('remove_split_stations',
                         'Yes',
                         [
                           'Yes',
                           'No'
                         ],
                         'Remove split stations')

dbutils.widgets.dropdown('drop_corrupt_mop_rows',
                         'Yes',
                         [
                           'Yes',
                           'No'
                         ],
                         'Drop corrupt MOP rows')

# COMMAND ----------

#######################################
#####    USER INPUTS (widgets)    #####
#######################################

national_tv_broadcast_start_date        = dbutils.widgets.get('national_tv_broadcast_start_date')
national_tv_broadcast_end_date          = dbutils.widgets.get('national_tv_broadcast_end_date')
metered_start_date                      = datetime.strftime(datetime.strptime(national_tv_broadcast_start_date, '%Y-%m-%d')
                                                              - timedelta(days=60),
                                                            '%Y-%m-%d')
viewing_end_date                        = datetime.strftime(datetime.strptime(national_tv_broadcast_end_date, '%Y-%m-%d')
                                                              + timedelta(days=8),
                                                            '%Y-%m-%d')

originator_common_service_name          = "('{}')".format(dbutils.widgets.get('originator_common_service_name').replace(",", "', '"))
playback_type_list                      = sorted(dbutils.widgets.get('time_shifted_viewing').split(','))
execution_type                          = dbutils.widgets.get('execution_type') 
remove_split_stations                   = dbutils.widgets.get('remove_split_stations') == 'Yes'

adjust_commercial_duration              = dbutils.widgets.get('adjust_commercial_duration') == 'Yes'
drop_corrupt_mop_rows                   = dbutils.widgets.get('drop_corrupt_mop_rows') == 'Yes'

please_export                           = dbutils.widgets.get('please_export') == 'Yes'
output_folder                           = dbutils.widgets.get('output_folder')

print('Start date                 :  {}'.format(national_tv_broadcast_start_date))
print('End date                   :  {}'.format(national_tv_broadcast_end_date))
print('Metering start date        :  {}'.format(metered_start_date))
print('Viewing start date         :  {}'.format(viewing_end_date))

print('Service types              : {}'.format(originator_common_service_name.strip('()')))
print('Playback types             : {}'.format(', '.join(["'{}'".format(type) for type in playback_type_list])))

print('Execution type             :  {}'.format(execution_type))
print('remove split stations      :  {}'.format('Yes' if remove_split_stations else 'No'))
print('Adjust commercial duration :  {}'.format('Yes' if adjust_commercial_duration else 'No'))
print('Drop corrupt MOP rows      :  {}'.format('Yes' if drop_corrupt_mop_rows else 'No'))

print('Write output               :  {}'.format('Yes' if please_export else 'No'))
print('Output folder              :  {}'.format(output_folder))

# COMMAND ----------

######################################
#####    USER INPUTS (manual)    #####
######################################

genre                                   = '' 
#                                           '("GENERAL VARIETY", "DAYTIME DRAMA", "INSTRUCTION, ADVICE",' \
#                                           ' "GENERAL DRAMA", "SITUATION COMEDY", "CHILD MULTI-WEEKLY",' \
#                                           ' "NEWS", "SPORTS EVENT", "FEATURE FILM", "GENERAL DOCUMENTARY")'

# playback_type_code                      = '(0,1,2)'
# tsv_type_codes                          = '(0, 2, 3, 4, 5, 6, 7, 8)'  # 0=Live, 1=same day, 2=1st day, 3=2nd day, etc.

demos_of_interest                       = ['HH']
# demos_of_interest                       = ['P2_11',
#                                            'P12_17',
                                           
#                                            'P25_54', 'F25_54', 'M25_54',
                                           
#                                            'P18_49', 'F18_49', 'M18_49',
#                                            'P18_34', 'F18_34', 'M18_34',
#                                            'P35_49', 'F35_49', 'M35_49',
#                                            'P50_999','F50_999','M50_999']
household_run                           = 'HH' in demos_of_interest
is_rounded                              = True if execution_type == 'Rounded file, rounded methodology' else False

# station_mapping_table_path              = 's3://useast1-nlsn-w-digital-dsci-dev/data/users/luje8004/station_mapping/2019-04-11/station_mapping_updated.parquet'  # April
station_mapping_table_path              = 's3://useast1-nlsn-w-digital-dsci-dev/data/users/pakdmj01/station_mapping/2019-04-11/station_mapping_altered_sep_2019'  # Sep
split_station_table_path                = '/FileStore/tables/split_station.csv'

# These two variables are used for locating the checkpoints
user_name                               = 'pakdmj01'
tuning_file_sub_mins_dir                = 's3://adexposure-tam-sub-min-poc/program_coded_tsv'
override_dir                            = 's3://useast1-nlsn-w-digital-dsci-dev/projects/commercial_sub_minutes'

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_1_copy

# COMMAND ----------

pb_program_coded_stn_demo.cache().count()
#177069735
# 135582655
# 135608189

# COMMAND ----------

# telecast_broadcast_data = '2019-08-28'
# program_id = 871296
# telecast_id = 99702
# playback_type = 'live'


# pb_program_coded_stn_demo.write.parquet('s3://useast1-nlsn-w-digital-dsci-dev/users/pakdmj01/prod_sep_extended_removed',mode = 'overwrite')
pb_program_coded_stn_demo = spark.read.parquet('s3://useast1-nlsn-w-digital-dsci-dev/users/pakdmj01/prod_sep_extended_removed') \
# .filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
# .filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id) \
                                                 
pb_program_coded_stn_demo.cache().count()

# COMMAND ----------

extra_prod_dir_by_day           = 's3://useast1-nlsn-w-digital-dsci-dev/users/pakdmj01/households_in_prod_but_not_poc_by_day_extended_removed'
extra_prod_by_day = spark.read.parquet(extra_prod_dir_by_day)
print(extra_prod_by_day.count())

pb_program_coded_stn_demo = pb_program_coded_stn_demo.join(extra_prod_by_day,
              on = ['telecast_broadcast_date','source_installed_location_number','household_media_consumer_id'],
              how = 'left').filter(extra_prod_by_day['source_installed_location_number'].isNull()).cache()
pb_program_coded_stn_demo.createOrReplaceTempView('pb_program_coded_stn_demo')
print('the number of rows in prod after removing extra homes in prod : ',pb_program_coded_stn_demo.cache().count())

# COMMAND ----------

# extra_prod_dir_by_month           = 's3://useast1-nlsn-w-digital-dsci-dev/users/pakdmj01/households_in_prod_but_not_poc_by_month_extended_home_removed'
# extra_prod_by_month = spark.read.parquet(extra_prod_dir_by_month)

# pb_program_coded_stn_demo = pb_program_coded_stn_demo.join(extra_prod_by_month,
#               on = ['source_installed_location_number','household_media_consumer_id'],
#               how = 'left').filter(extra_prod_by_month['source_installed_location_number'].isNull()).cache()
# pb_program_coded_stn_demo.createOrReplaceTempView('pb_program_coded_stn_demo')
# print('the number of rows in prod after removing extra homes in prod : ',pb_program_coded_stn_demo.cache().count())

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_2

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_3

# COMMAND ----------

# ads_fact_dim = ads_fact_dim.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id)
ads_fact_dim.cache().count()

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_3.5

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_4

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_5

# COMMAND ----------

# MAGIC %run ./modules/commercial_sub_part_6

# COMMAND ----------

##############################################################################
### Here the output is written to S3 bucket
##############################################################################

# A string with the UTC date and time of when this cell is being run,
# in the format 'yyyy-MM-dd_HH-mm-ss',
# used for time stamping the output
run_datetime = \
  str(datetime.utcnow()) \
    .split('.')[0] \
    .replace(' ', '_') \
    .replace(':', '-')

run_type  = 'household'         if household_run              else 'person'
run_type1 = execution_type
run_type2 = 'duration-adjusted' if adjust_commercial_duration else 'duration-unadjusted'
run_type3 = 'mop-cleaned'       if drop_corrupt_mop_rows      else 'mop-uncleaned'
run_type4 = 'splits-removed'    if remove_split_stations      else 'splits-unremoved'

if please_export:
  output_file = '{of}{dt}_{rt}_acm-ecm-table_{sd}_to_{ed}_{rt1}_{rt2}_{rt3}_{rt4}/' \
    .format(of =output_folder,
            dt =run_datetime,
            sd =national_tv_broadcast_start_date,
            ed =national_tv_broadcast_end_date,
            rt =run_type,
            rt1=run_type1,
            rt2=run_type2,
            rt3=run_type3,
            rt4=run_type4)

  acm_ecm_table2.write.parquet(output_file,
                               mode='overwrite')
  
  print(output_file)
  
  dbutils.fs.put(file     =output_folder + 'debugging_info',
                 contents =output_file,
                 overwrite=True)

# COMMAND ----------

new_data1 = spark.read.parquet('s3://useast1-nlsn-w-digital-dsci-dev/projects/commercial_sub_minutes/pakdmj01/2020-01-24_18-23-53_household_acm-ecm-table_2019-08-26_to_2019-09-22_Rounded file, rounded methodology_duration-unadjusted_mop-cleaned_splits-removed/')
new_data1.count()

# COMMAND ----------

new_data1.createOrReplaceTempView('new_data1')

# COMMAND ----------

# MAGIC %sql select * from new_data1 where ac_rating is null

# COMMAND ----------

# MAGIC %sql select * from dsci_sandbox.ecm_ecs_hh_npm_tv_dev2 where acm_projection is null

# COMMAND ----------

display(new_data1.filter('playback_type = "live+7"'))

# COMMAND ----------

# MAGIC %md
# MAGIC # ACM Enhanced

# COMMAND ----------

acm_table_enhanced.cache().count()

# COMMAND ----------

display(acm_table_enhanced.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id) )

# COMMAND ----------

ecm_table_enhanced.cache().count()

# COMMAND ----------

ecm_table_enhanced.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id).filter(f.col('viewing_source_short_name') == 'GRTAF').count()

# COMMAND ----------

display(ecm_table_enhanced.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id).filter(f.col('viewing_source_short_name') == 'not exists'))

# COMMAND ----------

##############################################################################
### Column max_pod_in_telecast shows the maximum pod value for a specific 
### telecast. This column can be used to decided if an ads is early or late 
### within a telecast.
###############################################################################

max_pod_in_telecast_df = \
  spark.sql('''--sql
    SELECT 
      broadcast_date,
      program_id,
      telecast_id,
      max(pod_value) AS max_pod_in_telecast
    FROM
      ads_fact_dim
    GROUP BY
      broadcast_date,
      program_id,
      telecast_id
      
    --endsql''')

max_pod_in_telecast_df.createOrReplaceTempView('max_pod_in_telecast_df') 

# COMMAND ----------

##############################################################################
### Column commercial_counts_in_pod specified the number of commercials 
### per telecast/pod. This column can be used to  decide if an ad is early 
### or late within a specific pod.
###############################################################################

commercial_counts_in_pod_df = \
  spark.sql('''--sql
    SELECT 
      broadcast_date,
      program_id,
      telecast_id,
      pod_value,
      max(position_in_pod) AS commercial_counts_in_pod
    FROM 
      commercial_pos_in_pod_df
    GROUP BY
      broadcast_date,
      program_id,
      telecast_id,
      pod_value
      
    --endsql''')

commercial_counts_in_pod_df.createOrReplaceTempView('commercial_counts_in_pod_df')

# COMMAND ----------

####################################################################################
### Here columns max_pod_in_telecast and commercial_counts_in_pod are added 
### to the viewing table. 
####################################################################################

s = '''--sql
  SELECT 
    ECM.*,
    POD_COUNT.max_pod_in_telecast,
    ADS_COUNT.commercial_counts_in_pod
  FROM
    ecm_table_enhanced AS ECM

  JOIN
    max_pod_in_telecast_df AS POD_COUNT
  ON    TRUE
    AND ECM.telecast_broadcast_date = POD_COUNT.broadcast_date
    AND ECM.program_id              = POD_COUNT.program_id
    AND ECM.telecast_id             = POD_COUNT.telecast_id

  JOIN
    commercial_counts_in_pod_df AS ADS_COUNT
  ON    TRUE
    AND ECM.telecast_broadcast_date = ADS_COUNT.broadcast_date
    AND ECM.program_id              = ADS_COUNT.program_id
    AND ECM.telecast_id             = ADS_COUNT.telecast_id
    AND ECM.pod_value               = ADS_COUNT.pod_value

  --endsql'''

ecm_table_with_pod_pos = spark.sql(s).cache()
ecm_table_with_pod_pos.createOrReplaceTempView("ecm_table_with_pod_pos")

# COMMAND ----------

##############################################################################
### Here the ACM and ECM tables are joined. The resulted table is the master
### dataset which is outputed as the final result of this notebook
##############################################################################

s = '''--sql
  WITH
    ecm_acm_join AS (
      SELECT
        ECM.telecast_broadcast_date,
        ECM.program_id,
        ECM.telecast_id,
        ECM.playback_type,
        ECM.demo,
        ACM.originator_common_service_name,
        ACM.program_name,
        ACM.telecast_name,
        ACM.telecast_broadcast_date_days_of_week,
        ACM.report_start_time_ny,
        ECM.viewing_source_short_name,
        ACM.program_summary_type_desc,
        ECM.commercial_start_time,
        ECM.advertisement_duration_per_telecast AS ad_duration,
        ECM.pod_value,
        ECM.max_pod_in_telecast,
        POD_POS.position_in_pod,
        ECM.commercial_counts_in_pod,
        ECM.advertisement_key,
        ECM.advertisement_code,
        ECM.brand_code,
        ECM.brand_desc,
        ECM.brand_variant_desc,
        ECM.creative_desc,
        INTAB.ue,
        ECM.pcc_industry_group_desc,
        ACM.ac_projection,
        ACM.ac_rating,
        ECM.ec_projection,
        ECM.ec_rating

      FROM 
        ecm_table_with_pod_pos AS ECM

      LEFT JOIN  -- may or may not have an ACM match
        acm_table_enhanced     AS ACM
      ON    TRUE
        AND ECM.telecast_broadcast_date   = ACM.telecast_broadcast_date
        AND ECM.program_id                = ACM.program_id
        AND ECM.telecast_id               = ACM.telecast_id
        AND ECM.viewing_source_short_name = ACM.viewing_source_short_name
        AND ECM.demo                      = ACM.demo
        AND ECM.playback_type             = ACM.playback_type

      JOIN
        commercial_pos_in_pod_df AS POD_POS
      ON    TRUE
        AND ECM.telecast_broadcast_date = POD_POS.broadcast_date
        AND ECM.program_id              = POD_POS.program_id
        AND ECM.telecast_id             = POD_POS.telecast_id
        AND ECM.advertisement_key       = POD_POS.advertisement_key
        AND ECM.advertisement_code      = POD_POS.advertisement_code
        AND ECM.pod_value               = POD_POS.pod_value
        AND ECM.commercial_start_time   = POD_POS.commercial_start_time

      JOIN
        intab_df AS INTAB
      ON    TRUE
        AND ECM.telecast_broadcast_date = INTAB.intab_period_start_date
        AND ECM.demo                    = INTAB.demo
    )


  -- telecasts with zero tuning and non-zero ad minutes
  SELECT 
    ECM.telecast_broadcast_date,
    ECM.program_id,
    ECM.telecast_id,
    ECM.playback_type,
    ECM.demo,
    ACM.originator_common_service_name,
    ACM.program_name,
    ACM.telecast_name,
    ACM.telecast_broadcast_date_days_of_week,
    ACM.report_start_time_ny,
    ACM.viewing_source_short_name,  -- ACM instead of ECM
    ACM.program_summary_type_desc,
    ECM.commercial_start_time,
    ECM.ad_duration,
    ECM.pod_value,
    ECM.max_pod_in_telecast,
    ECM.position_in_pod,
    ECM.commercial_counts_in_pod,
    ECM.advertisement_key,
    ECM.advertisement_code,
    ECM.brand_code,
    ECM.brand_desc,
    ECM.brand_variant_desc,
    ECM.creative_desc,
    ECM.ue,
    ECM.pcc_industry_group_desc,
    ACM.ac_projection,
    ACM.ac_rating,
    ECM.ec_projection,
    ECM.ec_rating
    
  FROM ecm_acm_join AS ECM

  JOIN
    acm_table_enhanced AS ACM
  ON    TRUE
    AND ECM.ac_projection          IS NULL

    AND ECM.telecast_broadcast_date  = ACM.telecast_broadcast_date
    AND ECM.program_id               = ACM.program_id
    AND ECM.telecast_id              = ACM.telecast_id
    AND ECM.demo                     = ACM.demo
    AND ECM.playback_type            = ACM.playback_type

    
  UNION ALL


  -- all other telecasts
  SELECT
    *

  FROM ecm_acm_join

  WHERE TRUE
    AND ac_projection IS NOT NULL

  --endsql'''

acm_ecm_table = spark.sql(s)
acm_ecm_table.createOrReplaceTempView("acm_ecm_table")
acm_ecm_table.cache().count()

# COMMAND ----------

display(acm_ecm_table.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id).filter(f.col('viewing_source_short_name') == 'not exists'))

# COMMAND ----------

display(acm_ecm_table.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id).filter(f.col('viewing_source_short_name') == 'GRTAF'))

# COMMAND ----------

display(acm_ecm_table.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id).filter(f.col('viewing_source_short_name') == 'LAFAF'))

# COMMAND ----------

#########################################################
## here for those telecasts in acm_ecm_table that are
## also present in no_match table will be replaced with
## value "removed" for ratings
##########################################################

if drop_corrupt_mop_rows:
  s = '''--sql
    SELECT
      ac_ec.telecast_broadcast_date,
      ac_ec.program_id,
      ac_ec.telecast_id,
      ac_ec.playback_type,demo,
      ac_ec.originator_common_service_name,
      ac_ec.program_name,
      ac_ec.telecast_name,
      ac_ec.telecast_broadcast_date_days_of_week,
      ac_ec.report_start_time_ny,
      ac_ec.viewing_source_short_name,
      ac_ec.program_summary_type_desc,   
      ac_ec.commercial_start_time,
      ac_ec.ad_duration,
      ac_ec.pod_value,
      ac_ec.max_pod_in_telecast,
      ac_ec.position_in_pod,
      ac_ec.commercial_counts_in_pod,
      ac_ec.advertisement_key,
      ac_ec.advertisement_code,
      ac_ec.brand_code,
      ac_ec.brand_desc,
      ac_ec.brand_variant_desc,
      ac_ec.creative_desc,
      ac_ec.ue,
      ac_ec.pcc_industry_group_desc,
      CASE WHEN no_match.ti IS NOT NULL THEN 'removed' ELSE ac_ec.ac_projection END AS ac_projection,
      CASE WHEN no_match.ti IS NOT NULL THEN 'removed' ELSE ac_ec.ac_rating END AS ac_rating,
      CASE WHEN no_match.ti IS NOT NULL THEN 'removed' ELSE ac_ec.ec_projection END AS ec_projection,
      CASE WHEN no_match.ti IS NOT NULL THEN 'removed' ELSE ac_ec.ec_rating END AS ec_rating
    FROM
      acm_ecm_table AS ac_ec

    LEFT JOIN
      no_match
    ON    TRUE
      AND ac_ec.telecast_broadcast_date = no_match.bd
      AND ac_ec.program_id              = no_match.pi
      AND ac_ec.telecast_id             = no_match.ti

    --endsql'''
    
  acm_ecm_table2 = spark.sql(s)
  acm_ecm_table2.createOrReplaceTempView("acm_ecm_table")
  acm_ecm_table2.cache().count()

# COMMAND ----------

display(acm_ecm_table2.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id).filter(f.col('viewing_source_short_name') == 'not exists'))

# COMMAND ----------

display(acm_ecm_table2.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id).filter(f.col('viewing_source_short_name') == 'GRTAF'))

# COMMAND ----------

display(acm_ecm_table2.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id).filter(f.col('viewing_source_short_name') == 'LAFAF'))

# COMMAND ----------



# COMMAND ----------

#############################################################
### Find distinct viewing sources per data/program/telecast/
### playback_type/demo
#############################################################
field = [
  t.StructField("telecast_broadcast_date",              t.StringType(),  True),
  t.StructField("program_id",                           t.IntegerType(), True),
  t.StructField("telecast_id",                          t.LongType(),    True),
  t.StructField("playback_type",                        t.StringType(),  True),
  t.StructField("viewing_source_short_name",            t.StringType(),  True),
  t.StructField("demo",                                 t.StringType(),  True)
]

def unique_viewing_sources_query(demo, viewing_df):
  s = """--sql
    SELECT DISTINCT
      telecast_broadcast_date,
      program_id,
      telecast_id,
      playback_type,
      viewing_source_short_name,
      '{demo}' AS demo
      
    FROM
      {viewing_df}
    
    --endsql""" \
      .format(demo = demo,
              viewing_df = viewing_df)

  return spark.sql(s)
  

unique_viewing_sources_schema = t.StructType(field)
unique_viewing_sources = spark.createDataFrame([], unique_viewing_sources_schema) 

for demo in demos_of_interest:
  unique_viewing_sources_part = unique_viewing_sources_query(demo, 'pb_program_coded_stn_demo')
  unique_viewing_sources = unique_viewing_sources.union(unique_viewing_sources_part)

unique_viewing_sources.createOrReplaceTempView('unique_viewing_sources')

# COMMAND ----------

#############################################################
### Find distinct viewing sources per date/program/telecast/
### playback_type/demo/commercial
#############################################################

ads_by_call = spark.sql('''--sql
  SELECT
    ADS.*,
    SOURCE.viewing_source_short_name,
    SOURCE.playback_type,
    SOURCE.demo
  FROM
    ads_fact_summary AS ADS

  JOIN
    unique_viewing_sources AS SOURCE
  ON    TRUE
    AND SOURCE.telecast_broadcast_date = ADS.broadcast_date
    AND SOURCE.program_id              = ADS.program_id
    AND SOURCE.telecast_id             = ADS.telecast_id
    
  JOIN
    telecast_details AS TEL
  ON    TRUE
    AND TEL.telecast_broadcast_date = ADS.broadcast_date
    AND TEL.program_id              = ADS.program_id
    AND TEL.telecast_id             = ADS.telecast_id

  --endsql''')

ads_by_call.createOrReplaceTempView("ads_by_call")
ads_by_call.cache().count()

# COMMAND ----------

###########################################################
## distinct date/prgram/telecast/playback/demo are pulled
## from acm_ecm_table
###########################################################

acm_table_distinct = acm_table_enhanced \
  .select('telecast_broadcast_date',  # acm_ecm_table
          'telecast_broadcast_date_days_of_week',

          'playback_type',
          'demo',

          'viewing_source_short_name',
          'originator_common_service_name',

          'program_id',
          'program_name',
          'program_summary_type_desc',

          'telecast_id',
          'telecast_name',
          'report_start_time_ny',

          'ac_projection',
          'ac_rating').distinct()
                        
acm_table_distinct.createOrReplaceTempView('acm_table_distinct')

# COMMAND ----------

##############################################################
## Here commercials that are distributed across multiple 
## viewing sources and have 0 ECs are created
##############################################################

acm_ecm_table_with_zero_ec_commercial = \
  spark.sql('''--sql
    SELECT
      SOURCE.broadcast_date AS telecast_broadcast_date,
      ACM.telecast_broadcast_date_days_of_week,

      SOURCE.playback_type,
      SOURCE.demo,
      INTAB.ue,

      ACM.originator_common_service_name,
      SOURCE.viewing_source_short_name,

      SOURCE.program_id,
      ACM.program_name,
      ACM.program_summary_type_desc,

      SOURCE.telecast_id,
      ACM.telecast_name,
      ACM.report_start_time_ny,

      SOURCE.commercial_start_time,
      SOURCE.advertisement_duration AS ad_duration,
      SOURCE.pod_value,
      POD_COUNT.max_pod_in_telecast,
      POD_POS.position_in_pod,
      ADS_COUNT.commercial_counts_in_pod,
      SOURCE.advertisement_key,
      SOURCE.advertisement_code,
      SOURCE.brand_code,
      SOURCE.brand_desc,
      SOURCE.brand_variant_desc,
      SOURCE.creative_desc,
      SOURCE.pcc_industry_group_desc,

      ACM.ac_projection,
      ACM.ac_rating,
      0.0 AS ec_projection,
      0.0 AS ec_rating

    FROM
      ads_by_call AS SOURCE

    LEFT ANTI JOIN
      acm_ecm_table AS EVENT
    ON    TRUE
      AND EVENT.telecast_broadcast_date   = SOURCE.broadcast_date
      AND EVENT.program_id                = SOURCE.program_id
      AND EVENT.telecast_id               = SOURCE.telecast_id
      AND EVENT.demo                      = SOURCE.demo
      AND EVENT.viewing_source_short_name = SOURCE.viewing_source_short_name
      AND EVENT.playback_type             = SOURCE.playback_type
      AND EVENT.advertisement_key         = SOURCE.advertisement_key
      AND EVENT.advertisement_code        = SOURCE.advertisement_code
      AND EVENT.pod_value                 = SOURCE.pod_value
      AND EVENT.commercial_start_time     = SOURCE.commercial_start_time

    JOIN
      acm_table_distinct AS ACM
    ON    TRUE
      AND SOURCE.broadcast_date            = ACM.telecast_broadcast_date
      AND SOURCE.playback_type             = ACM.playback_type
      AND SOURCE.demo                      = ACM.demo
      AND SOURCE.viewing_source_short_name = ACM.viewing_source_short_name
      AND SOURCE.program_id                = ACM.program_id
      AND SOURCE.telecast_id               = ACM.telecast_id

    JOIN
      max_pod_in_telecast_df AS POD_COUNT
    ON    TRUE
      AND SOURCE.broadcast_date = POD_COUNT.broadcast_date
      AND SOURCE.program_id     = POD_COUNT.program_id
      AND SOURCE.telecast_id    = POD_COUNT.telecast_id

    JOIN
      commercial_counts_in_pod_df AS ADS_COUNT
    ON    TRUE
      AND SOURCE.broadcast_date = ADS_COUNT.broadcast_date
      AND SOURCE.program_id     = ADS_COUNT.program_id
      AND SOURCE.telecast_id    = ADS_COUNT.telecast_id
      AND SOURCE.pod_value      = ADS_COUNT.pod_value

    JOIN
      commercial_pos_in_pod_df AS POD_POS
    ON    TRUE
      AND SOURCE.broadcast_date        = POD_POS.broadcast_date
      AND SOURCE.program_id            = POD_POS.program_id
      AND SOURCE.telecast_id           = POD_POS.telecast_id
      AND SOURCE.advertisement_key     = POD_POS.advertisement_key
      AND SOURCE.advertisement_code    = POD_POS.advertisement_code
      AND SOURCE.pod_value             = POD_POS.pod_value
      AND SOURCE.commercial_start_time = POD_POS.commercial_start_time

    JOIN
      intab_df AS INTAB
    ON    TRUE
      AND SOURCE.broadcast_date = INTAB.intab_period_start_date
      AND SOURCE.demo           = INTAB.demo

    --endsql''')

# COMMAND ----------

#####################################################
## Here the original acm_ecm_table is appended to
## the acm_ecm_table_with_zero_ec_commercial dataset
#####################################################

acm_ecm_table3 = \
  (acm_ecm_table2 if drop_corrupt_mop_rows else acm_ecm_table) \
    .union(acm_ecm_table_with_zero_ec_commercial.select(*acm_ecm_table.columns))

# COMMAND ----------

acm_ecm_table3.cache().count()

# COMMAND ----------

display(acm_ecm_table3.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id).filter(f.col('viewing_source_short_name') == 'not exists'))

# COMMAND ----------

display(acm_ecm_table3.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id).filter(f.col('viewing_source_short_name') == 'LAFAF'))

# COMMAND ----------

display(acm_ecm_table3.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id).filter(f.col('viewing_source_short_name') == 'GRTAF'))

# COMMAND ----------

display(acm_ecm_table3.filter(f.col('playback_type') == "live").filter(f.col('telecast_broadcast_date')== telecast_broadcast_data) \
.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id))

# COMMAND ----------



# COMMAND ----------

display(pb_program_coded_stn_demo)

# COMMAND ----------

display(pb_program_coded_stn_demo.select(f.col('household_media_consumer_id').alias('hh_no'),f.col('viewing_source_short_name').alias('call'),f.col('source_media_device_number').alias('device_id'),'program_name','viewed_start_of_viewing_ny_time','viewed_end_of_viewing_ny_time','start_minute_of_program','end_minute_of_program','duration_in_minutes','weight').orderBy('viewed_start_of_viewing_ny_time','viewed_end_of_viewing_ny_time'))

# COMMAND ----------

display(ads_fact_dim.select('brand_desc','minute_of_program','advertisement_duration','commercial_start_time','first_minute_indicator').orderBy('commercial_start_time',f.desc('first_minute_indicator')))

# COMMAND ----------

display(commercial_minutes_adj.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id).orderBy('minute_of_program'))

# COMMAND ----------

display(acm_commercial_minutes_adj_ag.filter(f.col('program_id') == program_id).filter(f.col('telecast_id') == telecast_id))

# COMMAND ----------

##############################################################
### This python function computes ACM rating and projection 
### for a specified viewing file, ads_fact table, and demo 
### of interest
##############################################################

def ACM_computation_engine_rounded(demo,viewing_df):
  
  '''
 
  '''
  
  s = '''--sql
    WITH
      commercial_tuning AS (
        SELECT 
          VIEW.telecast_broadcast_date,
          VIEW.program_id,
          VIEW.telecast_id,
          VIEW.playback_type,
          VIEW.originator_common_service_name,
          VIEW.program_name,
          VIEW.telecast_name,
          VIEW.telecast_broadcast_date_days_of_week,
          VIEW.report_start_time_ny,
          VIEW.viewing_source_short_name,
          VIEW.program_summary_type_desc,
          COMMERCIAL.minute_of_program,
          COMMERCIAL.advertisement_duration_adj,
          sum(VIEW.weight)                        AS sow
        
        FROM 
          {viewing_df} AS VIEW
        
        JOIN
          commercial_minutes_adj AS COMMERCIAL
        
        WHERE TRUE
          AND VIEW.telecast_broadcast_date       = COMMERCIAL.broadcast_date
          and VIEW.program_id                    = COMMERCIAL.program_id
          and VIEW.telecast_id                   = COMMERCIAL.telecast_id
          and COMMERCIAL.minute_of_program BETWEEN start_minute_of_program
                                               AND end_minute_of_program
          AND VIEW.{demo}                        = 'Y'
        
        GROUP BY
          VIEW.telecast_broadcast_date, VIEW.program_id, VIEW.telecast_id, VIEW.playback_type,
          VIEW.originator_common_service_name, VIEW.program_name, VIEW.telecast_name, VIEW.telecast_broadcast_date_days_of_week,
          VIEW.report_start_time_ny, VIEW.viewing_source_short_name, VIEW.program_summary_type_desc,
          COMMERCIAL.minute_of_program, COMMERCIAL.advertisement_duration_adj
      )


    SELECT
      playback_type,
      program_name,
      report_start_time_ny,
      minute_of_program,
      advertisement_duration_adj,
      sow

    FROM
      commercial_tuning AS VIEW_AG

   

    --endsql'''

  query = s.format(demo = demo,
                   viewing_df = viewing_df)
  acm_table_part = spark.sql(query)

  return acm_table_part

acm_table_part = ACM_computation_engine_rounded("HH", 'pb_program_coded_stn_demo').filter('playback_type = "live"')
acm_table_part.createOrReplaceTempView('acm_table_part')
display(acm_table_part.orderBy('minute_of_program'))

# COMMAND ----------


