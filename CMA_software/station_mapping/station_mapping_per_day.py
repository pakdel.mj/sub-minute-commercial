# Databricks notebook source
import pyspark.sql.functions as f

# COMMAND ----------

# # ######################################
# # #####    USER INPUTS (manual)    #####
# # ######################################
# start_date                 = '2019-05-30'
# end_date                   = '2019-05-30'
start_date = str(dbutils.widgets.get('start_date'))
end_date = str(dbutils.widgets.get('end_date'))
# output_s3_dir              = 's3://useast1-nlsn-w-digital-dsci-dev/data/users/luje8004/station_mapping/' + start_date + '/'

########################################################################################################
meas_prd_strt              = start_date[0:4] + start_date[5:7] + start_date[8:]
meas_prd_end               = end_date[0:4] + end_date[5:7] + end_date[8:]

# COMMAND ----------

station_list = "'ABC', 'AMC', 'AOT', 'BBCA', 'CNN', 'COZAF', 'ENBRD', 'ENCR', 'ENCBL', 'FOXNC', 'FS1', 'FX', 'HBO', 'HIST', 'IFC', 'MAX', 'METAF', 'MSNBC', 'NBC', 'NGC', 'NGWD', 'SHOW', 'SPLGI', 'STRZ', 'SUND', 'TBSC', 'TWC', 'USA', 'WETV', 'CBS', 'ION', 'PBSPR', 'SYFY', 'AEN', 'APL', 'DISC', 'FOX', 'HGTV', 'INSP', 'OUTD', 'PAR', 'POP', 'SMTH', 'TLC', 'DSNY', 'ENT', 'HALL', 'HMM', 'NBCSN', 'BET', 'BRVO', 'CW', 'ESCAF', 'FOOD', 'FRFM', 'FUSE', 'FYI', 'GSN', 'ID', 'LIF', 'LMN', 'MTV', 'MTV2', 'NAN', 'NICK', 'OWN', 'TVL', 'TVLC', 'TV1', 'UP', 'WGNA', 'DXD', 'HLN', 'OXYG', 'TOON', 'ADSM', 'CMT', 'ESPNU', 'ESPN2', 'FBN', 'LOGO', 'NFLN', 'RLZC', 'SCI', 'UNIAF', 'VEL', 'VH1', 'BTN', 'CMDY', 'CNBC', 'ESPN', 'TNT', 'NKJR', 'OVTN', 'SPLCB', 'TELAF', 'TENN', 'UMAAF', 'UNVSO', 'DIY', 'FOXD', 'FS2', 'NKTNS', 'PBSS', 'TNNK', 'CC', 'ESNL', 'FXM', 'PRST', 'TRU', 'VICE', 'AHC', 'DLIF', 'GAC', 'MLBN', 'GOLF', 'BABY', 'ESPD', 'GALA', 'DSE', 'DAM', 'FXX', 'TRAV', 'NBAT', 'BOUAF', 'GRTAF', 'BEIE', 'BHER', 'MTVC', 'CDTV', 'REY', 'UDN', 'DFAM', 'DFC', 'RFD', 'ZLIV', 'BEIN', 'DISJR', 'AZAAF', 'SPMN', 'BOOM', 'FMTV', 'JCTV', 'CINL', 'ETV', 'LAFAF', 'COMAF', 'STVAF', 'HIAF'"

# COMMAND ----------

# MAGIC %md
# MAGIC **1. Calulate Station tuning and weights**

# COMMAND ----------

############## Station Mapping #####################
station_mapping_stal = spark.sql('''
SELECT DISTINCT v.call, v.viewing_source_name, c.metered_tv_credit_station_code, v.viewing_source_id
FROM(
SELECT DISTINCT viewing_source_id, viewing_source_short_name AS call, viewing_source_name
FROM tam_npm_mch_tv_prod.VIEWING_SOURCE
WHERE effective_start_date <= '{start_date}'
AND   effective_end_date >= '{end_date}'
AND   viewing_source_short_name <> ''
) v,
tam_npm_mch_tv_prod.VIEWING_SOURCE_TV_MEDIA_CREDIT c
WHERE v.viewing_source_id = c.viewing_source_id
AND   effective_start_date <= '{start_date}'
AND   effective_end_date >= '{end_date}'
AND   call IN ({station_list})  
'''.format(start_date = start_date, end_date = end_date, station_list = station_list))
station_mapping_stal.createOrReplaceTempView('station_mapping_stal')
spark.sql('cache table station_mapping_stal')
# display(station_mapping_stal) 
#station_mapping_mdl.write.parquet(output_s3_dir + 'station_mapping_mdl', mode='overwrite')

# COMMAND ----------

station_mapping = spark.sql('''
SELECT 'OTHER_SPCBL' AS call, viewing_source_name, metered_tv_credit_station_code AS station_code, viewing_source_id
FROM station_mapping_stal 
WHERE CALL IN ('SPLCB') 
AND metered_tv_credit_station_code NOT IN 
    (SELECT metered_tv_credit_station_code FROM station_mapping_stal WHERE call NOT IN ('SPLCB'))

UNION

SELECT * FROM station_mapping_stal WHERE call NOT IN ('SPLCB')

UNION

SELECT cat, '' AS viewing_source_name, station_code, 0 AS viewing_source_id
FROM (
select station_code, 
       CASE service_code                                                                             
            WHEN  'I' THEN 'OTHER_ENBRD'                                                                
            WHEN  'W' THEN 'OTHER_ENBRD'                                                                
            WHEN  '2' THEN 'OTHER_ENBRD'                                                                
            WHEN '02' THEN 'OTHER_ENBRD'                                                                
            WHEN '11' THEN 'OTHER_ENBRD'                                                                
            WHEN '22' THEN 'OTHER_ENBRD'                                                                
            WHEN '23' THEN 'OTHER_ENBRD'                                                                
            WHEN 'CC' THEN 'OTHER_ENBRD'                                                                
            WHEN  '3' THEN 'OTHER_ENCBL'                                                                
            WHEN '03' THEN 'OTHER_ENCBL'                                                                
            WHEN  '4' THEN 'OTHER_ENCBL'                                                                
            WHEN '04' THEN 'OTHER_ENCBL'                                                                
            WHEN  '5' THEN 'OTHER_ENCBL'                                                                
            WHEN '05' THEN 'OTHER_ENCBL'                                                                
            WHEN  '6' THEN 'OTHER_ENCBL'                                                                
            WHEN '06' THEN 'OTHER_ENCBL'                                                                
            WHEN  '7' THEN 'OTHER_ENCBL'                                                                
            WHEN '07' THEN 'OTHER_ENCBL'                                                                
            WHEN  '8' THEN 'OTHER_ENCBL'                                                                
            WHEN '08' THEN 'OTHER_ENCBL'                                                                
            WHEN  'D' THEN 'OTHER_ENCBL'                                                                
            WHEN  'G' THEN 'OTHER_ENCBL'                                                                
            WHEN  'H' THEN 'OTHER_ENCBL'                                                                
            WHEN  'K' THEN 'OTHER_ENCBL'                                                                
            WHEN  'Q' THEN 'OTHER_ENCBL'                                                                
            WHEN  'R' THEN 'OTHER_ENCBL'                                                                
            WHEN  'V' THEN 'OTHER_ENCBL'                                                                
            WHEN 'ZZ' THEN 'OTHER_ENCBL'                                                                
            WHEN '20' THEN 'OTHER_ENCBL'                                                                
            WHEN '21' THEN 'OTHER_ENCBL'
       END AS Cat
from
      tam_npm_mch_tv_aggregate_prod_v.station_dimension
where '{start_date}' BETWEEN start_date AND end_date
and station_code NOT IN 
                        (SELECT metered_tv_credit_station_code FROM station_mapping_stal)

) WHERE Cat IS NOT NULL


'''.format(start_date = start_date, end_date = end_date))
station_mapping.createOrReplaceTempView('station_mapping')
spark.sql('cache table station_mapping')
# display(station_mapping) 

# COMMAND ----------

station_mapping = station_mapping.withColumn('effective_date',f.lit(start_date))

# COMMAND ----------

station_mapping.write.parquet('s3://useast1-nlsn-w-digital-dsci-dev/data/users/pakdmj01/station_mapping/'+start_date,mode = 'overwrite')
