# Databricks notebook source
import pyspark.sql.types as t
import datetime

# COMMAND ----------

import pandas as pd
datelist = pd.date_range('2019-08-26', periods=36).tolist()
date_range = [datetime.datetime.strftime(i,'%Y-%m-%d') for i in datelist]
date_range

# COMMAND ----------

time1 = datetime.datetime.now()
for date in date_range:
  print('working on date : ',date)
  dbutils.notebook.run('station_mapping_per_day',5000,
                     {'start_date': date,
                      'end_date': date})
time2 = datetime.datetime.now()
print(time2 - time1)

# COMMAND ----------

field = [
  t.StructField("call",t.StringType(),  True),
  t.StructField("viewing_source_name",t.StringType(),  True),
  t.StructField("station_code",t.IntegerType(), True),
  t.StructField("viewing_station_id",t.IntegerType(),  True),
  t.StructField("effective_date",t.StringType(),  True)
  
]
station_maping_schema = t.StructType(field)
station_mapping_dated = spark.createDataFrame([], station_maping_schema)

# COMMAND ----------

time1 = datetime.datetime.now()
for date in date_range:
  print('working on date : ',date)
  
  station_mapping = spark.read.parquet('s3://useast1-nlsn-w-digital-dsci-dev/data/users/pakdmj01/station_mapping/'+date).cache()
  print('number of rows : ',station_mapping.count())
  
  station_mapping_dated = station_mapping_dated.union(station_mapping)
  print('the overal number', station_mapping_dated.cache().count())
time2 = datetime.datetime.now()
print(time2 - time1)

# COMMAND ----------

8847*3+8848

# COMMAND ----------

# MAGIC %fs ls s3://useast1-nlsn-w-digital-dsci-dev/data/users/pakdmj01/station_mapping/

# COMMAND ----------

station_mapping_dated.write.parquet('s3://useast1-nlsn-w-digital-dsci-dev/data/users/pakdmj01/station_mapping/2019-04-11/station_mapping_altered_sep_2019',mode = 'overwrite')

# COMMAND ----------

8721*36

# COMMAND ----------


