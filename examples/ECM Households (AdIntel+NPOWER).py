# Databricks notebook source
# MAGIC %md
# MAGIC # Purpose
# MAGIC 
# MAGIC This notebook demonstrates calculations of the ECM statistic for a sample retrieved from Ad Intel and NPOWER. This sample uses:
# MAGIC   * network stations
# MAGIC   * broadcast date of 2019-03-21
# MAGIC   * household-level tuning data

# COMMAND ----------

debugging = False

# COMMAND ----------

# Using a subfolder of the personal folder on S3,
# the two variables below are used by the Init notebook to generate the full path
# (see Init's output for the specific values)
user_id        = 'egar8001'
project_folder = 'Projects/Sub-minute ratings/'

checkpointing_folder = 'checkpoints/'

# COMMAND ----------

# MAGIC %run /Users/arseny.egorov@nielsen.com/Init

# COMMAND ----------

# MAGIC %md
# MAGIC # Imports

# COMMAND ----------

# `xlrd` is the Excel parser used by `pandas.read_excel`
dbutils.library.installPyPI('xlrd')

# COMMAND ----------

from pyspark.sql.functions import col, lit,\
                                  when,\
                                  unix_timestamp,\
                                  concat, trim, substring, upper,\
                                  round as sql_round,\
                                  max   as sql_max,\
                                  abs   as sql_abs  # Don't want to override the built-in
                                                    # `round`, `max`, and `abs`, so using aliases
from pyspark.sql.types import StructType, StructField, StringType, IntegerType

import pandas as pd
info_message('Pandas version',
             pd._version.get_versions()['version'])

# COMMAND ----------

# MAGIC %run ./ecm

# COMMAND ----------

# MAGIC %md
# MAGIC # Data

# COMMAND ----------

originator_lookup_df =\
  spark.sparkContext.parallelize([
    ('ABC',            'ABC'),
    ('AZA',            'AZA'),
    ('BOUNCE TV',      'BOU'),
    ('CBS',            'CBS'),
    ('COMET',          'COM'),
    ('COZI TV',        'COZI'),
    ('CW',             'CW'),
    ('ESCAPE',         'ESC'),
    ('ESTRELLA',       'ETV'),
    ('FOX',            'FOX'),
    ('GRIT',           'GRT'),
    ('HEROES & ICONS', 'HI'),
    ('ION',            'ION'),
    ('LAFF',           'LAF'),
    ('ME TV',          'MET'),
    ('NBC',            'NBC'),
    ('START TV',       'STV'),
    ('TEL',            'TEL'),
    ('UNI',            'UNI'),
    ('UNIMAS',         'UMA')
  ]).toDF(schema=StructType([
                   StructField('npower',   StringType()),
                   StructField('ad_intel', StringType())
                 ]))

# display(originator_lookup_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Ads Data

# COMMAND ----------

ads_file_name             = 'input/20190321 Ad Schedule.xlsx'  # All of Broadcast for the day, 2019-03-21
ads_excel_report_location = user_home_path + project_folder + ads_file_name

report_sheet_name = 'Report'
header_row        = 3
skip_rows         = range(header_row)

# COMMAND ----------

pd.set_option('precision', 2)
ads_pd = pd.read_excel(io=ads_excel_report_location,
                          # in different versions of `pandas`,
                          # the sheet name parameter is either `sheetname` or `sheet_name`,
                          # so setting both, for broader compatibility
                          sheetname =report_sheet_name,
                          sheet_name=report_sheet_name,
                          header=header_row,
                          skiprows=skip_rows,
                          # the thousands separator to use when parsing numbers
                          thousands =',')\
                [['Brand Variant',
                  'Distributor',
                  'Date',
                  'Time',
                  'POD',
                  'Duration',
                  'Program Title']]\
           .dropna()    # Dropping nulls to skip the subtotal rows

ads_pd['Date'] = ads_pd['Date'].astype(str)
ads_pd['Time'] = [r[1] for r in
                  ads_pd['Time'].astype(str)
                                .str.split()]

ads_pd['POD split'] = ads_pd['POD'].str.strip()\
                                   .str.split(r'\s*:\s*')
ads_pd['Pod']       = [r[0] for r in
                       ads_pd['POD split']]
ads_pd['Ad']        = [r[1] for r in
                       ads_pd['POD split']]

ads_pd = ads_pd.reset_index()\
               .drop(['index',
                      'POD',
                      'POD split'],
                     axis=1)

if debugging:
  ads_pd.head()

# COMMAND ----------

ads_df =\
  spark.createDataFrame(ads_pd) \
       .select(col('Date')                          .astype('date')  .alias('ad_broadcast_date'),
               upper(trim(col('Distributor')))                       .alias('ad_originator'),
               trim(col('Program Title'))                            .alias('ad_program_name'),
               col('pod')                           .astype('int')   .alias('ad_pod_number'),
               col('ad')                            .astype('int')   .alias('ad_in_pod'),
               col('Time')                                           .alias('ad_start_time'),
               col('duration')                      .astype('int')   .alias('ad_duration'),
               col('Brand Variant')                                  .alias('ad_brand')
              )

# COMMAND ----------

if debugging:
  ads_df.cache()
  display(ads_df.orderBy('ad_originator',
                         'ad_broadcast_date',
                         'ad_start_time'))

# COMMAND ----------

# MAGIC %md
# MAGIC ## Tuning Data

# COMMAND ----------

# Update to whatever Excel file/sheet with the NPOWER report you're using
npower_file_names = [
    'input/ACM Sample 20190321 Broadcast.xlsx'  # All of Broadcast for the date of 2019-03-21
  ]
npower_excel_report_locations = [user_home_path + project_folder + npower_file_name
                                 for npower_file_name in npower_file_names]

# program_sheet_name           = 'MC AA Program'
# telecast_sheet_name          = 'MC AA Telecast'
minute_sheet_name            = 'MC AA Minute'

# COMMAND ----------

pd.set_option('precision', 8)

tuning_pd =\
  pd.DataFrame(columns=['Broadcast Date',
                        'Originator',
                        'Program Name',
                        'Originator ID',
                        'Program ID',
                        'Telecast ID',
                        'Telecast Report Start Time',
                        'MOP Value',
                        'Commercial Duration',
                        'Promotional Duration',
                        'MC Weighted Viewing AA Minutes',
                        'Rating Base'])

for file in npower_excel_report_locations:
  tuning_part_pd = pd.read_excel(io=file,
                                 # in different versions of `pandas`,
                                 # the sheet name parameter is either `sheetname` or `sheet_name`,
                                 # so setting both, for broader compatibility
                                 sheetname =minute_sheet_name,
                                 sheet_name=minute_sheet_name,
                                 # the thousands separator to use when parsing numbers
                                 thousands =',')\
                       [['Broadcast Date',
                         'Originator',
                         'Program Name',
                         'Originator ID',
                         'Program ID',
                         'Telecast ID',
                         'Telecast Report Start Time',
                         'MOP Value',
                         'Commercial Duration',
                         'Promotional Duration',
                         'MC Weighted Viewing AA Minutes',
                         'Rating Base']]

  tuning_part_pd['Broadcast Date'] =\
    tuning_part_pd['Broadcast Date'].astype(str)

  tuning_part_pd['Telecast Report Start Time'] =\
    tuning_part_pd['Telecast Report Start Time'].astype(str)

  tuning_part_pd['MC Weighted Viewing AA Minutes'] =\
    tuning_part_pd['MC Weighted Viewing AA Minutes'].round(2)

  tuning_part_pd['Rating Base'] =\
    tuning_part_pd['Rating Base'].round()  # If the decimal isn't rounded, int() floors the decimal, making it smaller by one
  
  tuning_pd = tuning_pd.append(tuning_part_pd)
  
if debugging:
  tuning_pd.head()

# COMMAND ----------

tuning_df =\
  spark.createDataFrame(tuning_pd)\
       .select(col('Broadcast Date')                .astype('date')  .alias('tc_broadcast_date'),
               upper(trim(col('Originator')))                        .alias('tc_originator'),
               col('Originator ID')                 .astype('int')   .alias('tc_originator_id'),
               col('Program Name')                                   .alias('tc_program_name'),
               col('Program ID')                    .astype('int')   .alias('tc_program_id'),
               col('Telecast Report Start Time')                     .alias('tc_start_time'),
               col('Telecast ID')                   .astype('int')   .alias('tc_id'),
               col('MOP Value')                     .astype('int')   .alias('tc_mop'),
               col('MC Weighted Viewing AA Minutes').astype('double').alias('mop_sow'),
               col('Rating Base')                   .astype('int')   .alias('tc_total_ue'),
               col('Commercial Duration')           .astype('int')   .alias('mop_total_commercial_duration')
              )\
       .join(originator_lookup_df,                     # Replacing the NPOWER's value of the originator column with the Ad Intel's counterpart
             on=[col('npower') == col('tc_originator')],
             how='outer')\
       .drop('npower',
             'tc_originator')\
       .withColumnRenamed('ad_intel', 'tc_originator')

# COMMAND ----------

if debugging:
  tuning_df.cache()
  display(tuning_df.orderBy('tc_broadcast_date',
                            'tc_originator',
                            'tc_broadcast_date',
                            'tc_start_time',
                            'tc_mop'))

# COMMAND ----------

# MAGIC %md
# MAGIC # ECM Calculations

# COMMAND ----------

ads_with_ids_df = ads_df.withColumn('ad_id', col('ad_start_time'))

# COMMAND ----------

ads_df_props = ADS_DF_DEFAULTS.copy()

ads_df_props['originator ID column'] = 'ad_originator'
ads_df_props['ad ID column'] = 'ad_id'
ads_df_props['ad duration column'] = 'ad_duration'

ads_df_props['date column'] = 'ad_broadcast_date'
ads_df_props['ad start time column'] = 'ad_start_time'

ads_df_props

# COMMAND ----------

tuning_df_props = TUNING_DF_DEFAULTS.copy()

tuning_df_props['originator ID column'] = 'tc_originator'  # Using originator name because ads data doesn't have the ID
tuning_df_props['program ID column'] = 'tc_program_id'
tuning_df_props['telecast ID column'] = 'tc_id'

tuning_df_props['date column'] = 'tc_broadcast_date'
tuning_df_props['telecast start time column'] = 'tc_start_time'
tuning_df_props['MOP column'] = 'tc_mop'

tuning_df_props['SOW column'] = 'mop_sow'
tuning_df_props['UE column'] = 'tc_total_ue'

tuning_df_props

# COMMAND ----------

ecm_nielsen_answers_df = \
  ecm(ads_with_ids_df,
      tuning_df,
      ads_df_properties   =ads_df_props,
      tuning_df_properties=tuning_df_props) ['projections'].cache()

# COMMAND ----------

display(ecm_nielsen_answers_df.orderBy(tuning_df_props['originator ID column'],
                                       AD_START_TS_COL))

# COMMAND ----------

# save_dataframe(saved_dataframe = ecm_nielsen_answers_df,
#                saving_mode     = SAVING_MODE_PARQUET,
#                saving_location = user_home_path + project_folder + 'ecm_projections_nielsen_answers.parquet')