# Databricks notebook source
# MAGIC %md
# MAGIC # Purpose
# MAGIC 
# MAGIC This notebook demonstrates calculations of the ECM statistic for a sample retrieved from MDL. This sample uses:
# MAGIC   * network stations
# MAGIC   * broadcast date of 2019-03-21
# MAGIC   * household-level tuning data

# COMMAND ----------

debugging = False

# COMMAND ----------

# Using a subfolder of the personal folder on S3,
# the two variables below are used by the Init notebook to generate the full path
# (see Init's output for the specific values)
user_id        = 'egar8001'
project_folder = 'Projects/Sub-minute ratings/'

checkpointing_folder = 'checkpoints/'

# COMMAND ----------

# MAGIC %run /Users/arseny.egorov@nielsen.com/Init

# COMMAND ----------

# MAGIC %md
# MAGIC # Imports

# COMMAND ----------

from pyspark.sql.functions import col, when, \
                                  unix_timestamp, \
                                  concat, \
                                  max as sql_max, \
                                  sum as sql_sum

from pyspark.sql.types import IntegerType

# COMMAND ----------

# MAGIC %run ./ecm

# COMMAND ----------

# MAGIC %md
# MAGIC # Data

# COMMAND ----------

# 8=BROADCAST, 9=CABLE, 12=SYNDICATION
distributor_types   = [8]

# 0=Live
# TODO: update when changing to Live and Live+3 streams
playback_type_codes = [0]

broadcast_dates     = ['2019-03-21']

# COMMAND ----------

# MAGIC %md
# MAGIC ## Ads Data

# COMMAND ----------

ads_query = """\
  -- need DISTINCT because each minute of program has a separate entry in advertisement_fact
  SELECT DISTINCT
    ad_f.advertisement_key              AS ad_key,                   -- int
    ad_f.broadcast_date                 AS ad_broadcast_date,        -- str

    ad_f.program_distributor_id         AS ad_originator_id,         -- int
    tc_d.program_distributor_short_name AS ad_originator_short_name, -- str

    -- ad_f.program_id                     AS ad_program_id,            -- int
    tc_d.program_long_name              AS ad_program_long_name,     -- str

    -- ad_f.telecast_id                    AS ad_tcast_id,              -- int
    tc_d.episode_long_name              AS ad_tcast_long_name,       -- str
    
    -- tc_d.telecast_report_start_time     AS ad_tcast_start_time,      -- str
    
    ad_f.commercial_start_time          AS ad_start_time,            -- str
    ad_f.total_advertisement_duration   AS ad_duration              -- int

    -- ad_d.brand_desc                     AS ad_brand_desc,            -- str
    -- ad_d.brand_variant_desc             AS ad_brand_variant_desc,    -- str
    -- ad_d.product_desc                   AS ad_product_desc,          -- str
    -- ad_d.creative_desc                  AS ad_creative_desc,         -- str
    -- ad_d.parent_desc                    AS ad_parent_desc            -- str

  FROM
    TAM_NPM_MCH_TV_PROD.advertisement_fact
          AS ad_f

  JOIN
    TAM_NPM_MCH_TV_PROD.advertisement_dimension
          AS ad_d
  ON    TRUE
    AND ad_f.broadcast_date        IN  ({broadcast_dates})
    AND ad_f.tcast_deleted_flag     =  'No'
    AND ad_f.tcast_valid_data_flag  =   1
    AND ad_d.advertisement_key      =   ad_f.advertisement_key
    
    -- excluding Promotionals and PSAs --
    AND ad_d.promotional_flag      IN ('1', 'N')  -- can be '0', '1' or 'N'
    AND ad_d.parent_code           !=   2         -- 2 = 'PSA PARENT'

  JOIN
    TAM_NPM_EDW_TV_PROD.telecast_dimension
          AS tc_d
  ON    TRUE
    AND tc_d.telecast_broadcast_date        =  ad_f.broadcast_date
    AND tc_d.program_distributor_type_code IN ({distributor_types})
    AND tc_d.telecast_key                   =  ad_f.telecast_key
  """\
    .format(broadcast_dates  =', '.join([
                                     "'" + d + "'"
                                     for d in broadcast_dates
                                   ]),
            distributor_types=', '.join([
                                     str(t)                      # `distributor_types` contains integers
                                     for t in distributor_types
                                   ]))

ads_df = spark.sql(ads_query)

# COMMAND ----------

if debugging:
  ads_df.cache()
  display(ads_df.orderBy('ad_originator_short_name',
                         'ad_broadcast_date',
                         'ad_start_time'))

# COMMAND ----------

# MAGIC %md
# MAGIC ## Tuning Data

# COMMAND ----------

tuning_fact_query = """\
  SELECT
    sample_characteristic_intab_period_id,
    report_originator_lineup_key,
    
    household_media_consumer_id,
    location_media_consumer_id,
    
    start_minute_of_program,
    end_minute_of_program
    
  FROM
    TAM_NPM_MCH_TV_PROD.program_coded_metered_tv_location_media_engagement_event
    
  WHERE TRUE
    AND viewed_date                  IN ({viewed_dates})
    AND released_for_processing_flag  = 'Y'
    AND playback_type_code           IN ({playback_type_codes})  -- 0=Live, 1=SD, 2=everything else (2nd day and later)
    AND media_device_category_code    =  1                      -- 1=TV, 2=PC, 3=OOH, 4=Mobile
    AND is_simultaneous_viewing_flag  = 'N'
"""\
  .format(viewed_dates       =', '.join([
                                     date.replace('-', '')        # `viewed_dates` is in the 'yyyyMMdd' format, so removing the dashes
                                     for date in broadcast_dates
                                   ]),
          playback_type_codes=', '.join([
                                     str(code)                        # `playback_type_codes` contains integers
                                     for code in playback_type_codes
                                   ]))

tuning_fact_df = \
  spark.sql(tuning_fact_query)

tuning_fact_df.createOrReplaceTempView('tuning_fact')

# COMMAND ----------

max_minute = \
  tuning_fact_df.select(sql_max('end_minute_of_program').alias('max_minute')) \
                .collect()[0]['max_minute']

minutes_df =\
  spark.sparkContext.parallelize(range(1, max_minute + 1)) \
                    .toDF(schema=IntegerType()) \
                    .select(col('value').alias('mop'))

# COMMAND ----------

tuning_by_qh_query = """\
  SELECT
    lineup.national_tv_broadcast_date    AS tc_broadcast_date,
    lineup.report_originator_lineup_key  AS tc_key,

    lineup.originator_distributor_id     AS tc_originator_id,
    origr.originator_name                AS tc_originator_name,

    lineup.national_tv_report_program_id AS tc_program_id,
    lineup.program_name                  AS tc_program_name,

    lineup.report_originator_lineup_id   AS tc_id,
    lineup.episode_name                  AS tc_episode_name,
    
    lineup.report_start_time_ny          AS tc_start_time,

    -- tuning.household_media_consumer_id   AS hh_household_media_consumer_id,  -- these will be removed during the aggregation anyway
    -- tuning.location_media_consumer_id    AS hh_location_media_consumer_id,
    intab_weight.location_weight         AS hh_weight,

    tuning.start_minute_of_program       AS hh_start_minute_of_program,
    tuning.end_minute_of_program         AS hh_end_minute_of_program

  FROM
    TAM_NPM_MCH_TV_PROD.report_originator_lineup
            AS lineup

  JOIN
    tuning_fact
            AS tuning
  ON    TRUE  
    AND lineup.national_tv_broadcast_date   IN ({broadcast_dates})
    AND lineup.released_for_processing_flag  =  'Y'
    AND lineup.multi_barter_flag            !=  'Y'  -- Determines how ad spots are filled by local stations
    AND lineup.cash_barter_flag             !=  'Y'  -- Determines how ad spots are filled by local stations
    AND lineup.lineup_release_type_code      =   1   -- Currency telecasts only (excluding Fasties, Retro Coded telecasts)
    AND lineup.originator_type_code         IN ({distributor_types})  -- 8=Broadcast, 9=Cable, 12=Syndication
    AND tuning.report_originator_lineup_key  =   lineup.report_originator_lineup_key

  JOIN 
    TAM_NPM_MCH_TV_PROD.national_tv_content_originator
            AS origr
  ON    TRUE
    AND origr.originator_distributor_id   =         lineup.originator_distributor_id
    AND origr.distributor_type_code       IN      ({distributor_types})  -- 8=Broadcast, 9=Cable, 12=Syndication
    AND lineup.national_tv_broadcast_date BETWEEN   origr.effective_start_date
                                          AND       origr.effective_end_date

  -- JOIN
  --   TAM_NPM_MCH_TV_PROD.metered_tv_household_location_intab_status
  --           AS intab
  -- ON    TRUE
  --   AND intab.intab_period_id               =   tuning.sample_characteristic_intab_period_id
  --   AND intab.household_media_consumer_id   =   tuning.household_media_consumer_id
  --   AND intab.media_consumer_id             =   tuning.location_media_consumer_id
  --   AND intab.released_for_processing_flag  =  'Y'
  --   AND intab.sample_collection_method_key IN  (7)  -- 7=NPM Homes, see TAM_NPM_MCH_TV_PROD.sample_collection_method
  --   AND intab.is_intab_flag                 =  'Y'

  JOIN
    TAM_NPM_MCH_TV_PROD.metered_tv_household_location_intab_and_weight
            AS intab_weight
  ON    TRUE
    AND intab_weight.is_intab_indicator                    =  1
    AND intab_weight.released_for_processing_flag          = 'Y'
    AND intab_weight.media_device_category_code            =  1  -- tuning.media_device_category_code,    1=TV, 2=PC, 3=OOH, 4=Mobile
    AND intab_weight.sample_collection_method_key          =  7  -- intab.sample_collection_method_key,  7=NPM Homes, see TAM_NPM_MCH_TV_PROD.sample_collection_method
    AND intab_weight.sample_characteristic_intab_period_id =  tuning.sample_characteristic_intab_period_id
    AND intab_weight.household_media_consumer_id           =  tuning.household_media_consumer_id
    AND intab_weight.location_media_consumer_id            =  tuning.location_media_consumer_id
  """\
    .format(broadcast_dates  =', '.join([
                                     "'" + date + "'"
                                     for date in broadcast_dates
                                   ]),
            distributor_types=', '.join([
                                     str(type)  # `distributor_types` contains integers
                                     for type in distributor_types
                                   ]))

tuning_by_qh_df = spark.sql(tuning_by_qh_query)

# COMMAND ----------

if debugging:
  tuning_by_qh_df.cache()
  display(tuning_by_qh_df.orderBy('tc_originator_name',
                                  'tc_id', 
                                  'hh_start_minute_of_program'))

# COMMAND ----------

tuning_by_minute_df = \
  tuning_by_qh_df.join(minutes_df.hint('broadcast'),
                       on=[
                         col('hh_start_minute_of_program') <= col('mop'),
                         col('mop')                     <= col('hh_end_minute_of_program')
                       ])

# COMMAND ----------

if debugging:
  tuning_by_minute_df.cache()
  display(tuning_by_minute_df.orderBy('originator_name',
                                      'telecast_id',
                                      'household_media_consumer_id',
                                      'mop'))

# COMMAND ----------

tuning_sow_df = \
  tuning_by_minute_df.groupBy('tc_broadcast_date',
                              'tc_key',
                              'tc_originator_id',
                              'tc_originator_name',
                              'tc_program_id',
                              'tc_program_name',
                              'tc_id',
                              'tc_start_time',
                              'tc_episode_name',
                              'mop') \
                     .agg(sql_sum('hh_weight').alias('sow'))

# COMMAND ----------

ues_query = """\
  SELECT
    universe_estimate AS ue,
    effective_start_date,
    effective_end_date
  FROM
    tam_npm_mch_tv_prod.national_tv_weighting_control_universe_estimate
  WHERE true
    AND household_or_person_code           = 'H'
    AND universe_estimate_type_code        =  1
    AND released_for_processing_flag       = 'Y'
    AND weighting_control_id               =  9999"""

ues_df = spark.sql(ues_query)

# COMMAND ----------

if debugging:
  tuning_sow_df.cache()
  display(tuning_sow_df.orderBy('originator_name',
                                'telecast_id',
                                'mop'))

# COMMAND ----------

tuning_df = \
  tuning_sow_df.join(ues_df,
                     on=[
                       col('effective_start_date') <= col('tc_broadcast_date'),
                       col('tc_broadcast_date')    <= col('effective_end_date')
                     ]) \
               .drop('effective_start_date',
                     'effective_end_date')

# COMMAND ----------

if debugging:
  tuning_df.cache()
  display(tuning_df.orderBy('tc_originator_name',
                            'tc_start_time',
                            'mop'))

# COMMAND ----------

# MAGIC %md
# MAGIC # ECM Calculations

# COMMAND ----------

ads_df_props = ADS_DF_DEFAULTS.copy()

ads_df_props['originator ID column'] = 'ad_originator_id'
ads_df_props['ad ID column'] = 'ad_key'
ads_df_props['ad duration column'] = 'ad_duration'

ads_df_props['date column'] = 'ad_broadcast_date'
ads_df_props['ad start time column'] = 'ad_start_time'

ads_df_props

# COMMAND ----------

tuning_df_props = TUNING_DF_DEFAULTS.copy()

tuning_df_props['originator ID column'] = 'tc_originator_id'
tuning_df_props['program ID column'] = 'tc_program_id'
tuning_df_props['telecast ID column'] = 'tc_id'

tuning_df_props['date column'] = 'tc_broadcast_date'
tuning_df_props['telecast start time column'] = 'tc_start_time'
tuning_df_props['MOP column'] = 'mop'

tuning_df_props['SOW column'] = 'sow'
tuning_df_props['UE column'] = 'ue'

tuning_df_props

# COMMAND ----------

ecm_mdl_df =\
  ecm(ads_df, tuning_df,
      ads_df_properties=ads_df_props,
      tuning_df_properties=tuning_df_props)['projections'].cache()

# COMMAND ----------

display(ecm_mdl_df.orderBy(tuning_df_props['originator ID column'],
                           AD_START_TS_COL))  # Couple minutes on Viridian with 72 threads

# COMMAND ----------

# save_dataframe(saved_dataframe = ecm_mdl_df,
#                saving_mode     = SAVING_MODE_PARQUET,
#                saving_location = user_home_path + project_folder + 'ecm_projections_mdl.parquet')