# Databricks notebook source
# TODO: make `ecm.py` an installable "wheel"
"""
A set of functions for calculating the ECM statistics provided
an ad schedule and tuning/viewing data.
"""

from pyspark.sql.functions import col, lit, \
                                  when, \
                                  round as sql_round, \
                                  unix_timestamp, \
                                  concat
from pyspark.sql.types     import IntegerType


def timestamp_offset(time, cutoff='06'):
  """
  Returns 24 hours if the time is earlier than the cutoff, otherwise returns 0.
  
  :param time: A string representing the time to check against the cut-off time.
  :type  time: `str`
  
  :param cutoff: A string representing the cut-off time. The default value of '06' is based on the time format 'HH:mm:ss' and the cut-off time of 06:00:00.
  :type  cutoff: `str`
  
  :return: The offset time in seconds (24 hours = 86400 s).
  :rtype:  `int`
  """
  
  return 86400 if time < cutoff else 0


# This registers the above function as a function that can be appied to columns in Spark SQL
ts_offset = spark.udf.register('ts_offset', timestamp_offset, IntegerType())


# Standardized column names used in ads dataframes
AD_ORIGR_ID_COL    = 'ad_oringinator_id'
AD_ID_COL          = 'ad_id'

AD_DATE_COL        = 'ad_date'
AD_START_TIME_COL  = 'ad_start_time'
AD_DURATION_COL    = 'ad_duration'


def _ecm_standardized_ad_cols(ads_df,
                              ad_origr_id_col, ad_id_col,
                              ad_date_col, ad_start_time_col,
                              ad_duration_col):
  """
  Replaces custom column names in a Spark dataframe containing ads data with standardized ones
  and creates a column with the timestamp for the end of each ad.
  
  :param ads_df:  The dataframe with ads data  
  :type  ads_df:  :class:`pyspark.sql.DataFrame`
  
  :param ad_origr_id_col:  The name of the column containing ad originator ID  
  :type  ad_origr_id_col:  `str`
  
  :param ad_id_col:  The name of the column containing ad ID  
  :type  ad_id_col:  `str`
  
  :param ad_date_col:  The name of the column containing ad airing date  
  :type  ad_date_col:  `str`
  
  :param ad_start_time_col:  The name of the column containing ad start time  
  :type  ad_start_time_col:  `str`
  
  :param ad_duration_col:  The name of the column containing ad duration  
  :type  ad_duration_col:  `str`
  
  :return:  A new Spark dataframe with the standardized column names  
  :rtype:  :class:`pyspark.sql.DataFrame`
  """
  
  return ads_df \
    .select(ad_origr_id_col, ad_id_col,
            ad_date_col,
            ad_start_time_col, ad_duration_col) \
    .withColumnRenamed(ad_origr_id_col,   AD_ORIGR_ID_COL) \
    .withColumnRenamed(ad_id_col,         AD_ID_COL) \
    .withColumnRenamed(ad_date_col,       AD_DATE_COL) \
    .withColumnRenamed(ad_start_time_col, AD_START_TIME_COL) \
    .withColumnRenamed(ad_duration_col,   AD_DURATION_COL)


AD_START_TS_COL    = 'ad_start_ts'
AD_END_TS_COL      = 'ad_end_ts'


def _ecm_ads_with_timestamps(ads_df,
                             date_format, time_format,
                             broadcast_date_start_time):
  """
  Adds columns with calculated timestamps:
    * ad start
    * ad end
  to a dataframe with ads data.

  Expected columns:
    * Ad airing date
    * Ad start time
    * Ad duration
  
  The column names are expected to be standardized.

  :param ads_df:  The dataframe with ads data  
  :type  ads_df:  :class:`pyspark.sql.DataFrame`

  :param date_format:  A format string for the ad airing date column; e.g, 'yyyy-MM-dd'  
  :type  date_format:  `str`

  :param time_format:  A format string for the ad start time column; e.g, 'HH:mm:ss'  
  :type  time_format:  `str`

  :param broadcast_date_start_time:  The time at which a broadcast date begins (usually, 6 AM);
  generally, expected in the same format as the time column, but can be shortened, since it's only used for comparison  
  For example, for the 'HH:mm:ss' format, the string '06:00:00' could be shortened to just '06' as it would yield the same comparison results  
  :type  broadcast_date_start_time:  `str`

  :return:  A new dataframe with the two calculated columns  
  :rtype:   :class:`pyspark.sql.DataFrame`
  """

  return ads_df \
    .withColumn(AD_START_TS_COL,                                          # Start of the ad timestamp

                unix_timestamp(concat(col(AD_DATE_COL), col(AD_START_TIME_COL)),  # A naive timestamp from the broadcast date and time
                               date_format + time_format)
                + ts_offset(col(AD_START_TIME_COL),                               # Adding 24 hours for the ads whose start time is
                                                                                  # before broadcast date cutoff (usually, 6 am)
                            lit(broadcast_date_start_time))) \
    .withColumn(AD_END_TS_COL,                                            # End of the ad timestamp

                col(AD_START_TS_COL) + col(AD_DURATION_COL))


# Standardized column names used in tuning dataframes
TCAST_ORIGR_ID_COL    = 'tc_originator_id'
TCAST_PROG_ID_COL     = 'tc_program_id'
TCAST_ID_COL          = 'tc_id'

TCAST_START_TIME_COL  = 'tc_start_time'
TCAST_DATE_COL        = 'tc_date'
TCAST_MOP_COL         = 'tc_mop'

MOP_SOW_COL           = 'mop_sow'
DAY_UE_COL            = 'day_ue'


def _ecm_standardized_tuning_cols(tuning_df,
                                  tcast_origr_id_col, tcast_prog_id_col, tcast_id_col,
                                  tcast_date_col, tcast_start_time_col, tcast_mop_col,
                                  mop_sow_col, day_ue_col):
  """
  Replaces custom column names in a Spark dataframe containing tuning/viewing data
  with standardized ones
  and creates a column with the timestamps for the start and end of each minute of program.
  
  :param tuning_df: The dataframe with tuning/viewing data  
  :type tuning_df: :class:`pyspark.sql.DataFrame`
  
  :param tcast_origr_id_col: The name of the column containing telecast originator ID  
  :type tcast_origr_id_col: `str`
  
  :param prog_id_col: The name of the column containing program ID  
  :type prog_id_col: `str`
  
  :param tcast_id_col: The name of the column containing telecast ID  
  :type tcast_id_col: `str`
  
  :param tcast_date_col: The name of the column containing telecast airing date 
  :type tcast_date_col: `str`
  
  :param tcast_start_time_col: The name of the column containing telecast start time  
  :type tcast_start_time_col: `str`
  
  :param mop_col: The name of the column containing minute of program  
  :type mop_col: `str`
  
  :param sow_col: The name of the column containing sum of weights  
  :type sow_col: `str`
  
  :param ue_col: The name of the column containing universe estimate  
  :type ue_col: `str`
  
  :return: A new Spark dataframe with standardized column names  
  :rtype: :class:`pyspark.sql.DataFrame`
  """
  
  return tuning_df \
    .select(tcast_origr_id_col, tcast_prog_id_col, tcast_id_col,
            tcast_date_col, tcast_start_time_col, tcast_mop_col,
            mop_sow_col, day_ue_col) \
    .withColumnRenamed(tcast_origr_id_col,   TCAST_ORIGR_ID_COL) \
    .withColumnRenamed(tcast_prog_id_col,    TCAST_PROG_ID_COL) \
    .withColumnRenamed(tcast_id_col,         TCAST_ID_COL) \
    .withColumnRenamed(tcast_date_col,       TCAST_DATE_COL) \
    .withColumnRenamed(tcast_start_time_col, TCAST_START_TIME_COL) \
    .withColumnRenamed(tcast_mop_col,        TCAST_MOP_COL) \
    .withColumnRenamed(mop_sow_col,          MOP_SOW_COL) \
    .withColumnRenamed(day_ue_col,           DAY_UE_COL)


TCAST_START_TS_COL    = 'tcast_start_ts'
MOP_START_TS_COL      = 'mop_start_ts'
MOP_END_TS_COL        = 'mop_end_ts'


def _ecm_tuning_with_timestamps(tuning_df,
                                date_format, time_format,
                                broadcast_date_start_time):
  """
  Adds columns with calculated timestamps:
    * telecast start 
    * minute of program start
    * minute of program end
  to a dataframe with tuning data.

  Expected columns:
    * Telecast airing date
    * Telecast start time
    * MOP (minute of program)
  
  The column names are expected to be standardized.

  :param tuning_df:  The dataframe with tuning data  
  :type  tuning_df:  :class:`pyspark.sql.DataFrame`

  :param date_format:  A format string for the telecast airing date column; e.g, 'yyyy-MM-dd'  
  :type  date_format:  `str`

  :param time_format:  A format string for the telecast start time column; e.g, 'HH:mm:ss'  
  :type  time_format:  `str`

  :param broadcast_date_start_time:  The time at which a broadcast date begins (usually, 6 AM);
  generally, expected in the same format as the time column, but can be shortened, since it's only used for comparison  
  For example, for 'HH:mm:ss' the format, the string '06:00:00' could be shortened to just '06' as it would yield the same comparison results  
  :type  broadcast_date_start_time:  `str`

  :return:  A new dataframe with the two calculated columns  
  :rtype:   :class:`pyspark.sql.DataFrame`
  """

  return tuning_df \
    .withColumn(TCAST_START_TS_COL,                                       # Start of the telecast timestamp

                unix_timestamp(concat(col(TCAST_DATE_COL), col(TCAST_START_TIME_COL)),  # A naive timestamp from the broadcast date and time
                               date_format + time_format)
                + ts_offset(col(TCAST_START_TIME_COL),                            # Adding 24 hours for the telecasts whose start time is
                                                                                  # before broadcast date cutoff (usually, 6 am)
                            lit(broadcast_date_start_time))) \
    .withColumn(MOP_START_TS_COL,                                         # Start of the minute timestamp

                col(TCAST_START_TS_COL) + 60*(col(TCAST_MOP_COL) - 1)) \
    .withColumn(MOP_END_TS_COL,                                           # End of the minute timestamp

                col(TCAST_START_TS_COL) + 60*col(TCAST_MOP_COL))


# Column names used in joint dataframes
MINUTE_AD_DURATION_COL = 'minute_ad_duration'


def _ecm_tuning_with_ads(tuning_df, ads_df):
  """
  Joins the tuning/viewing and ads data minute-by-minute
  and creates a column with the ad duration during the minute of program.
  Thus, ads spanning across several minutes of program
  yield one record for each minute of program they overlap.
  
  :param tuning_df: The tuning/viewing data
  
  :type tuning_df: :class:`pyspark.sql.DataFrame`
  
  Expected columns for the tuning/viewing data:
    * Telecast originator ID
    * Minute of program start timestamp
    * Minute of program end timestamp
  
  :param ads_df: The ads data
  
  :type ads_df: :class:`pyspark.sql.DataFrame`
  
  Expected columns for the ads data:
    * Ad originator ID
    * Ad start timestamp
    * Ad end timestamp
  
  The column names are expected to be standardized.
  
  :return: A new Spark dataframe with the combined data
  
  :rtype: :class:`pyspark.sql.DataFrame`
  
  The result includes the following columns:
    * Telecast originator ID
    * Minute of program start timestamp
    * Minute of program end timestamp
    * Ad originator ID
    * Ad start timestamp
    * Ad end timestamp
    * Ad's duration within the minute of program
  """
  
  # See https://docs.databricks.com/delta/join-performance/range-join.html#id1
  # for Range Join optimization info.
  # The bin size is set to 60 (since it applies to the timestamp condition, it's 60 seconds),
  # since minutes are 60 seconds long
  #
  # tuning_df.hint('range_join', '60') won't work for some reason,
  # so using cluster config until that's sorted out
  # TODO: figure out how to use the hint method
  spark.sql('SET spark.databricks.optimizer.rangeJoin.binSize=60')

  return tuning_df \
    .join(ads_df,
          [ # Join conditions:
            #   The originator IDs should match
            col(AD_ORIGR_ID_COL) == col(TCAST_ORIGR_ID_COL),
            #   The ad spans across the minutes that:
            #     * start strictly before the ad ends, and
            #     * end strictly after the ad starts
            col(AD_END_TS_COL)   >  col(MOP_START_TS_COL),         #  minute ->     N        N+1      N+2      N+3
            col(AD_START_TS_COL) <  col(MOP_END_TS_COL)            #                |     +++|++++++++|++      |
                                                                   #                      ^              ^
                                                                   #      ad ->           start(incl)    end(excl)
          ]) \
    .withColumn(MINUTE_AD_DURATION_COL,                            # Ad duration within the minute of program. Cases:
                when(col(MOP_START_TS_COL) < col(AD_START_TS_COL),            # The ad starts strictly after the minute, and
                     when(col(AD_END_TS_COL) < col(MOP_END_TS_COL),           #   - ends strictly before the minute
                          col(AD_DURATION_COL))                               #     |        |  +++   |        |
                     .otherwise(                                              #   - ends with/goes over the minute
                          col(MOP_END_TS_COL) - col(AD_START_TS_COL)))        #     |     +++|--------|--      |
                .otherwise(                                                   # The ad starts with/extends into the minute, and
                     when(col(AD_END_TS_COL) < col(MOP_END_TS_COL),           #   - ends strictly before the minute
                          col(AD_END_TS_COL) - col(MOP_START_TS_COL))         #     |     ---|--------|++      |
                     .otherwise(                                              #   - spans across the entire minute
                          60)))                                               #     |     ---|++++++++|--      |


MINUTE_AD_WEIGHT_COL     = 'minute_ad_weight'
ECM_PROJECTION_COL       = 'ECM_projection'
ECM_PROJECTION_PRECISION = 0


def _ecm_projections(tuning_ads_df):
  """
  Creates a column with the weighted projections of individual ad airings.
  
  The weighted projections are calculated according to the formula:
  
  ``sum(sow[i] * dur[i]) / ad_duration``,
  
  where
    * ``sow[i]``      is the sum of household/person weights during minute ``i``,
    * ``dur[i]``      is the number of seconds the ad spans across minute ``i``, and
    * ``ad_duration`` is the total airing duration of the ad.
  
  :param tuning_ads_df: The minute-by-minute tuning data combined with ads data
  
  :type tuning_ads_df: :class:`pyspark.sql.DataFrame`
  
  Expected input columns:
    * Telecast originator ID
    * Program ID
    * Telecast ID
    * Ad ID
    * Ad airing date
    * Ad start time
    * Ad start timestamp
    * Ad duration within the minute of program - These two columns are aggregated
    * SOW during the minute of program         - into the newly created projection
    * UE during the telecast

  The column names are expected to be standardized.
  
  :return: A new Spark dataframe containing the ECM projections and UEs by the airing
  
  The result has the following columns:
    * Telecast originator ID
    * Program ID
    * Telecast ID
    * Ad ID
    * Ad start timestamp
    * UE during the telecast
    * Audience projection for the ad
    
  :rtype: :class:`pyspark.sql.DataFrame`
  """
  
  sum_ad_weight_col = 'sum({})'.format(MINUTE_AD_WEIGHT_COL)

  return tuning_ads_df \
    .withColumn(MINUTE_AD_WEIGHT_COL,                             # The product in the numerator
                col(MOP_SOW_COL) * col(MINUTE_AD_DURATION_COL)) \
    .groupBy(TCAST_ORIGR_ID_COL,
             TCAST_PROG_ID_COL,
             TCAST_ID_COL,
             AD_ID_COL,
             AD_DATE_COL,
             AD_START_TIME_COL,
             AD_START_TS_COL,
             AD_DURATION_COL,
             DAY_UE_COL) \
    .agg({MINUTE_AD_WEIGHT_COL: 'sum'}) \
    .withColumn(ECM_PROJECTION_COL,
                sql_round(col(sum_ad_weight_col) / col(AD_DURATION_COL),
                          ECM_PROJECTION_PRECISION)) \
    .drop(sum_ad_weight_col)


ECM_RATING_COL       = 'ECM_rating'
AVG_ECM_RATING_COL   = 'avg_ECM_rating'
ECM_RATING_PRECISION = 6


# TODO: different report levels, such as time period, program, telecast, campaign, etc.
def _ecm_ratings(ad_projections_df):
  """
  Calculates average ECM ratings.
  The average is taken over all airings with a matching ad ID.
  
  :param ad_projections_df: The ads data with airings projections and UEs
  
  :type ad_projections_df: :class:`pyspark.sql.DataFrame`
  
  Expected input columns:
    * Ad ID
    * Audience projection for the ad
    * UE during the telecast
  
  The column names are expected to be standardized.
  
  :return: A new Spark dataframe containing the average ECM ratings by the ad ID
  
  The result has the following columns:
    * Ad ID
    * Ad's rating
  
  :rtype: :class:`pyspark.sql.DataFrame`
  """
  
  avg_ecm_rating_col = 'avg({})'.format(ECM_RATING_COL)  # The name of the column with ECM average

  return ad_projections_df \
    .withColumn(ECM_RATING_COL,
                col(ECM_PROJECTION_COL) / col(DAY_UE_COL)) \
    .groupBy(AD_ID_COL) \
    .agg({ECM_RATING_COL: 'avg'}) \
    .withColumn(AVG_ECM_RATING_COL,
                sql_round(col(avg_ecm_rating_col),
                          ECM_RATING_PRECISION)) \
    .drop(avg_ecm_rating_col)


# Standard formats for date and time
STD_DATE_FORMAT              = 'yyyy-MM-dd'
STD_TIME_FORMAT              = 'HH:mm:ss'
STD_BROADCAST_DAY_START_TIME = '06'  # Shortened '06:00:00', since it suffices for comparison


ADS_DF_DEFAULTS = {
  'date format'              : STD_DATE_FORMAT,
  'time format'              : STD_TIME_FORMAT,
  'broadcast day start time' : STD_BROADCAST_DAY_START_TIME,

  'originator ID column'     : AD_ORIGR_ID_COL,
  'ad ID column'             : AD_ID_COL,
  'date column'              : AD_DATE_COL,
  'ad start time column'     : AD_START_TIME_COL,
  'ad duration column'       : AD_DURATION_COL
}

TUNING_DF_DEFAULTS = {
  'date format'                : STD_DATE_FORMAT,
  'time format'                : STD_TIME_FORMAT,
  'broadcast day start time'   : STD_BROADCAST_DAY_START_TIME,

  'originator ID column'       : TCAST_ORIGR_ID_COL,
  'program ID column'          : TCAST_PROG_ID_COL,
  'telecast ID column'         : TCAST_ID_COL,
  'date column'                : TCAST_DATE_COL,
  'telecast start time column' : TCAST_START_TIME_COL,
  'MOP column'                 : TCAST_MOP_COL,
  'SOW column'                 : MOP_SOW_COL,
  'UE column'                  : DAY_UE_COL
}


def ecm(ads_df, tuning_df,
        ad_ids              =[],
        ads_df_properties   =ADS_DF_DEFAULTS,
        tuning_df_properties=TUNING_DF_DEFAULTS):
  """
  Calculates the Exact Commercial Minute projections for the ads
  provided the tuning/viewing information.
  
  Expected ads data columns:
    * Originator ID - a unique station ID             (?)
    * Ad ID                                           (?)  

    * Date                                            (string)
    * Ad start time: hours, minutes, and seconds      (string)
    * Ad duration, in seconds                         (integer)
  
  Expected tuning data columns:
    * Oiginator ID - a unique station ID                   (?)
    * Program ID - a program ID within station             (?)
    * Telecast ID - a telecast ID within station           (?)  

    * Date                                                 (string)
    * Telecast start time: hours, minutes, and seconds     (string)
    * Minute of program (MOP)                              (integer)  

    * SOW                                                  (float)  
      (the sum of weights of the households/persons tuned
       into the station during the MOP)
    * UE                                                   (integer)  
      (the universe estimate of the households/persons
       for the date)
  
  :param ads_df: The ads data  
  :type  ads_df: :class:`pyspark.sql.DataFrame`
  
  :param tuning_df: The tuning/viewing data  
  :type  tuning_df: :class:`pyspark.sql.DataFrame`

  :param ad_ids: The list of ad IDs that should be included in the output;  
  if empty, calculations will be done for all the distinct ad IDs  
  :type  ad_ids: `list`

  :param ads_df_properties: A dictionary specifying the parameters of the `ads_df` dataframe  
  Default is :const:`ADS_DF_DEFAULTS`. To override, create a copy of the default dictionary,
  aggisn the desired values to the keys you'd like to change, and use the copy as the argument.  
  The dictionary has the following entries:
    * 'date format'               : the format of the date column
    * 'time format'               : the format of the time column
    * 'broadcast day start time'  : the start time of a broadcast day (in the same format as ad start time)  

    * 'originator ID column'      : the name of the originator ID column
    * 'ad ID column'              : the name of the ad ID column  

    * 'date column'               : the name of the date column
    * 'ad start time column'      : the name of the ad start time column
    * 'ad duration column'        : the name of the ad duration column
  :type  ads_df_properties: `dict`

  :param tuning_df_properties: A dictionary specifying the parameters of the `tuning_df` dataframe   
  Default is :const:`TUNING_DF_DEFAULTS`. To override, create a copy of the default dictionary,
  aggisn the desired values to the keys you'd like to change, and use the copy as the argument.  
  The dictionary has the following entries:
    * 'date format'                : the format of the date column
    * 'time format'                : the format of the time column
    * 'broadcast day start time'   : the start time of a broadcast day (in the same format as telecast start time)  

    * 'originator ID column'       : the name of the originator ID column
    * 'program ID column'          : the name of the program ID column
    * 'telecast ID column'         : the name of the telecast ID column  

    * 'date column'                : the name of the date column
    * 'telecast start time column' : the name of the telecast start time column
    * 'MOP column'                 : the name of the MOP column  
    
    * 'SOW column'                 : the name of the SOW column
    * 'UE column'                  : the name of the UE column
  :type  tuning_df_properties: `dict`
  
  :return: A Spark dataframe with columns the ECM ratings by the ad ID  
  :rtype:  :class:`pyspark.sql.DataFrame`
  """
  
  # Changing the column names to standardized ones
  std_ads_df = \
    _ecm_standardized_ad_cols(ads_df,
                              ads_df_properties['originator ID column'],
                              ads_df_properties['ad ID column'],

                              ads_df_properties['date column'],
                              ads_df_properties['ad start time column'],
                              ads_df_properties['ad duration column'])

  std_ads_with_timestamps_df = \
    _ecm_ads_with_timestamps(std_ads_df,
                             ads_df_properties['date format'],
                             ads_df_properties['time format'],
                             ads_df_properties['broadcast day start time'])
  
  std_tuning_df = \
    _ecm_standardized_tuning_cols(tuning_df,
                                  tuning_df_properties['originator ID column'],
                                  tuning_df_properties['program ID column'],
                                  tuning_df_properties['telecast ID column'],

                                  tuning_df_properties['date column'],
                                  tuning_df_properties['telecast start time column'],
                                  tuning_df_properties['MOP column'],
                                  tuning_df_properties['SOW column'],
                                  tuning_df_properties['UE column'])

  std_tuning_with_timestamps_df = \
    _ecm_tuning_with_timestamps(std_tuning_df,
                                tuning_df_properties['date format'],
                                tuning_df_properties['time format'],
                                tuning_df_properties['broadcast day start time'])
  
  # Ad filter
  filter_conditions = '{col} IN ({vals})'.format(col  = AD_ID_COL,
                                                 vals = ','.join(["'"+id+"'" for id in ad_ids])) \
                      if ad_ids else 'true'
  
  std_tuning_ads_df = \
    _ecm_tuning_with_ads(std_tuning_with_timestamps_df,
                         std_ads_with_timestamps_df.where(filter_conditions))
  
  std_ad_projections_df = _ecm_projections(std_tuning_ads_df)
  std_ad_ratings_df     = _ecm_ratings(std_ad_projections_df)
  
  return {
    'projections':
      std_ad_projections_df  # Renaming the columns back to their originsl names
        .withColumnRenamed(TCAST_ORIGR_ID_COL, tuning_df_properties['originator ID column'])
        .withColumnRenamed(TCAST_PROG_ID_COL,        tuning_df_properties['program ID column'])
        .withColumnRenamed(TCAST_ID_COL,       tuning_df_properties['telecast ID column'])
        .withColumnRenamed(AD_ID_COL,          ads_df_properties['ad ID column'])
        .withColumnRenamed(AD_DATE_COL,        ads_df_properties['date column'])
        .withColumnRenamed(AD_START_TIME_COL,  ads_df_properties['ad start time column'])
        .withColumnRenamed(DAY_UE_COL,             tuning_df_properties['UE column']),
    'ratings':
      std_ad_ratings_df
        .withColumnRenamed(AD_ID_COL, ads_df_properties['ad ID column'])
  }
  