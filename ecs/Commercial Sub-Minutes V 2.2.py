# Databricks notebook source
import pyspark.sql.functions as func

from datetime import datetime

from pyspark.sql.functions import *
from pyspark.sql.types import StructField, StringType, StructType

from __future__ import division

# COMMAND ----------

######################################
#####    USER INPUTS (manual)    #####
######################################

national_tv_broadcast_start_date        = '2019-04-03'
national_tv_broadcast_end_date          = '2019-04-03'
ue_table_effective_start_date           = '2019-04-29'
originator_common_service_name          = '("Syndication","Cable","Network")'
genre                                   = '("GENERAL VARIETY","DAYTIME DRAMA","INSTRUCTION, ADVICE",                                                                                                                       "GENERAL DRAMA","SITUATION COMEDY","CHILD MULTI-WEEKLY","NEWS","SPORTS EVENT","FEATURE FILM","GENERAL DOCUMENTARY")'
playback_type_code                      = '(0,1,2)'
demos_of_interest                       = ['HH']
# station_mapping_table_path              = 's3://useast1-nlsn-w-digital-dsci-dev/data/users/luje8004/station_mapping/2018-04-11/station_mapping.parquet'
station_mapping_table_path              = 's3://useast1-nlsn-w-digital-dsci-dev/data/users/luje8004/station_mapping/2019-04-11/station_mapping_updated.parquet'
please_export                           = True
user_name                               = 'pakdmj01'
override_dir                            = 's3://useast1-nlsn-w-digital-dsci-dev/projects/commercial_sub_minutes'
playback_type_list                      = ['live','live+7']


tuning_file_sub_mins_dir                = 's3://adexposure-tam-sub-min-poc/program_coded_tsv'

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 1: Pull the Tuning Table from the MDL

# COMMAND ----------

tuning_table_non_rounded_df = spark.read.parquet(tuning_file_sub_mins_dir)
tuning_table_non_rounded_df.createOrReplaceTempView("tuning_table_non_rounded_df")
display(tuning_table_non_rounded_df)

# COMMAND ----------

s = '''
SELECT 
   a.*,b.intab_period_start_date 
FROM
   tuning_table_non_rounded_df a
JOIN
   tam_npm_mch_tv_prod.intab_period b
ON
   a.METERED_TV_INTAB_PERIOD_ID = b.intab_period_id
WHERE
   1 = 1
   AND RELEASED_FOR_PROCESSING_FLAG = 'Y'
   AND intab_period_start_date between '2019-04-03' and '2019-04-03'
   
'''
tuning_table_non_rounded_df1 = spark.sql(s)
tuning_table_non_rounded_df1.createOrReplaceTempView('tuning_table_non_rounded_df1')
tuning_table_non_rounded_df1.cache().count()

# COMMAND ----------

s = '''
WITH TEMP_TELECAST_DETAILS AS 
(SELECT     
    COMP.REPORT_ORIGINATOR_LINEUP_KEY,
    COMP.NATIONAL_TV_REPORT_PROGRAM_ID,
    COMP.REPORT_ORIGINATOR_LINEUP_ID AS TELECAST_ID,
    COMP.REPORTABLE_REPORT_ORIGINATOR_LINEUP_ID AS REPORTABLE_TELECAST_ID,
    COMP.COMPLEX_ID,
    COMP.NATIONAL_TV_BROADCAST_DATE AS TELECAST_BROADCAST_DATE, 
    COMP.PROGRAM_NAME, 
    COMP.TELECAST_NAME, 
    COMP.PROGRAM_SUMMARY_TYPE_CODE, 
    COMP.REPORT_DURATION_IN_MINUTES,
    COMP.REPORT_START_TIME_NY,
    COMP.IS_UMBRELLA_FLAG,
    CONT.ORIGINATOR_DISTRIBUTOR_ID,
    CONT.DISTRIBUTOR_TYPE_CODE,
    CONT.ORIGINATOR_NAME,
    case when DISTRIBUTOR_TYPE_CODE=9 then 'Cable'
       when DISTRIBUTOR_TYPE_CODE=12 then 'Syndication'
       when DISTRIBUTOR_TYPE_CODE=21 then 'Unwired Network'
       when DISTRIBUTOR_TYPE_CODE=8 then 'Network'
else 'Unknown' end as ORIGINATOR_COMMON_SERVICE_NAME
FROM  
    tam_npm_mch_tv_prod.report_originator_lineup COMP 
JOIN 
    tam_npm_mch_tv_prod.National_tv_content_originator CONT 
    ON CONT.ORIGINATOR_DISTRIBUTOR_ID = COMP.ORIGINATOR_DISTRIBUTOR_ID 
    AND COMP.NATIONAL_TV_BROADCAST_DATE BETWEEN CONT.EFFECTIVE_START_DATE AND CONT.EFFECTIVE_END_DATE 
WHERE    
    COMP.RELEASED_FOR_PROCESSING_FLAG = 'Y' 
    AND COMP.MULTI_BARTER_FLAG <> 'Y' 
    AND COMP.CASH_BARTER_FLAG <> 'Y' 
    AND COMP.LINEUP_RELEASE_TYPE_CODE = 1 --Currency telecasts only (excluding Fasties, Retro Coded telecasts)
    AND COMP.ORIGINATOR_TYPE_CODE IN ( 8, 9, 12 )), -- 8:Broadcast 9:Cable 12:Syndication 
    
TEMP_PROGRAM_CODED_HOUSEHOLD_TUNING AS 
(SELECT 
    TELECAST.REPORT_START_TIME_NY,
    TELECAST.REPORT_DURATION_IN_MINUTES,
    TELECAST.DISTRIBUTOR_TYPE_CODE, 
    TELECAST.ORIGINATOR_NAME, 
    TELECAST.ORIGINATOR_COMMON_SERVICE_NAME, 
    TELECAST.NATIONAL_TV_REPORT_PROGRAM_ID,
    TELECAST.TELECAST_ID,
    TELECAST.REPORTABLE_TELECAST_ID,
    TELECAST.COMPLEX_ID,
    TELECAST.TELECAST_BROADCAST_DATE, 
    TELECAST.PROGRAM_NAME, 
    TELECAST.TELECAST_NAME, 
    TELECAST.PROGRAM_SUMMARY_TYPE_CODE, 
    TELECAST.ORIGINATOR_DISTRIBUTOR_ID, 
    TELECAST.IS_UMBRELLA_FLAG,
    VIEW.CREDIT_QUARTER_HOUR_DATETIME_UTC,
    VIEW.SOURCE_INSTALLED_LOCATION_NUMBER, 
    VIEW.LOCATION_MEDIA_CONSUMER_TYPE_CODE, 
    VIEW.MARKET_ID, 
    VIEW.NATIONAL_SERVICE_TIME_ZONE_ID, 
    VIEW.SOURCE_MEDIA_DEVICE_NUMBER, 
    VIEW.METERED_TV_CREDIT_STATION_CODE, 
    VIEW.CREDIT_START_OF_VIEWING_UTC, 
    VIEW.CREDIT_END_OF_VIEWING_UTC, 
    VIEW.VIEWED_START_OF_VIEWING_UTC, 
    VIEW.VIEWED_END_OF_VIEWING_UTC, 
    VIEW.CREDIT_START_OF_VIEWING_LOCAL_TIME, 
    VIEW.CREDIT_END_OF_VIEWING_LOCAL_TIME, 
    VIEW.VIEWED_START_OF_VIEWING_LOCAL_TIME, 
    VIEW.VIEWED_END_OF_VIEWING_LOCAL_TIME, 
    VIEW.CREDIT_START_OF_VIEWING_NY_TIME, 
    VIEW.CREDIT_END_OF_VIEWING_NY_TIME, 
    VIEW.VIEWED_START_OF_VIEWING_NY_TIME, 
    VIEW.VIEWED_END_OF_VIEWING_NY_TIME, 
    VIEW.START_MINUTE_OF_PROGRAM, 
    VIEW.END_MINUTE_OF_PROGRAM, 
    VIEW.START_MINUTE_AFTER_MIDNIGHT, 
    VIEW.END_MINUTE_AFTER_MIDNIGHT,  
    VIEW.START_SECOND_OF_PROGRAM,
    VIEW.END_SECOND_OF_PROGRAM,
    VIEW.DURATION_IN_MINUTES, 
    VIEW.duration_in_seconds,
    VIEW.MINUTES_FROM_RECORD_TO_PLAYBACK, 
    VIEW.PLAYBACK_TYPE_CODE, 
    VIEW.TIME_SHIFTED_VIEWING_TYPE_CODE, 
    VIEW.IS_SIMULTANEOUS_VIEWING_FLAG, 
    VIEW.HOUSEHOLD_MEDIA_CONSUMER_ID, 
    VIEW.LOCATION_MEDIA_CONSUMER_ID, 
    VIEW.MEDIA_DEVICE_ID, 
    VIEW.SAMPLE_CHARACTERISTIC_INTAB_PERIOD_ID, 
    VIEW.REPORT_ORIGINATOR_LINEUP_KEY,
    IP.INTAB_PERIOD_START_DATE AS INTAB_PERIOD_DATE, 
    VIP.INTAB_PERIOD_START_DATE AS VIEWED_INTAB_PERIOD_DATE, 
    INTAB.SAMPLE_COLLECTION_METHOD_KEY, 
    INTAB.IS_INTAB_FLAG,
    CASE WHEN PCD.MEDIA_DEVICE_ID IS NOT NULL 
         THEN 2
         ELSE 1 
    END AS MEDIA_DEVICE_CATEGORY_CODE 
FROM 
    {tuning_table} VIEW
    --tam_npm_mch_tv_prod.program_coded_metered_tv_location_media_engagement_event VIEW 
JOIN 
    TEMP_TELECAST_DETAILS TELECAST 
    ON TELECAST.REPORT_ORIGINATOR_LINEUP_KEY = VIEW.REPORT_ORIGINATOR_LINEUP_KEY 
JOIN 
    tam_npm_mch_tv_prod.intab_period VIP 
    ON 
    VIP.INTAB_PERIOD_ID = VIEW.VIEWED_INTAB_PERIOD_ID 
JOIN 
    tam_npm_mch_tv_prod.intab_period IP
    ON 
    IP.INTAB_PERIOD_ID = VIEW.METERED_TV_INTAB_PERIOD_ID 
JOIN 
    tam_npm_mch_tv_prod.metered_tv_household_location_intab_status INTAB 
    ON 
    INTAB.INTAB_PERIOD_ID = VIEW.SAMPLE_CHARACTERISTIC_INTAB_PERIOD_ID 
    AND INTAB.HOUSEHOLD_MEDIA_CONSUMER_ID = VIEW.HOUSEHOLD_MEDIA_CONSUMER_ID 
    AND INTAB.MEDIA_CONSUMER_ID = VIEW.LOCATION_MEDIA_CONSUMER_ID 
LEFT OUTER JOIN 
    tam_npm_mch_tv_prod.METERED_TV_MEDIA_DEVICE_PC_INTAB_PERIOD PCD 
    ON PCD.MEDIA_DEVICE_ID = VIEW.MEDIA_DEVICE_ID 
    AND PCD.HOUSEHOLD_MEDIA_CONSUMER_ID = VIEW.HOUSEHOLD_MEDIA_CONSUMER_ID
    AND PCD.LOCATION_MEDIA_CONSUMER_ID = VIEW.LOCATION_MEDIA_CONSUMER_ID
    AND PCD.INTAB_PERIOD_ID = VIEW.SAMPLE_CHARACTERISTIC_INTAB_PERIOD_ID
LEFT OUTER JOIN 
    tam_npm_mch_tv_prod.METERED_TV_MEDIA_DEVICE_TUNER_INTAB_PERIOD TVD 
    ON TVD.MEDIA_DEVICE_ID = VIEW.MEDIA_DEVICE_ID 
    AND TVD.HOUSEHOLD_MEDIA_CONSUMER_ID = VIEW.HOUSEHOLD_MEDIA_CONSUMER_ID 
    AND TVD.LOCATION_MEDIA_CONSUMER_ID = VIEW.LOCATION_MEDIA_CONSUMER_ID
    AND TVD.INTAB_PERIOD_ID = VIEW.SAMPLE_CHARACTERISTIC_INTAB_PERIOD_ID
WHERE     
    VIEW.RELEASED_FOR_PROCESSING_FLAG = 'Y' 
    AND INTAB.RELEASED_FOR_PROCESSING_FLAG='Y'
    AND INTAB.SAMPLE_COLLECTION_METHOD_KEY IN ( 2, 7 )),
    
    
    
TEMP_PROGRAM_CODED_HOUSEHOLD_TUNING_WITH_WEIGHT AS 
(SELECT 
    VIEW_T.DISTRIBUTOR_TYPE_CODE, 
    VIEW_T.REPORT_ORIGINATOR_LINEUP_KEY,
    VIEW_T.ORIGINATOR_NAME, 
    VIEW_T.ORIGINATOR_COMMON_SERVICE_NAME, 
    VIEW_T.NATIONAL_TV_REPORT_PROGRAM_ID as PROGRAM_ID, 
    VIEW_T.TELECAST_ID, 
    VIEW_T.REPORTABLE_TELECAST_ID, 
    VIEW_T.COMPLEX_ID, 
    VIEW_T.TELECAST_BROADCAST_DATE, 
    VIEW_T.PROGRAM_NAME, 
    VIEW_T.TELECAST_NAME, 
    VIEW_T.IS_UMBRELLA_FLAG,
    VIEW_T.PROGRAM_SUMMARY_TYPE_CODE, 
    VIEW_T.ORIGINATOR_DISTRIBUTOR_ID, 
    VIEW_T.SOURCE_INSTALLED_LOCATION_NUMBER, 
    VIEW_T.LOCATION_MEDIA_CONSUMER_TYPE_CODE, 
    VIEW_T.MARKET_ID, 
    VIEW_T.NATIONAL_SERVICE_TIME_ZONE_ID, 
    VIEW_T.SOURCE_MEDIA_DEVICE_NUMBER, 
    VIEW_T.METERED_TV_CREDIT_STATION_CODE, 
    VIEW_T.CREDIT_START_OF_VIEWING_UTC, 
    VIEW_T.CREDIT_END_OF_VIEWING_UTC, 
    VIEW_T.VIEWED_START_OF_VIEWING_UTC, 
    VIEW_T.VIEWED_END_OF_VIEWING_UTC, 
    VIEW_T.CREDIT_START_OF_VIEWING_LOCAL_TIME, 
    VIEW_T.CREDIT_END_OF_VIEWING_LOCAL_TIME, 
    VIEW_T.VIEWED_START_OF_VIEWING_LOCAL_TIME, 
    VIEW_T.VIEWED_END_OF_VIEWING_LOCAL_TIME, 
    DATE_FORMAT(CAST(UNIX_TIMESTAMP(credit_start_of_viewing_ny_time, 'yyyy-MM-ddTH:m:s.000+0000') AS TIMESTAMP),'yyyy-MM-dd HH:mm:ss') 
    as CREDIT_START_OF_VIEWING_NY_TIME,
    DATE_FORMAT(CAST(UNIX_TIMESTAMP(credit_end_of_viewing_ny_time, 'yyyy-MM-ddTH:m:s.000+0000') AS TIMESTAMP),'yyyy-MM-dd HH:mm:ss') 
    as CREDIT_END_OF_VIEWING_NY_TIME,
    DATE_FORMAT(CAST(UNIX_TIMESTAMP(viewed_start_of_viewing_ny_time, 'yyyy-MM-ddTH:m:s.000+0000') AS TIMESTAMP),'yyyy-MM-dd HH:mm:ss') 
    as viewed_START_OF_VIEWING_NY_TIME,
    DATE_FORMAT(CAST(UNIX_TIMESTAMP(viewed_end_of_viewing_ny_time, 'yyyy-MM-ddTH:m:s.000+0000') AS TIMESTAMP),'yyyy-MM-dd HH:mm:ss') 
    as viewed_END_OF_VIEWING_NY_TIME,
    VIEW_T.START_MINUTE_OF_PROGRAM, 
    VIEW_T.END_MINUTE_OF_PROGRAM, 
    VIEW_T.START_SECOND_OF_PROGRAM,
    VIEW_T.END_SECOND_OF_PROGRAM,
    VIEW_T.START_MINUTE_AFTER_MIDNIGHT, 
    VIEW_T.END_MINUTE_AFTER_MIDNIGHT, 
    VIEW_T.DURATION_IN_MINUTES, 
    VIEW_T.DURATION_IN_SECONDS,
    VIEW_T.MINUTES_FROM_RECORD_TO_PLAYBACK, 
    VIEW_T.PLAYBACK_TYPE_CODE, 
    VIEW_T.TIME_SHIFTED_VIEWING_TYPE_CODE, 
    VIEW_T.IS_SIMULTANEOUS_VIEWING_FLAG, 
    VIEW_T.INTAB_PERIOD_DATE, 
    VIEW_T.VIEWED_INTAB_PERIOD_DATE, 
    VIEW_T.SAMPLE_COLLECTION_METHOD_KEY, 
    VIEW_T.MEDIA_DEVICE_CATEGORY_CODE, 
    VIEW_T.REPORT_DURATION_IN_MINUTES,
    VIEW_T.REPORT_START_TIME_NY,
    VIEW_T.IS_INTAB_FLAG,
    VIEW_T.CREDIT_QUARTER_HOUR_DATETIME_UTC,
    WEIGHT.LOCATION_WEIGHT as WEIGHT
FROM 
    TEMP_PROGRAM_CODED_HOUSEHOLD_TUNING VIEW_T
LEFT OUTER JOIN 
    tam_npm_mch_tv_prod.metered_tv_household_location_intab_and_weight WEIGHT
    ON WEIGHT.HOUSEHOLD_MEDIA_CONSUMER_ID = VIEW_T.HOUSEHOLD_MEDIA_CONSUMER_ID 
    AND WEIGHT.LOCATION_MEDIA_CONSUMER_ID = VIEW_T.LOCATION_MEDIA_CONSUMER_ID
    AND WEIGHT.SAMPLE_CHARACTERISTIC_INTAB_PERIOD_ID = VIEW_T.SAMPLE_CHARACTERISTIC_INTAB_PERIOD_ID
    AND WEIGHT.MEDIA_DEVICE_CATEGORY_CODE = VIEW_T.MEDIA_DEVICE_CATEGORY_CODE 
    AND WEIGHT.SAMPLE_COLLECTION_METHOD_KEY = VIEW_T.SAMPLE_COLLECTION_METHOD_KEY)


SELECT 
    * 
FROM 
    TEMP_PROGRAM_CODED_HOUSEHOLD_TUNING_WITH_WEIGHT 

WHERE 
    1=1
    AND TELECAST_BROADCAST_DATE BETWEEN '{national_tv_broadcast_start_date}' and '{national_tv_broadcast_end_date}'
    AND ORIGINATOR_COMMON_SERVICE_NAME in {originator_common_service_name}
    AND PLAYBACK_TYPE_CODE in {playback_type_code}
    AND SAMPLE_COLLECTION_METHOD_KEY = 7 -- 7:NPM Homes  2:SM Homes
    AND MEDIA_DEVICE_CATEGORY_CODE = 1 -- 1:TV Tuning 2:PC Tuning
    AND IS_INTAB_FLAG='Y'
    -- AND LOCATION_MEDIA_CONSUMER_TYPE_CODE=1  --deleting extended homes
    AND IS_SIMULTANEOUS_VIEWING_FLAG='N'    --removing simultaneouse viewing

--limit 100'''.format(tuning_table = 'tuning_table_non_rounded_df1', originator_common_service_name = originator_common_service_name,playback_type_code = playback_type_code,national_tv_broadcast_start_date = national_tv_broadcast_start_date,national_tv_broadcast_end_date = national_tv_broadcast_end_date)
tuning_table_non_rounded = spark.sql(s)
tuning_table_non_rounded.createOrReplaceTempView("tuning_table_non_rounded")
tuning_table_non_rounded.cache().count()

# COMMAND ----------

display(tuning_table_rounded_df)

# COMMAND ----------

# tuning_table_non_rounded.write.parquet(override_dir+'/'+user_name+'/'+national_tv_broadcast_start_date+'to'+national_tv_broadcast_end_date+'non_rounded_tuning'+'processed_V1', mode='overwrite')
program_coded = spark.read.parquet(override_dir+'/'+user_name+'/'+national_tv_broadcast_start_date+'to'+national_tv_broadcast_end_date+'non_rounded_tuning'+'processed_V1')
program_coded.createOrReplaceTempView("program_coded")
program_coded.cache().count()


# COMMAND ----------

###########################################################################################
### The station maping parquet file is pulled from the MDL here              #######
### this file is used to identify the networks each station code belongs to  #######
###########################################################################################

station_maping_table = sqlContext.read.format("parquet").load(station_mapping_table_path, header=True).hint('broadcast')
station_maping_table.createOrReplaceTempView("station_maping_table")

# COMMAND ----------

#############################################################################
### Here the viewing file is joined with the station mapping file and 
### genre table. Also the data is cut back to only genres present in
### genra variable, which is one of the input parameters
#############################################################################

s = '''--sql
  SELECT
    VIEW.*,
    date_format(VIEW.telecast_broadcast_date, 'EEEE')  AS telecast_broadcast_date_days_of_week,
    CASE
      WHEN call IS NULL THEN 'not exists'
      ELSE                    call
    END                                                AS viewing_source_short_name,
    GENRE.program_summary_type_desc

  FROM
    {table} AS VIEW

  LEFT JOIN
    station_maping_table AS STATION
  ON
    VIEW.metered_tv_credit_station_code = STATION.station_code

  JOIN
    TAM_NPM_MCH_TV_PROD.program_summary_type AS GENRE
  ON
    VIEW.program_summary_type_code = GENRE.program_summary_type_code

  WHERE 1=1
    -- AND genre.program_summary_type_desc in {genre}
    
  --endsql''' \
    .format(table = 'program_coded', genre = genre)

program_coded_stn = spark.sql(s)
program_coded_stn.createOrReplaceTempView('program_coded_stn')

# COMMAND ----------

###########################################################################
### The function append_demo_indicator is defined to add demo indicator 
### columns to the input data set. The assumption of the function is that
### the columns age and  gender_code exist within the input dataset.       
###########################################################################

def append_demo_indicator (data_frame_without_demo_ind):
  

  s = '''--sql
    SELECT
      TABLE.*,
      
      CASE
        WHEN TRUE THEN 'Y' ELSE 'N'
      END AS HH
    FROM
      {data_frame_without_demo_ind} AS TABLE

  --endsql''' \
    .format(data_frame_without_demo_ind = data_frame_without_demo_ind)

  data_frame_with_demo_ind = spark.sql(s)

  return data_frame_with_demo_ind

# COMMAND ----------

###########################################################################
### The function append_demo_indicator is called for 
### viewing_table_rounded_stn as an input table. The output table will have
### a bunch of demo indicator columns.
###########################################################################

program_coded_stn_demo = append_demo_indicator('program_coded_stn')
program_coded_stn_demo.createOrReplaceTempView("program_coded_stn_demo")
program_coded_stn_demo.cache().count()

# COMMAND ----------

display(program_coded_stn_demo)

# COMMAND ----------

#############################################################################
### Here the playback type is redefined.
### `time_shifted_viewing_type_code` of 0 means "live",
### 1 means "Same Day",
### 2 means "1st day", 3 means "2nd day", etc.
### (See `TAM_NPM_MCH_TV_PROD.time_shifted_viewing_type`)
#############################################################################

live_program_coded_stn_demo = \
  program_coded_stn_demo.filter('time_shifted_viewing_type_code IN (0)')
live_program_coded_stn_demo = \
  live_program_coded_stn_demo.withColumn('playback_type', lit('live'))

seven_day_pb_program_coded_stn_demo = \
  program_coded_stn_demo #.filter('time_shifted_viewing_type_code IN (0, 2, 3, 4, 5, 6, 7, 8)')
seven_day_pb_program_coded_stn_demo = \
  seven_day_pb_program_coded_stn_demo.withColumn('playback_type', lit('live+7'))

three_day_pb_program_coded_stn_demo = \
  program_coded_stn_demo.filter('time_shifted_viewing_type_code IN (0, 2, 3, 4)')
three_day_pb_program_coded_stn_demo = \
  three_day_pb_program_coded_stn_demo.withColumn('playback_type', lit('live+3'))

pb_program_coded_stn_demo = \
  live_program_coded_stn_demo.union(seven_day_pb_program_coded_stn_demo) \
                                     .union(three_day_pb_program_coded_stn_demo)

pb_program_coded_stn_demo.createOrReplaceTempView("pb_program_coded_stn_demo")
pb_program_coded_stn_demo.cache().count()

# COMMAND ----------

s = ''' select * from pb_program_coded_stn_demo
where PROGRAM_ID = 847475
and telecast_id = 99966
and SOURCE_INSTALLED_LOCATION_NUMBER = 841680
and playback_type = 'live'
order by 
CREDIT_START_OF_VIEWING_NY_TIME'''
pb_program_coded_stn_demo = spark.sql(s)
pb_program_coded_stn_demo.createOrReplaceTempView('pb_program_coded_stn_demo')
pb_program_coded_stn_demo.cache().count()

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 2 : Import UEs and Sum of Weights for a Day

# COMMAND ----------

hh_wc_ue = spark.sql("""
SELECT  
    effective_start_date,WEIGHTING_CONTROL_ID,UNIVERSE_ESTIMATE*1.0 as target_value,HOUSEHOLD_OR_PERSON_CODE
FROM
    tam_npm_mch_tv_prod.NATIONAL_TV_WEIGHTING_CONTROL_UNIVERSE_ESTIMATE
WHERE
    1 = 1
    AND EFFECTIVE_START_DATE = '{ue_table_effective_start_date}'
    AND HOUSEHOLD_OR_PERSON_CODE = 'H'
    AND UNIVERSE_ESTIMATE_TYPE_CODE = 1
    AND RELEASED_FOR_PROCESSING_FLAG = 'Y'
    AND WEIGHTING_CONTROL_ID = 9999
ORDER BY
    WEIGHTING_CONTROL_ID
""".format(ue_table_effective_start_date = ue_table_effective_start_date) )
hh_wc_ue.createOrReplaceTempView("hh_wc_ue")
hh_ue = int(hh_wc_ue.select('target_value').collect()[0][0])
print('household ue is : ',hh_ue)
# display(hh_wc_ue)

# COMMAND ----------

s = '''
SELECT 
   intab_period_start_date,sum(location_weight) as sow_per_day
FROM
   (SELECT 
      intab_period_start_date,	household_media_consumer_id,location_weight
    FROM
      tam_npm_mch_tv_prod.INTAB_PERIOD as d,
      tam_npm_mch_tv_prod.METERED_TV_HOUSEHOLD_LOCATION_INTAB_AND_WEIGHT as i 
 
    
    WHERE 1=1
      AND i.SAMPLE_COLLECTION_METHOD_KEY=7    --NPM SAMPLE
      AND I.MEDIA_DEVICE_CATEGORY_CODE = 1    -- 1:TV Tuning 2:PC Tuning
      AND (d.INTAB_PERIOD_START_DATE between'{start_date}' and '{end_date}') 
      AND i.IS_INTAB_INDICATOR=1      --keeping intab homes
      AND i.LOCATION_MEDIA_CONSUMER_TYPE_CODE = '1'     --removing extended homes
      AND  d.INTAB_PERIOD_ID = i.INTAB_PERIOD_ID) a
GROUP BY
   intab_period_start_date
ORDER BY
   intab_period_start_date 

   '''
intab_df = spark.sql(s.format(start_date = national_tv_broadcast_start_date, end_date = national_tv_broadcast_end_date))
intab_df = intab_df.withColumn('ue',lit(hh_ue)).withColumn('demo',lit('HH'))
intab_df = intab_df.withColumn('adj_factor', func.round(col('ue')/col('sow_per_day'),6))
intab_df.cache()
intab_df.createOrReplaceTempView("intab_df")

# COMMAND ----------

display(intab_df)

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 3: Pull Ad Fact Table and Aggregate it

# COMMAND ----------

ads_fact_dim = spark.read.parquet(override_dir+'/'+user_name+'/'+national_tv_broadcast_start_date+'to'+national_tv_broadcast_end_date+'ads_fact_v0')
ads_fact_dim = ads_fact_dim.filter('PROGRAM_ID = 847475').filter('telecast_id = 99966')
ads_fact_dim.createOrReplaceTempView("ads_fact_dim")
ads_fact_dim.cache().count()

# COMMAND ----------

display(ads_fact_dim)

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 4: Join non-rounded Tuning and Ads Fact

# COMMAND ----------

s = '''
SELECT 
  broadcast_date,program_id,
  telecast_id,
  advertisement_key,
  advertisement_code,
  commercial_start_time,
  pod_value,
  brand_desc,
  creative_desc,
  brand_code,
  pcc_industry_group_desc,
  min(minute_of_program) as start_mop,
  max(minute_of_program) as end_mop,
  sum(advertisement_duration) as advertisement_duration
FROM
  ads_fact_dim
GROUP BY
  broadcast_date,program_id,
  telecast_id,
  advertisement_key,
  advertisement_code,
  commercial_start_time,
  pod_value,
  brand_desc,
  creative_desc,
  brand_code,
  pcc_industry_group_desc
ORDER BY
  broadcast_date,program_id,telecast_id,commercial_start_time'''
ads_fact_summary = spark.sql(s)
ads_fact_summary.createOrReplaceTempView("ads_fact_summary")
display(ads_fact_summary)

# COMMAND ----------

ads_fact_summary.count()

# COMMAND ----------

s = '''
SELECT
  *,
  (START_MOP-1)*60+substring(commercial_start_time,7,2)+1 as computed_start_second_of_ad,
  (START_MOP-1)*60+substring(commercial_start_time,7,2)+advertisement_duration	 as computed_end_second_of_ad
FROM
  ads_fact_summary
ORDER BY
  broadcast_date,program_id,telecast_id,commercial_start_time'''
ads_fact_summary2 = spark.sql(s)
ads_fact_summary2.cache()
ads_fact_summary2.createOrReplaceTempView("ads_fact_summary2")
display(ads_fact_summary2)

# COMMAND ----------

query = '''
SELECT
  *,adj_computed_end_second_of_ad - adj_computed_start_second_of_ad +1 as watched_advertisement_duration
FROM
(
SELECT
  VIEW.HH,
  VIEW.telecast_broadcast_date, 
  VIEW.source_installed_location_number,
  VIEW.source_media_device_number,
  VIEW.program_id,
  VIEW.telecast_id,
  VIEW.playback_type,
  VIEW.originator_common_service_name,
  VIEW.program_name,
  VIEW.telecast_name,
  VIEW.telecast_broadcast_date_days_of_week,
  VIEW.report_start_time_ny,
  VIEW.viewing_source_short_name,
  VIEW.program_summary_type_desc,
  VIEW.REPORT_ORIGINATOR_LINEUP_KEY,
  VIEW.COMPLEX_ID,
  VIEW.credit_start_of_VIEWing_ny_time,
  VIEW.credit_end_of_VIEWing_ny_time,
  VIEW.viewed_start_of_viewing_ny_time,
  VIEW.viewed_end_of_viewing_ny_time,
  VIEW.start_minute_of_program,
  VIEW.end_minute_of_program,
  VIEW.start_second_of_program,
  VIEW.end_second_of_program,
  VIEW.duration_in_seconds,
  VIEW.weight,
    
  ADS.advertisement_key,
  ADS.advertisement_code,
  ADS.commercial_start_time,
  ADS.advertisement_duration,
  ADS.pod_value,
  ADS.creative_desc,
  ADS.brand_desc,
  ADS.brand_code,
  ADS.pcc_industry_group_desc,
  ADS.computed_start_second_of_ad,
  ADS.computed_end_second_of_ad,
   
  CASE 
      WHEN computed_start_second_of_ad >= start_second_of_program and computed_end_second_of_ad <= end_second_of_program THEN computed_start_second_of_ad
      WHEN computed_start_second_of_ad < start_second_of_program and computed_end_second_of_ad >= start_second_of_program THEN start_second_of_program
      WHEN computed_start_second_of_ad <= end_second_of_program and computed_end_second_of_ad > end_second_of_program THEN computed_start_second_of_ad
  END AS adj_computed_start_second_of_ad,
  CASE 
      WHEN computed_start_second_of_ad >= start_second_of_program and computed_end_second_of_ad<=end_second_of_program THEN computed_end_second_of_ad
      WHEN computed_start_second_of_ad < start_second_of_program and computed_end_second_of_ad >= start_second_of_program THEN computed_end_second_of_ad
      WHEN computed_start_second_of_ad <= end_second_of_program and computed_end_second_of_ad > end_second_of_program THEN end_second_of_program
  END AS adj_computed_end_second_of_ad
  
   
FROM
   {table} VIEW
   
JOIN
   ads_fact_summary2 ADS
ON
   VIEW.TELECAST_BROADCAST_DATE = ADS.broadcast_date
   AND VIEW.program_id = ADS.program_id
   AND VIEW.telecast_id = ADS.telecast_id
   
WHERE
   1=1
   AND ((computed_start_second_of_ad >= start_second_of_program and computed_end_second_of_ad <= end_second_of_program) 
   OR (computed_start_second_of_ad < start_second_of_program and computed_end_second_of_ad >= start_second_of_program)
   OR (computed_start_second_of_ad <= end_second_of_program and computed_end_second_of_ad > end_second_of_program))
   
ORDER BY
   computed_start_second_of_ad)
  
   '''

pb_program_coded_stn_demo_ads = spark.sql(query.format(table = 'pb_program_coded_stn_demo'))
pb_program_coded_stn_demo_ads.cache().count()
pb_program_coded_stn_demo_ads.createOrReplaceTempView("pb_program_coded_stn_demo_ads")

display(pb_program_coded_stn_demo_ads)

# COMMAND ----------

display(ads_fact_summary2)

# COMMAND ----------

# MAGIC %sql select 
# MAGIC --*
# MAGIC creative_desc,minute_of_program,first_minute_indicator,commercial_start_time,advertisement_duration 
# MAGIC from ads_fact_dim
# MAGIC where True
# MAGIC --and PROGRAM_ID = 890562
# MAGIC --and telecast_id = 99994
# MAGIC order by 
# MAGIC minute_of_program ,commercial_start_time, first_minute_indicator asc

# COMMAND ----------

# MAGIC %sql 
# MAGIC SELECT
# MAGIC --   program_coded_media_engagement_event_key,
# MAGIC    b.TELECAST_BROADCAST_DATE,
# MAGIC    b.SOURCE_INSTALLED_LOCATION_NUMBER,
# MAGIC --   b.LOCATION_MEDIA_CONSUMER_ID,
# MAGIC    b.SOURCE_MEDIA_DEVICE_NUMBER,
# MAGIC    b.COMPLEX_ID,
# MAGIC    b.REPORT_ORIGINATOR_LINEUP_KEY,
# MAGIC    credit_start_of_viewing_ny_time,
# MAGIC    credit_end_of_viewing_ny_time,
# MAGIC    b.VIEWED_START_OF_VIEWING_NY_TIME,
# MAGIC    b.VIEWED_END_OF_VIEWING_NY_TIME,
# MAGIC    duration_in_seconds,
# MAGIC    --is_simultaneous_viewing_flag,
# MAGIC    time_shifted_viewing_type_code,
# MAGIC    weight
# MAGIC   
# MAGIC FROM
# MAGIC    pb_program_coded_stn_demo b
# MAGIC WHERE
# MAGIC    1 = 1
# MAGIC    --and SOURCE_INSTALLED_LOCATION_NUMBER = 123535
# MAGIC    --and SOURCE_MEDIA_DEVICE_NUMBER = 4
# MAGIC    --and REPORT_ORIGINATOR_LINEUP_KEY = 9219300146
# MAGIC    --and minute(VIEWED_END_OF_VIEWING_NY_TIME) - minute(VIEWED_START_OF_VIEWING_NY_TIME) in (0)
# MAGIC --ORDER BY 
# MAGIC    --VIEWED_START_OF_VIEWING_NY_TIME

# COMMAND ----------

# MAGIC %sql 
# MAGIC SELECT
# MAGIC --   program_coded_media_engagement_event_key,
# MAGIC    
# MAGIC    b.TELECAST_BROADCAST_DATE,
# MAGIC    b.SOURCE_INSTALLED_LOCATION_NUMBER,
# MAGIC --   b.LOCATION_MEDIA_CONSUMER_ID,
# MAGIC    b.SOURCE_MEDIA_DEVICE_NUMBER,
# MAGIC    b.COMPLEX_ID,
# MAGIC    b.REPORT_ORIGINATOR_LINEUP_KEY,
# MAGIC    credit_start_of_viewing_ny_time,
# MAGIC    credit_end_of_viewing_ny_time,
# MAGIC    b.VIEWED_START_OF_VIEWING_NY_TIME,
# MAGIC    b.VIEWED_END_OF_VIEWING_NY_TIME,
# MAGIC    duration_in_seconds,
# MAGIC    advertisement_duration,
# MAGIC    adj_computed_start_second_of_ad,
# MAGIC    adj_computed_end_second_of_ad,
# MAGIC    watched_advertisement_duration,
# MAGIC    --is_simultaneous_viewing_flag,
# MAGIC    playback_type
# MAGIC   
# MAGIC FROM
# MAGIC    pb_program_coded_stn_demo_ads b
# MAGIC WHERE
# MAGIC    1 = 1
# MAGIC    --and SOURCE_INSTALLED_LOCATION_NUMBER = 123535
# MAGIC    --and SOURCE_MEDIA_DEVICE_NUMBER = 4
# MAGIC    --and REPORT_ORIGINATOR_LINEUP_KEY = 9219300146
# MAGIC    --and minute(VIEWED_END_OF_VIEWING_NY_TIME) - minute(VIEWED_START_OF_VIEWING_NY_TIME) in (0)
# MAGIC ORDER BY 
# MAGIC    VIEWED_START_OF_VIEWING_NY_TIME

# COMMAND ----------

# MAGIC %sql select sum(watched_Advertisement_duration * weight * 1.000002)/766 as acs_proj from pb_program_coded_stn_demo_ads

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 5: Compute ACS

# COMMAND ----------

commercial_minutes_adj = \
  spark.sql("""--sql
    WITH
      commercial_minutes AS (
        SELECT 
            broadcast_date,
            program_id,
            telecast_id,
            minute_of_program,
            sum(advertisement_duration) AS advertisement_duration

        FROM
          ads_fact_dim

        GROUP BY
          broadcast_date, program_id, telecast_id, minute_of_program
      )


    SELECT
      *,
      CASE 
        WHEN advertisement_duration > 60 THEN 60
        ELSE                                  advertisement_duration
      END                                     AS advertisement_duration_adj

    FROM
      commercial_minutes

    --endsql""") \
    .cache()

commercial_minutes_adj.createOrReplaceTempView('commercial_minutes_adj')

# COMMAND ----------

acm_commercial_minutes_adj_ag = \
  spark.sql("""--sql
    SELECT
      broadcast_date,
      program_id,
      telecast_id,
      sum(advertisement_duration_adj) AS advertisement_duration_per_telecast

    FROM
      commercial_minutes_adj

    GROUP BY
      broadcast_date, program_id, telecast_id

    --endsql""") \
    .cache()

acm_commercial_minutes_adj_ag.createOrReplaceTempView('acm_commercial_minutes_adj_ag')

# COMMAND ----------

##############################################################
### This python function computes ACM rating and projection 
### for a specified viewing file, ads_fact table, and demo 
### of interest
##############################################################

def ACM_computation_engine(demo,viewing_df):
  
  '''
  This python function returns ACM projection and rating
  for a specified viewing file and ads_fact table.
  The function assumes that following columns exist in the input datasets with the specified format. 
  
  viewing table columns:
  [
    "TELECAST_BROADCAST_DATE",              StringType()
    "PROGRAM_ID",                           StringType()
    "TELECAST_ID",                          StringType()
    "playback_type",                        StringType()
    "DEMO",                                 StringType()
    "ORIGINATOR_COMMON_SERVICE_NAME",       StringType()
    "PROGRAM_NAME",                         StringType()
    "TELECAST_NAME",                        StringType()
    "TELECAST_BROADCAST_DATE_DAYS_OF_WEEK", StringType()
    "REPORT_START_TIME_NY",                 StringType()
    "viewing_source_short_name",            StringType()
    "program_summary_type_desc",            StringType()
    "start_minute_of_program",              IntegerType()
    "end_minute_of_program"],               IntegerType()
  ]
  
  
  ads_fact table columns: 
  [
    "broadcast_date",         StringType()
    "PROGRAM_ID",             StringType()
    "TELECAST_ID",            StringType()
    "minute_of_program",      IntegerType()
    "advertisement_duration", DecimalType()
  ]
  '''
  
  s = '''--sql
    WITH
      commercial_tuning_adj_ag AS (
        SELECT
          VIEW.telecast_broadcast_date,
          VIEW.program_id,
          VIEW.telecast_id,
          VIEW.playback_type,
          VIEW.originator_common_service_name,
          VIEW.program_name,
          VIEW.telecast_name,
          VIEW.telecast_broadcast_date_days_of_week,
          VIEW.report_start_time_ny,
          VIEW.viewing_source_short_name,
          VIEW.program_summary_type_desc,
          round(sum(VIEW.watched_advertisement_duration  * VIEW.weight * INTAB.adj_factor),2) AS sow_per_telecast

        FROM
          pb_program_coded_stn_demo_ads AS VIEW
        JOIN
          intab_df AS INTAB
        ON
          VIEW.telecast_broadcast_date = INTAB.intab_period_start_date
          

        

        GROUP BY
          VIEW.telecast_broadcast_date, VIEW.program_id, VIEW.telecast_id, VIEW.playback_type,
          VIEW.originator_common_service_name, VIEW.program_name, VIEW.telecast_name, VIEW.telecast_broadcast_date_days_of_week,
          VIEW.report_start_time_ny, VIEW.viewing_source_short_name, VIEW.program_summary_type_desc
      )


    SELECT
      VIEW_AG.telecast_broadcast_date,
      VIEW_AG.program_id,
      VIEW_AG.telecast_id,
      VIEW_AG.playback_type,
      '{demo}'                            AS demo,
      VIEW_AG.originator_common_service_name,
      VIEW_AG.program_name,
      VIEW_AG.telecast_name,
      VIEW_AG.telecast_broadcast_date_days_of_week,
      VIEW_AG.report_start_time_ny,
      VIEW_AG.viewing_source_short_name,
      VIEW_AG.program_summary_type_desc,
      round(VIEW_AG.sow_per_telecast / COMMERCIAL_AG.advertisement_duration_per_telecast)
                                          AS acm_projection,
      100 * VIEW_AG.sow_per_telecast / (COMMERCIAL_AG.advertisement_duration_per_telecast*INTAB.ue)
                                          AS acm_rating

    FROM
      commercial_tuning_adj_ag AS VIEW_AG

    JOIN
      intab_df AS INTAB
    ON
      VIEW_AG.telecast_broadcast_date = INTAB.intab_period_start_date

    JOIN
      acm_commercial_minutes_adj_ag AS COMMERCIAL_AG
    ON    TRUE
      AND VIEW_AG.telecast_broadcast_date  = COMMERCIAL_AG.broadcast_date
      AND VIEW_AG.program_id               = COMMERCIAL_AG.program_id
      AND VIEW_AG.telecast_id              = COMMERCIAL_AG.telecast_id

    WHERE
      INTAB.demo = '{demo}'

    '''

  query = s.format(demo = demo,
                   viewing_df = viewing_df)
  acm_table_part = spark.sql(query)

  return acm_table_part

# COMMAND ----------

####################################################################
### The function ACM_computation_engine is called for 
### the specific demo of interest. The output table will have
### ACM_projection (000) and ACM_rating columns. The ACM_computation_engine
### library assumes that columns with a very specific format exist in both 
### viewing and ads_fact tables
#####################################################################

field = [
  StructField("telecast_broadcast_date",              StringType(), True),
  StructField("program_id",                           StringType(), True),
  StructField("telecast_id",                          StringType(), True),
  StructField("playback_type",                        StringType(), True),
  StructField("demo",                                 StringType(), True),
  StructField("originator_common_service_name",       StringType(), True),
  StructField("program_name",                         StringType(), True),
  StructField("telecast_name",                        StringType(), True),
  StructField("telecast_broadcast_date_days_of_week", StringType(), True),
  StructField("report_start_time_ny",                 StringType(), True),
  StructField("viewing_source_short_name",            StringType(), True),
  StructField("program_summary_type_desc",            StringType(), True),
  StructField("acm_projection",                       StringType(), True),
  StructField("acm_rating",                           StringType(), True)
]

acm_schema = StructType(field)
acm_table = sqlContext.createDataFrame(sc.emptyRDD(), acm_schema) 

for demo in demos_of_interest:
  acm_table_part = ACM_computation_engine(demo, 'pb_program_coded_stn_demo')
  acm_table = acm_table.union(acm_table_part)

acm_table.createOrReplaceTempView("acm_table")

# COMMAND ----------

display(acm_table)

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 6: Compute ECS

# COMMAND ----------

ecm_commercial_minutes_adj_ag = \
  spark.sql("""--sql
    SELECT
      broadcast_date,
      program_id,
      telecast_id,
      advertisement_key,
      advertisement_code,
      commercial_start_time,
      pod_value,
      brand_desc,
      creative_desc,
      brand_code,
      pcc_industry_group_desc,
      sum(advertisement_duration) AS advertisement_duration_per_telecast

    FROM
      ads_fact_dim
      
    GROUP BY
      broadcast_date, program_id, telecast_id, advertisement_key, advertisement_code,
      commercial_start_time, pod_value, brand_desc, creative_desc,
      brand_code, pcc_industry_group_desc

    --endsql""") \
    .cache()

ecm_commercial_minutes_adj_ag.createOrReplaceTempView('ecm_commercial_minutes_adj_ag')

# COMMAND ----------

display(pb_program_coded_stn_demo_ads)

# COMMAND ----------

##############################################################
### This python function computes ECM rating and projection 
### for a specified viewing file, ads_fact table, and demo 
### of interest
##############################################################

def ECM_computation_engine(demo,viewing_df,ads_fact_dim_df):
  
  s = '''--sql
    WITH
      

      commercial_tuning_adj_ag AS (
        SELECT
          telecast_broadcast_date,
          program_id,
          telecast_id,
          playback_type,
          viewing_source_short_name,
          commercial_start_time,
          pod_value,
          advertisement_key,
          advertisement_code,
          brand_desc,
          creative_desc,
          brand_code,
          pcc_industry_group_desc,
          sum(weight * watched_advertisement_duration * INTAB.adj_factor) AS sow_per_ads_per_telecast

        FROM
          {viewing_df} VIEW
        JOIN
          intab_df INTAB
        ON
          VIEW.telecast_broadcast_date = INTAB.intab_period_start_date

        GROUP BY
          telecast_broadcast_date,
          program_id,
          telecast_id,
          playback_type,
          viewing_source_short_name,
          commercial_start_time,
          pod_value,
          advertisement_key,
          advertisement_code,
          brand_desc,
          creative_desc,
          brand_code,
          pcc_industry_group_desc
      )

    SELECT
      VIEW_AG.telecast_broadcast_date,
      VIEW_AG.program_id,
      VIEW_AG.telecast_id,
      VIEW_AG.playback_type,
      '{demo}'                                            AS demo,
      VIEW_AG.viewing_source_short_name,
      VIEW_AG.commercial_start_time,
      VIEW_AG.pod_value,
      VIEW_AG.advertisement_key,
      VIEW_AG.advertisement_code,
      VIEW_AG.brand_desc,
      VIEW_AG.creative_desc,
      VIEW_AG.brand_code,
      VIEW_AG.pcc_industry_group_desc,
      VIEW_AG.sow_per_ads_per_telecast,
      COMMERCIAL_AG.advertisement_duration_per_telecast,
      round(VIEW_AG.sow_per_ads_per_telecast / COMMERCIAL_AG.advertisement_duration_per_telecast)
                                                          AS ecm_projection,
      100 * VIEW_AG.sow_per_ads_per_telecast / (COMMERCIAL_AG.advertisement_duration_per_telecast*INTAB.ue)
                                                          AS ecm_rating

    FROM
      commercial_tuning_adj_ag AS VIEW_AG

    JOIN
      intab_df AS INTAB
    ON
      VIEW_AG.telecast_broadcast_date = INTAB.intab_period_start_date

    JOIN
      ecm_commercial_minutes_adj_ag AS COMMERCIAL_AG
    ON    TRUE
      AND VIEW_AG.telecast_broadcast_date  = COMMERCIAL_AG.broadcast_date
      AND VIEW_AG.program_id               = COMMERCIAL_AG.program_id
      AND VIEW_AG.telecast_id              = COMMERCIAL_AG.telecast_id
      AND VIEW_AG.advertisement_key        = COMMERCIAL_AG.advertisement_key
      AND VIEW_AG.advertisement_code       = COMMERCIAL_AG.advertisement_code
      AND VIEW_AG.commercial_start_time    = COMMERCIAL_AG.commercial_start_time

    WHERE
      intab.demo = '{demo}'

  --endsql'''

  query = s.format(demo = demo,
                   viewing_df = viewing_df,
                   ads_fact_dim_df = ads_fact_dim_df)
  ecm_table_part = spark.sql(query)

  return ecm_table_part

# COMMAND ----------

####################################################################
### The function ECM_computation_engine is called for 
### the specific demo of interest. The output table will have
### ACM_projection (000) and ACM_rating columns. The ACM_computation_engine
### library assumes that columns with a very specific format exist in both 
### viewing and ads_fact tables
#####################################################################

field = [
  StructField("telecast_broadcast_date",             StringType(), True),
  StructField("program_id",                          StringType(), True),
  StructField("telecast_id",                         StringType(), True),
  StructField("playback_type",                       StringType(), True),
  StructField("demo",                                StringType(), True),
  StructField("viewing_source_short_name",           StringType(), True),
  StructField("commercial_start_time",               StringType(), True),
  StructField("pod_value",                           StringType(), True),
  StructField("advertisement_key",                   StringType(), True),
  StructField("advertisement_code",                  StringType(), True),
  StructField("brand_desc",                          StringType(), True),
  StructField("creative_desc",                       StringType(), True),
  StructField("brand_code",                          StringType(), True),
  StructField("pcc_industry_group_desc",             StringType(), True),
  StructField("sow_per_ads_per_telecast",            StringType(), True),
  StructField("advertisement_duration_per_telecast", StringType(), True),
  StructField("ecm_projection",                      StringType(), True),
  StructField("ecm_rating",                          StringType(), True)
]

ecm_schema = StructType(field)
ecm_table = sqlContext.createDataFrame(sc.emptyRDD(), ecm_schema) 

for demo in demos_of_interest:
  ecm_table_part = ECM_computation_engine(demo, 'pb_program_coded_stn_demo_ads', 'ads_fact_dim')
  ecm_table = ecm_table.union(ecm_table_part)

ecm_table.createOrReplaceTempView('ecm_table')
# display(ecm_table)

# COMMAND ----------

display(ecm_table.orderBy('commercial_start_time'))

# COMMAND ----------


