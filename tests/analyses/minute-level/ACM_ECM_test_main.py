# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC # Commercial Metrics Analysis Test Suite
# MAGIC 
# MAGIC 
# MAGIC **PURPOSE**
# MAGIC > This notebook runs a suite of tests
# MAGIC   that compare the output of the Commercial Metric Analysis notebook
# MAGIC   to a previously validated dataset.
# MAGIC 
# MAGIC 
# MAGIC **USAGE**
# MAGIC > * If the notebook doesn't have input widgets, run the notebook once to get those.  
# MAGIC     This will raise an exception and stop right after creating the widgets.
# MAGIC > * Fill in the widgets with the appropriate values (see below), and run the entire notebook.
# MAGIC > * Once the notebook finishes, the test results will be printed out at the end.
# MAGIC 
# MAGIC 
# MAGIC **USER INPUTS (READ FROM THE WIDGETS)**  
# MAGIC > 1. **Analysis notebook location**
# MAGIC      - The location of the Commercial Metrics Analysis notebook
# MAGIC      
# MAGIC > 1. **Cost center**
# MAGIC      - The cost center of the user running this notebook  
# MAGIC        It can be found on your [Nielsen Directory](https://script.google.com/a/macros/nielsen.com/s/AKfycby1F5fy7J0Q_v9zwEVSimKZvyqlctbTpF4BbfscdCtbRfhiXM75JvxKcYbrL47gvYEf/exec) page.
# MAGIC        
# MAGIC > 1. **Data filter**
# MAGIC      - A SQL Boolean expression to use for filtering the tested and validated data prior to comparing
# MAGIC      
# MAGIC > 1. **Databricks token file**
# MAGIC      - The path to a file containing a Databricks access token  
# MAGIC        You can create a new access token in [User Settings](https://nielsen-prod.cloud.databricks.com/#setting/account),
# MAGIC        under the **Access Tokens** tab.  
# MAGIC        The token is shown to you only once, so save it to a text file
# MAGIC        with a single line containing the token.  
# MAGIC        Put the file to a location accessible by Databricks, and use this input to specify this location.
# MAGIC        
# MAGIC > 1. **Output folder**
# MAGIC      - The locations under which all of the notebook output will be saved
# MAGIC      
# MAGIC > 1. **Truth file**
# MAGIC      - The path to a file containing the validated dataset to compare the tested output to
# MAGIC      
# MAGIC > 1. **Your email**
# MAGIC      - The Nielsen email of the user running this notebook
# MAGIC 
# MAGIC 
# MAGIC **EXAMPLE INPUTS**
# MAGIC >  | Widget                     | Value                                                                                                                                                     |
# MAGIC    | ---                        | ---                                                                                                                                                       |
# MAGIC    | Analysis notebook location | `/Users/arseny.egorov@nielsen.com/Projects/Sub-Minute Commercial/analyses/minute-level/commercial_sub_main`                                               |
# MAGIC    | Cost center                | `1234567`<br/>(not a valid value)                                                                                                                             |
# MAGIC    | Data filter                | `playback_type = 'live'`<br/>(use `TRUE` if you don't need filtering)                                                                                         |
# MAGIC    | Databricks token file      | `s3://useast1-nlsn-w-digital-dsci-dev/users/egar8001/Projects/Sub-minute ratings/dummy_db_token.txt`<br/>(this one doesn't contain a valid token)             |
# MAGIC    | Output folder              | `s3://useast1-nlsn-w-digital-dsci-dev/users/egar8001/Projects/Sub-minute ratings/`                                                            |
# MAGIC    | Truth file                 | `s3://useast1-nlsn-w-digital-dsci-dev/users/egar8001/Projects/Sub-minute ratings/checkpoints/2019-07-31_15-14-15_acm_ecm_table_2019-04-01_to_2019-04-28/` |
# MAGIC    | Your email                 | `arseny.egorov@nielsen.com`                                                                                                                               |
# MAGIC 
# MAGIC 
# MAGIC **CODE DEVELOPERS**
# MAGIC > * Arseny EGOROV

# COMMAND ----------

# MAGIC %md
# MAGIC ### Input (Automated)
# MAGIC 
# MAGIC This section checks if the input widgets have already been created, and creates them if need be.
# MAGIC 
# MAGIC If the input widgets weren't there ther first time,
# MAGIC you'll need to rerun the notebook
# MAGIC after the widgets have been created and filled in with the appropriated values.

# COMMAND ----------

# Uncomment and run this cell once when you've changed the widget properties below,
# then comment it out again.
# Before running this, save the current widget values to a separate location
# if you intend to reuse them, since the values will be reset when the widgets are recreated.

# dbutils.widgets.removeAll()

# COMMAND ----------

# checking if the input widgets exist
try:
  dbutils.widgets.get('user_email')
  
except:
  # If the widgets don't yet exist, create them
  
  # DB Buffet inputs
  dbutils.widgets.text('user_email',        'your.name@nielsen.com',                                       'Your email')
  dbutils.widgets.text('token_file',        'path_to_file_with_db_token',                                  'Databricks token file')
  dbutils.widgets.text('cost_center',       'cost center # here',                                          'Cost center')
  
  # Analysis inputs
  dbutils.widgets.text('notebook_location', '/Users/your.email@nielsen.com/path_to_notebook',              'Analysis notebook location')
  dbutils.widgets.dropdown('full_test',     'Yes',
                                            ['Yes', 'No'],                                                 'Check output data')
  dbutils.widgets.text('output_folder',     'path_to_folder',                                              'Output folder')
  dbutils.widgets.text('truth_dataset',     's3://useast1-nlsn-w-digital-dsci-dev/'
                                            'users/egar8001/'
                                            'Projects/Sub-minute ratings/checkpoints/'
                                            '2019-07-31_15-14-15_acm_ecm_table_2019-04-01_to_2019-04-28/', 'Truth file')
  dbutils.widgets.text('data_filter',       'TRUE',                                                        'Data filter')
  
  raise Exception('Please fill in the input widgets with actual values and run the notebook again.')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Imports & Constants

# COMMAND ----------

# required import
from db_buffet import DatabricksRestClient
from time import sleep
from pyspark.sql.functions import max as MAX, \
                                  abs as ABS

FLOATING_POINT_TOLERANCE = 6

PROJECT_NAME = 'NATPM-80_Sub-Minute_Commercial'
CLUSTER_NAME = 'Sub-Minute_Commercial_Tests'

# COMMAND ----------

# MAGIC %md
# MAGIC ### Creating a DB Buffet job

# COMMAND ----------

notebook_location = dbutils.widgets.get('notebook_location')
output_folder     = dbutils.widgets.get('output_folder')

# this reads the Databricks access token from the specified file
token_file        = dbutils.widgets.get('token_file')

# required inputs to create a client
user_email        = dbutils.widgets.get('user_email')
user_cost_center  = dbutils.widgets.get('cost_center')
user_db_token     = dbutils.fs.head(token_file).strip()

# COMMAND ----------

# # create client
client = DatabricksRestClient(email=user_email,
                              token=user_db_token,
                              cost_center=user_cost_center)

# COMMAND ----------

truth_dataset = dbutils.widgets.get('truth_dataset')
data_filter   = dbutils.widgets.get('data_filter')

run_full_test = \
  dbutils.widgets.get('full_test') == 'Yes'

# COMMAND ----------

print('-- Latest test parameters --')
print('User email        :  {}'.format(user_email))
print('Analysis notebook :  {}'.format(notebook_location))
print('Check output data :  {}'.format('Yes' if run_full_test else 'No'))
print('Output folder     :  {}'.format(output_folder))
print('Truth dataset     :  {}'.format(truth_dataset))
print('Data filter       :  {}'.format(data_filter))

# COMMAND ----------

# output a random amazon availability zone
client.clusters.choose_random_az()

run_id = \
  client.jobs.start_run(cluster_name   =  CLUSTER_NAME,
                        project_name   =  PROJECT_NAME,
                        num_of_workers =  8 if run_full_test else 1,  # if actual data isn't generated, all work is done on the master node
                                                                      # so this could actually be 0
                                                                      # I'm just not sure if 0 is a valid value here
                        spot_instances =  True,
                        mdl_type       = 'dev',
                        autoterm_mins  =  20,
                        task_type      = 'notebook',
                        task_path      =  notebook_location,
                        task_params    = {
                          'national_tv_broadcast_start_date':  # yyyy-MM-dd
                            '2019-04-01',

                          'national_tv_broadcast_end_date':    # yyyy-MM-dd
                            '2019-04-28',

                          'originator_common_service_name':    # Can include 'Network', 'Cable', and 'Syndication'
                                                               # (comma-separated without spaces)
                                                               # e.g, 'Network,Cable'
                            'Network,Cable,Syndication',

                          'time_shifted_viewing':              # Can include 'live', 'live+3', amd 'live+7'
                                                               # (comma-separated without spaces)
                                                               # e.g, 'live,live+7'
                            'live,live+3,live+7',

                          'please_export':                     # 'Yes' to check actual data
                                                               #  'No' to only make sure the code runs
                            'Yes' if run_full_test else 'No',

                          'output_folder':                     # A storage location accessible by Databricks
                            output_folder,
                          
                          'adjust_commercial_duration':
                            'Yes',
                          
                          'is_rounded':
                            'Yes',
                          
                          'drop_corrupt_mop_rows':
                            'Yes'
                        })

status = client.jobs.get_run_status(run_id)

while status['state']['life_cycle_state'] in ('PENDING', 'RUNNING'):
  print('Run pending... waiting 5 minutes')
  sleep(300) # wait 5 minutes
  status = client.jobs.get_run_status(run_id)

print('Run finished')

# COMMAND ----------

print('-- Run status --')
status

# COMMAND ----------

# MAGIC %md
# MAGIC ### Comparing the tested ouput to the truth dataset

# COMMAND ----------

test_flags = []
tests_run  = dict()

# COMMAND ----------

test_flags.append('result_state' in status['state'] and status['state']['result_state'] == 'SUCCESS')

tests_run['01. Running the tested notebook'] = (
  'PASSED:  The tested notebook was able to run in full'
  if test_flags[-1] else
  'FAILED:  The tested notebook wasn\'t able to run in full')

# COMMAND ----------

if run_full_test and all(test_flags):  
  notebook_output = dbutils.fs.head(output_folder + 'debugging_info')

  new_df = \
    spark.read.parquet(notebook_output) \
         .where(data_filter)
  
  old_df = \
    spark.read.parquet(truth_dataset) \
         .where(data_filter)

  old_count = old_df.where(old_df['ac_rating'].astype('string') != 'removed').count()
  new_count = new_df.where(new_df['ac_rating'].astype('string') != 'removed').count()
  
  test_flags.append(new_count == old_count)
  
  print('Old row count: {}'.format(old_count))
  print('New row count: {}'.format(new_count))

  tests_run['02. Checking the total row count'] = (
    'PASSED:  The number of rows matches the truth dataset'
    if test_flags[-1] else
    'FAILED:  The number of rows does not match the truth dataset')
  
else:
  tests_run['02. Checking the total row count'] = 'SKIPPED'

# COMMAND ----------

if run_full_test and all(test_flags):
  # The dataframe juxtaposing the new and old data
  comp = new_df.join(old_df,
                     on=[
                       old_df.telecast_broadcast_date   == new_df.telecast_broadcast_date,
                       old_df.demo                      == new_df.demo,
                       old_df.playback_type             == new_df.playback_type,
                       old_df.viewing_source_short_name == new_df.viewing_source_short_name,
                       old_df.program_id                == new_df.program_id,
                       old_df.telecast_id               == new_df.telecast_id,
                       old_df.commercial_start_time     == new_df.commercial_start_time,
                       old_df.advertisement_key         == new_df.advertisement_key,
                       old_df.advertisement_code        == new_df.advertisement_code
                     ],
                     how='outer').cache()
  
  test_flags.append(comp.where(old_df.acm_projection.isNull() | new_df.acm_projection.isNull()).count() == 0)
  
  tests_run['03. Checking the ads list'] = (
    'PASSED:  All ads match the truth dataset'
    if test_flags[-1] else
    'FAILED:  There are mismatches with the truth dataset')
  
else:
  tests_run['03. Checking the ads list'] = 'SKIPPED'

# COMMAND ----------

if run_full_test and all(test_flags):
  max_diff = {
    'acm_projection':
      comp.select(
        # This difference could be 1, as the projections are in thousands and are rounded,
        # and when close to halfway between integers, might end up rounded either up or down
        MAX(ABS(new_df.acm_projection - old_df.acm_projection))
          .alias('new_column')
      ).collect() [0]['new_column'],

    'ecm_projection':
      comp.select(
        MAX(ABS(new_df.ecm_projection - old_df.ecm_projection))
          .alias('new_column')
      ).collect() [0]['new_column'],

    'acm_rating':
      comp.select(
        # Due to floating point operations error, this difference might not actually be 0,
        # but the error is in the 14-15th place after the decimal point
        MAX(ABS(new_df.acm_rating - old_df.acm_rating))
          .alias('new_column')
      ).collect() [0]['new_column'],

    'ecm_rating':
      comp.select(
        MAX(ABS(new_df.ecm_rating - old_df.ecm_rating))
          .alias('new_column')
      ).collect() [0]['new_column']
  }
    
  
  test_flags.append(max_diff['acm_projection'] < 2)
  tests_run['04. Checking ACM projections'] = (
    'PASSED:  ACM projections are within the threshold'
    if test_flags[-1] else
    'FAILED:  ACM projections difference exceeds the threshold')
  
  test_flags.append(max_diff['ecm_projection'] < 2)
  tests_run['05. Checking ECM projections'] = (
    'PASSED:  ECM projections are within the threshold'
    if test_flags[-1] else
    'FAILED:  ECM projections difference exceeds the threshold')
  
  test_flags.append(round(max_diff['acm_rating'], FLOATING_POINT_TOLERANCE) == 0)
  tests_run['06. Checking ACM ratings'] = (
    'PASSED:  ACM ratings are within the threshold'
    if test_flags[-1] else
    'FAILED:  ACM ratings difference exceeds the threshold')
  
  test_flags.append(round(max_diff['ecm_rating'], FLOATING_POINT_TOLERANCE) == 0)
  tests_run['07. Checking ECM ratings'] = (
    'PASSED:  ECM ratings are within the threshold'
    if test_flags[-1] else
    'FAILED:  ECM ratings difference exceeds the threshold')
  
  print('Max differences: {}'.format(max_diff))

else:
  tests_run['04. Checking ACM projections'] = 'SKIPPED'
  tests_run['05. Checking ECM projections'] = 'SKIPPED'
  tests_run['06. Checking ACM ratings']     = 'SKIPPED'
  tests_run['07. Checking ECM ratings']     = 'SKIPPED'

# COMMAND ----------

for test in sorted(tests_run):
  print('TEST {test}\n         {test_result}'.format(test=test, test_result=tests_run[test]))