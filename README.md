Sub-Minute Commercial Ratings Project
=====================================
<!-- Purpose of the project - One paragraph of project description goes here.
+ What does this do?
+ How does it do it (example below)?
    + Step 1: Read intab and weighting control data from MDL via Databricks.
    + Step 2: Call Benchpress to produce weights on a daily basis.
    + Step 3: Write CSV file output with weights.
+ Who the target users are?
+ Dependencies (e.g., MDL data availability)?
+ Screenshots / RecordIt -->

This project aims to develop PySpark code for calculating the commercial statistics below
 and that would be available for deployment throughout the Data Science organization.

The calculated statistics include:
  + **Average Comercial Minute (ACM)**  
    This statistic is currently available from NPOWER via Nielsen Answers,
    and this project's goal is to match NPOWER's output as close as possible
    using production data in MDL.
    This allows to validate MDL data against NPOWER's
    before calculating other statistics.

  + **Exact Commercial Minute (ECM)**  
    This statistic can be calculated using production data in MDL,
    but isn't available via existing tools.

  + **Exact Commercial Sub-Minute (tentatively, ECS)**  
    This statistic requires sub-minute tuning/viewing data not currently available in production MDL data.

See the [Statistics](#Statistics) section below for calculation details. Calculating these statistics, in general requires two sets of data, which the user should prepare in advance:
  + **Advertisement** schedule  
    At a minimum, the data should have the following fields:
      + Station/Network ID
      + Date, yyyy-MM-dd
      + Ad starting time, HH:mm:ss
      + Ad duration, in seconds <!-- TODO: Depending on the kind of average being used for calculating the ratings (e.g, "Over Ad Campaign"),  additional fields may be required: -->
      + Ad ID

  + **Tuning** (**Viewing**) events for households (persons)  
    The tuning/viewing data should provide the following fields:
      + Station/Network ID
      + Date, yyyy-MM-dd 
      + Telecast start time, HH:mm:ss
      + Minute of Program, MOP <!-- TODO: + Second of Minute, SOM (for sub-minute statistics) -->
      + SOW  
        The sum of weights of the households / persons tuned into the station during the MOP <!-- / SOP -->
      + UE  
        The universe estimate of the households / persons for the date <!-- TODO: Depending on the kind of average being used for calculating the ratings (e.g, "Over Time", "Per Program", "Per Telecast", etc.), additional fields may be required: -->
      + Program ID
      + Telecast ID

The code is meant to be run on Spark clusters, e.g, on Databricks.

Currently, the functions calculating the statistics take input and return the result in the form of `pyspark.sql.DataFrame` instances (i.e, Spark dataframes).


Getting Started
---------------
<!-- These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. -->
TBA

### 1. Prerequisites
<!-- Download and install [Git Bash](https://www.atlassian.com/git/tutorials/git-bash "Git Bash Download/Tutorial"). -->
TBA

### 2. Installing
<!-- A step by step series of examples that tell you how to get a development env running.
+ The URL of the Bitbucket remote repository for this project is:
    + https://adlm.nierlsen.com/...
+ Refer to the tutorial in Prerequisites section to:
    + Setup the local repository, add files, commit, and push changes. -->
TBA

### 3. Status
<!-- Notes/Warnings about the current status of the project - It may include “Use it at your own risk/discretion” sentence.
+ Coverage Report link (optional) -->
TBA


Deployment
----------
<!-- Notes about how to run/execute this using real data - Useful and motivating examples.
+ Inputs
    + Required (e.g., Start Date, End Date)
    + Optional (e.g., DMA)
+ Data Checks (e.g., Column Names and Types)
+ Outputs (e.g., Screenshots) -->
TBA


Built With
----------
+ Databricks
+ PySpark


Contributing
------------
<!-- Pull requests are welcome. For major changes, please open an issue first in JIRA to discuss what you would like to change.
[National-Audio-Tech Project Management - NATPM](https://adlm.nielsen.com/jira/projects/NATPM/issues/NATPM-78?filter=allopenissues "Jira NATPM Project").

+ Fork it
+ Create your feature branch - **_$ git checkout ..._**
+ Commit your changes - **_$ git commit ..._**
+ Push to the branch - **_$ git push ..._**
+ Create a new pull request -->
TBA


Authors
-------
+ [Arseny Egorov](mailto:arseny.egorov@nielsen.com)
+ [MJ Pakdel](mailto:mj.pakdel@nielsen.com)


Acknowledgments
---------------
<!-- + Inspiration - Innovator
+ Hat tips - Collaborator 1
+ Hat tips - Collaborator 2 -->
TBA



Statistics
==========

ACM
---
ACM stands for Average Commercial Minute and is a commercial statistic calculated at the telecast level using minute-level tuning/viewing data.

**Projections**  
For each minute of the telecast with commercial time, the audience projection is multiplied by the total commercial time during that minute. These products are summed up over the entire telecast, and the result is divided by the total commercial time during the telecast:
```
ACM_projection = sum(projection[m] * commercial_time[m]) / total_commercial_time
```
where `m` ranges over all minutes of the telecast with commercial time.

**Ratings**  
The ACM rating is computed by dividing the projection by the universe estimate.

### Software
  * `./acm/acm.py`   
    The abstracted functions for computing ACM projections and ratings

### Examples
TBA


ECM
---
ECM stands for Exact Commercial Minute and is a commercial statistic calculated at the individual ad level using minute-level tuning/viewing data.

**Projections**  
The projection is calculated for individual ad airings. For each minute of the telecast that overlaps with the ad, the audience projection is multiplied by the overlap  in seconds. These produccts are totaled and the result is divided by the total length of the ad in seconds:
```
ECM_projection = sum(projection[m] * ad_overlap_time[m]) / total_ad_time
```
where `m` ranges over all the minutes overlaping with the ad.

**Ratings**  
TBA
<!-- An individual airing's rating is computed by dividing the projection by the universe estimate. Individual airings' ratings are then aggregated over telecasts, programs, ad campaigns, etc., depending on the requirements. -->

### Software
  * `./ecm/ecm.py`   
    The abstracted functions for computing ECM projections and ratings

### Examples
  * `./examples/ECM Households (AdIntel+NPOWER).py`  
    A Databricks notebook with an example of ECM computations using ads and tuning data reported by AdIntel and NPOWER
  * `./examples/ECM Households (MDL).py`  
    A Databricks notebook with an example of ECM computations using ads and tuning data available in MDL


ECS
---
EC stands for Exact Commercial and is a commercial statistic calculated at the individual ad level using subminute-level tuning/viewing data.

**Projections**  
The audience projections for each second of the ad are summed up, and the sum is divided by the total ad time in seconds.
```
EC_projection = sum(projection[s]) / total_ad_time
```
where `s` ranges over all the seconds of the ad.

**Ratings**  
TBA

### Software
  * `./ecs/ecs.py`   
    The abstracted functions for computing ECS projections and ratings

### Examples
TBA
